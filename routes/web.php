<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => '{locale}',
    'where' => ['locale' => '(ru|ua|by|pl|kz|en)'],
    'middleware' => ['locale']
], function () {
    Route::get('qr', [\App\Http\Controllers\QrCodeController::class, 'index']);
    Route::get('catalog-link', [\App\Http\Controllers\CatalogController::class, 'getCatalogLink'])
        ->name('get-catalogue-link');
    Route::get('/', [\App\Http\Controllers\MainController::class, 'index'])->name('main');
    Route::get('/about', [\App\Http\Controllers\AboutController::class, 'show'])->name('about.show');
    Route::get('/contacts', [\App\Http\Controllers\ContactsController::class, 'show'])->name('contacts.show');
    Route::prefix('academy')->group(function () {
        Route::get('', [\App\Http\Controllers\AcademyPageController::class, 'index'])->name('academy.index');
        Route::get('courses', [\App\Http\Controllers\AcademyCourseController::class, 'index'])
            ->name('academy.courses.index');
        Route::get('courses/{course}', [\App\Http\Controllers\AcademyCourseController::class, 'show'])
            ->where('course', '[0-9]+')
            ->name('academy.courses.show');
        Route::post('apply', [\App\Http\Controllers\AcademyPageController::class, 'apply'])->name('courses.apply');
//    Route::view('/search', 'search.index');
    });
    Route::get('search', [\App\Http\Controllers\SearchController::class, 'index'])->name('search');
    Route::get('/cart', [\App\Http\Controllers\CartController::class, 'index'])->name('cart');
    Route::post('/change-cart', [\App\Http\Controllers\CartItemController::class, 'changeQty'])
        ->name('change-cart-qty');
    Route::post('/delete-from-cart', [\App\Http\Controllers\CartItemController::class, 'removeItem']);
    Route::post('/add-to-cart', [\App\Http\Controllers\CartItemController::class, 'addItem'])
        ->name('add-to-cart');
    Route::post('apply-promocode', [\App\Http\Controllers\CartController::class, 'applyPromocode']);
    Route::post('refuse-promocode', [\App\Http\Controllers\CartController::class, 'refusePromocode']);
    Route::post('create-order', [\App\Http\Controllers\OrderController::class, 'create']);

    Route::post('cancel-order', [\App\Http\Controllers\OrderController::class, 'delete'])->name('cancel-order');

    Route::get('catalog', [\App\Http\Controllers\CatalogController::class, 'fabricCollection']);
//        Route::get('fabrics', [\App\Http\Controllers\CatalogController::class, 'fabrics'])
//        ->name('catalog.fabric');
//    Route::post('fabrics', [\App\Http\Controllers\CatalogController::class, 'fabricsFilters'])
//        ->name('catalog.fabric.filter');
    Route::get('fabrics', [\App\Http\Controllers\CatalogController::class, 'next'])
        ->name('catalog.fabric');
    Route::get('/fabrics/collections', [\App\Http\Controllers\CatalogController::class, 'fabricCollection'])
        ->name('catalog.collections.fabric');
    Route::get('/fabrics/collections/{collection}', [\App\Http\Controllers\CatalogController::class, 'fabric'])
        ->name('catalog.collection.fabrics')
        ->where('collection', '[0-9]+');

    Route::post('/filters/{collection}', [\App\Http\Controllers\CatalogController::class, 'filters'])
        ->name('catalog.fabric.get')
        ->where('collection', '[0-9]+');

    Route::post('/fabrics/collections', [\App\Http\Controllers\CatalogController::class, 'getFabricCollections'])
        ->name('catalog.fabric.collections.get');
    Route::post('/fabrics', [\App\Http\Controllers\CatalogController::class, 'filteredFabrics'])
        ->name('catalog.fabric.filters');
    Route::post('/catalog/next/{collection}', [\App\Http\Controllers\CatalogController::class, 'next'])
        ->name('catalog.fabric.next')
        ->where('collection', '[0-9]+');

    Route::get('/catalog/carpets', [\App\Http\Controllers\CarpetController::class, 'carpet'])
        ->name('catalog.carpets');
    Route::post('/catalog/carpets', [\App\Http\Controllers\CarpetController::class, 'get'])
        ->name('catalog.carpets.get');
    Route::get('/catalog/carpets/next', [\App\Http\Controllers\CarpetController::class, 'next'])
        ->name('catalog.carpets.next');

    Route::get('/catalog/component', [\App\Http\Controllers\ComponentController::class, 'component'])
        ->name('catalog.component');
    Route::post('/catalog/component', [\App\Http\Controllers\ComponentController::class, 'get'])
        ->name('catalog.component.get');
    Route::post('/catalog/component/next', [\App\Http\Controllers\ComponentController::class, 'next'])
        ->name('catalog.component.next');

    Route::get('/fabric/{fabric}/{slug}', [\App\Http\Controllers\ProductController::class, 'fabric'])
        ->name('product');
    Route::get('/component/{component}/{slug}', [\App\Http\Controllers\ProductController::class, 'component'])
        ->name('accessories');
    Route::get('/carpet/{carpet}/{slug}', [\App\Http\Controllers\ProductController::class, 'carpet'])
        ->name('carpets');
    Route::post('/cart-submit', [\App\Http\Controllers\CartController::class, 'submit']);
    Route::get('/checkout', [\App\Http\Controllers\CheckoutController::class, 'checkout'])
        ->name('checkout');

    Route::group(['prefix' => 'profile', 'middleware' => 'auth'], function () {
        Route::get('/', [\App\Http\Controllers\ProfileDataController::class, 'index'])
            ->name('profile.index');

        Route::prefix('data')->group(function () {
            Route::get('/', [\App\Http\Controllers\ProfileDataController::class, 'index'])
                ->name('profile.data.index');
            Route::post('/', [\App\Http\Controllers\ProfileDataController::class, 'store'])
                ->name('profile.data.store');
        });

        Route::prefix('orders')->group(function () {
            Route::get('/', [\App\Http\Controllers\ProfileOrderController::class, 'index'])
                ->name('profile.orders.index');
            Route::get('pdf/{order}', [\App\Http\Controllers\OrderController::class, 'generateInvoice'])
                ->where('order', '[0-9]+')
                ->name('generate.pdf.invoice');
        });

        Route::prefix('news')->group(function () {
            Route::get('/', [\App\Http\Controllers\ProfileNewsController::class, 'index'])
                ->name('profile.news.index');
            Route::get('load', [\App\Http\Controllers\ProfileNewsController::class, 'index'])
                ->name('profile.news.load');
            Route::get('load-notification', [\App\Http\Controllers\ProfileNewsController::class, 'index'])
                ->name('profile.notifications.load');
            Route::get('{news}/download', [\App\Http\Controllers\ProfileNewsController::class, 'download']) //TODO ch method
            ->name('news.download');
        });

        Route::prefix('price-list')->group(function () {
            Route::get('/', [\App\Http\Controllers\ProfilePriceListController::class, 'index'])
                ->name('profile.price-list.show');
            Route::get('/download', [\App\Http\Controllers\ProfilePriceListController::class, 'download'])
                ->name('profile.price-list.download');
        });
    });

    Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'login']);
    Route::post('/login/code', [\App\Http\Controllers\Auth\LoginController::class, 'code'])
        ->middleware(['throttle:5:1'])
        ->name('login.code');
    Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
    Route::get('/login', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginCheckoutForm'])->name('login');

    Route::get('login/{provider}', [\App\Http\Controllers\Auth\SocialLoginController::class, 'show'])
        ->name('social-login.show');

    Route::get('register', [\App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('register', [\App\Http\Controllers\Auth\RegisterController::class, 'register']);

    Route::view('/403', 'errors.404');
    Route::view('/404', 'errors.404');
    Route::view('/500', 'errors.404');
    Route::view('/503', 'errors.503');

    Route::view('/print-text', 'checkout.print.bill-layout');
    Route::post('subscribe', [\App\Http\Controllers\SubscriberController::class, 'addToList']);
});
Route::get('download-orders-file/{name}', [\App\Http\Controllers\OrderController::class, 'dumpDownloadOrdersXlsx'])
    ->name('dump-download');

Route::get('login/{provider}/callback', [\App\Http\Controllers\Auth\SocialLoginCallbackController::class, 'show'])
    ->name('social-login-callback.show');
Route::match(['get', 'post'], 'deletion/{provider}/callback', [\App\Http\Controllers\Auth\SocialLoginCallbackController::class, 'destroy'])
    ->name('social-login-callback.destroy');

Route::get('robots.txt', 'RobotsController')->name('robots');
Route::get('sitemap.xml', 'SitemapController')->name('sitemap');

Route::fallback('LocaleRedirectController');
