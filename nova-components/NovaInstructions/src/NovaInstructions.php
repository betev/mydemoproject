<?php

namespace Flagstudio\NovaInstructions;

use Laravel\Nova\Card;

class NovaInstructions extends Card
{
    public function __construct($component = null)
    {
        parent::__construct($component);

        $this->withMeta([
            'instructions' => [
                [
                    'title' => 'Требования к загрузке изображений',
                    'link' => env(
                        'NOVA_INSTRUCTION_URL',
                        'https://docs.google.com/document/d/1GhcVr7zZS3tCaqCsu66ORwq3C-_8LHObOzc6MWrV4aY/edit#heading=h.m3cjb9xtveee'
                    ),
                ],
            ],
        ]);
    }

    /**
     * The width of the card (1/3, 1/2, or full).
     *
     * @var string
     */
    public $width = '1/3';

    /**
     * Get the component name for the element.
     *
     * @return string
     */
    public function component()
    {
        return 'nova-instructions';
    }
}
