Nova.booting((Vue, router, store) => {
    Vue.component('index-dadata-address', require('./components/IndexField'))
    Vue.component('detail-dadata-address', require('./components/DetailField'))
    Vue.component('form-dadata-address', require('./components/FormField'))
})
