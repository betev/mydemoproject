<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fabrics', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->jsonb('name');
            $table->string('slug')->unique();
            $table->decimal('price_per_roll')->default(0);
            $table->decimal('price_per_meter')->default(0);
            $table->jsonb('description')->nullable();
            $table->jsonb('information')->nullable();
            $table->boolean('in_stock')->default(true);
            $table->boolean('archived')->default(false);
            $table->softDeletes('deleted_at', 0);

            $table->foreignId('manufacturer_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');
            $table->foreignId('fabric_type_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');
            $table->foreignId('fabric_collection_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');
            $table->foreignId('filter_color_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');
            $table->foreignId('color_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');
            $table->foreignId('country_id')
                ->nullable()
                ->constrained()
                ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fabrics');
    }
}
