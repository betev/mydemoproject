<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Fabric::class, function (Faker $faker) {
    return [
        'name' => [
            'ru' => $name = $faker->unique()->word,
        ],
        'slug' => \Illuminate\Support\Str::slug($name),
        'code' => \Illuminate\Support\Str::random(6),
        'price_per_roll' => 10,
        'price_per_meter' => 1,
        'ru' => true,
        'fields' => [
            'ru' => [
                'hit' => false,
                'delete' => false,
                'publish' => true,
                'presence' => true
            ]
        ]
    ];
});
