<?php

use Illuminate\Database\Seeder;

class FabricSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manufacturers = \App\Models\Manufacturer::all();
        $fabricTypes = \App\Models\FabricType::all();
        $fabricCollections = \App\Models\FabricCollection::all();
        $colors = \App\Models\Color::all();
        $filterColors = \App\Models\FilterColor::all();
        $countries = \App\Models\Country::all();

        $image = public_path('images/product.jpg');

        foreach ($countries as $country) {
            $fabrics = factory(\App\Models\Fabric::class, 10)->make();

            foreach ($fabrics as $fabric) {
                $fabric->manufacturer()->associate($manufacturers->random());
                $fabric->fabricType()->associate($fabricTypes->random());
                $fabric->fabricCollection()->associate($fabricCollections->random());
                $fabric->color()->associate($colors->random());
                $fabric->filterColor()->associate($filterColors->random());
                $fabric->country()->associate($country);
                $fabric->save();

                if (file_exists($image)) {
                    $fabric->addMedia($image)
                        ->preservingOriginal()
                        ->toMediaCollection(\App\Models\Fabric::MEDIA_IMAGE);
                }
            }
        }
    }
}
