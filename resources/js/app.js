import Vue from 'vue';
import axios from 'axios';
import * as VueGoogleMaps from 'vue2-google-maps';
import MainMenuBtn from './components/header/main-menu-btn.vue';
import MainMenu from './components/header/main-menu.vue';
import HeaderAccount from './components/header/header-account.vue';
import HeaderCart from './components/header/header-cart.vue';
import SalesHits from './components/sales-hits.vue';
import CatalogPage from './components/catalog/catalog-page.vue';
import PersonalWarning from './components/personal-warning.vue';
import ImageSlider from './components/image-slider.vue';
import CustomVideo from './components/custom-video.vue';
import PopupSubscribe from './components/popup-subscribe.vue';
import ContactsPage from './components/contacts/contacts-page.vue';
import Orders from './components/profile/orders.vue';
import MyNews from './components/profile/my-news.vue';
import ProfileUserData from './components/profile/profile-user-data.vue';
import TextField from './components/text-field.vue';
import ProfileAddress from './components/profile/profile-address.vue';
import AddressCard from './components/profile/address-card.vue';
import PriceList from './components/profile/price-list.vue';
import LoginForm from './components/login/login-form.vue';
import ConfirmationCodeForm from './components/login/confirmation-code-form.vue';
import Cart from './components/cart/cart.vue';
import CheckoutForm from './components/checkout/checkout-form.vue';
import Carousel from './components/product/carousel.vue';
import ProductCartAction from './components/product/product-cart-action.vue';
import ProductTitle from './components/product/product-title.vue';
import ProductInfo from './components/product/product-info.vue';
import MobileAddressCard from './components/contacts/mobile-address-card.vue';
import Localization from "./mixins/Localization";
import AcademyForm from './components/academy/academy-form.vue';
import SuccessPopup from './components/popups/success-popup.vue';

Vue.mixin(Localization);

Vue.use(VueGoogleMaps, {
    load: { key: 'AIzaSyAZ_Z0sUJTm52AzMsXOmzeimjjOJLVlhQ4', libraries: 'places' },
});

window.csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
axios.defaults.headers.post['X-CSRF-Token'] = window.csrf;

new Vue({
    el: '#header',
    components: {
        MainMenuBtn,
        MainMenu,
        HeaderCart,
        HeaderAccount,
    },
});

new Vue({
    el: '#main',
    components: {
        SalesHits,
        CatalogPage,
        ImageSlider,
        CustomVideo,
        ContactsPage,
        Orders,
        MyNews,
        ProfileUserData,
        TextField,
        ProfileAddress,
        AddressCard,
        PriceList,
        Cart,
        Carousel,
        ProductCartAction,
        LoginForm,
        ConfirmationCodeForm,
        CheckoutForm,
        MobileAddressCard,
        ProductTitle,
        ProductInfo,
        AcademyForm,
        SuccessPopup,
    },
});

new Vue({
    el: '#footer',
    components: {
        PersonalWarning,
        PopupSubscribe,
    },
});
