const patternEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-ZА-Яа-яЁёa-zA-Z\-0-9]+\.)+[a-zA-ZА-Яа-яЁёa-zA-Z]{2,}))$/; // eslint-disable-line
const isEmail = (value) => patternEmail.test(value);

const isPhone = (value) => {
    const pattern = /^[ 0-9()\-+]{5,25}$/;
    return pattern.test(value);
};

const patternNumber = /^[0-9]+$/;
const isNumber = (value) => patternNumber.test(value);

export {
    isEmail,
    isPhone,
    isNumber,
};
