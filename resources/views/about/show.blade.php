@extends('layouts.app',['meta'=>$meta])
@section('content')

    @php
        $locale = app()->getLocale();
    @endphp

    <main id="main" class="about">

        @include('parts.breadcrumbs')

        @if ($pageImage)
            <img src="{{$pageImage}}" title="Изображение">
        @endif
        @foreach($constructor as $constructorItem)
            @switch($constructorItem->name())
                @case('gallery')
                <?php
                $key =  'gallery_'.$constructorItem->key();
                $media = $about->getMedia($key);
                $sliderItems = [];
                foreach($media as $m){
                    $sliderItems[] = [
                        'img' => [
                            'image_325' => $m->getUrl('325'),
                            'image_650' => $m->getUrl('650'),
                            'image_1300' => $m->getUrl('1300')
                        ],
                        'id' => $m['id'],
                        'name' => $m['id']
                    ];
                }
                $sliderItems = json_encode($sliderItems);
                ?>
                <section class="about__slider-row">
                    <div class="about__inner">
                        <image-slider
                                class="about__slider"
                                :slider-items='<?php echo $sliderItems ?>'
                                :buttons-text="{
                                'prev': 'Предыдущее',
                                'next': 'Далее',
                            }"
                                :buttons-labels="{
                                'prev': 'Предыдущие изображения',
                                'next': 'Следующие изображения',
                            }"
                                slide-title="Открыть"
                                close-text="Закрыть"
                        ></image-slider>
                    </div>
                </section>
                @break
                @case('big_photo')
                <section class="about__banner-row">
                    <div class="about__inner">
                        <img
                                src="{{ asset('storage/'.$constructorItem->getAttributes()['big_photo']) }}"
                                class="about__banner"
                                alt="Баннер страницы"
                                width="1417"
                                height="665"
                                loading="lazy"
                        >
                    </div>
                </section>
                @break

                @case('small_photo')
                <section class="about__second-banner-row">
                    <div class="about__inner about__second-banner-row-inner">
                        <img
                                src="{{ asset('storage/'.$constructorItem->getAttributes()['small_photo']) }}"
                                class="about__second-banner"
                                alt="Баннер страницы"
                                width="974"
                                height="546"
                                loading="lazy"
                        >
                    </div>
                </section>
                @break

                @case('citation')
                <section class="about__border-text-row">
                    <div class="about__inner">
                        <article class="about__border-text-article">
                            {!! $constructorItem->getAttributes()['citation_'.$locale] ?? '' !!}
                        </article>
                    </div>
                </section>
                @break

                @case('text')
                <section class="about__text-row">
                    <div class="about__inner about__text-row-inner">
                        {!! $constructorItem->getAttributes()['text_'.$locale] !!}
                    </div>
                </section>
                @break

                @case('divider')
                <hr class="about__divider">
                @break
                @case('video_frame')
                <section class="about__video-row">
                    <div class="about__video-inner">
                        <iframe
                            width="800"
                            height="480"
                            class="about__video-iframe"
                            src="https://www.youtube.com/embed/{{ $constructorItem->getAttributes()['video_frame'] }}">
                        </iframe>
                    </div>
                </section>
                @break
                @case('narrow_video')
                <?php
                $key =  'narrow_video_'.$constructorItem->key();
                $media = $about->getMedia($key);
                $videoUrl = null;
                foreach($media as $m){
                    $videoUrl = $m->getUrl();
                };
                ?>
                <section class="about__video-row">
                    <div class="about__video-inner">
                        <custom-video
                                title="Название видео"
                                url-mp4="{{ $videoUrl }}"
                                url-webm="{{ asset('video/about-video.webm') }}"
                                poster="{{ asset('images/about-video-banner-min.jpg') }}"
                                :text="{
                'play': 'Запустить',
                'stop': 'Остановить',
            }"
                        ></custom-video>
                    </div>
                </section>
                @break
                @case('big_link')
                <div class="about__text-link-wrap">
                    <a
                            href="{{ $constructorItem->getAttributes()['big_link_url'] }}"
                            target="_blank"
                            class="about__text-link-large about__text-subtitle"
                    >
                        {{ $constructorItem->getAttributes()['big_link_title'] }}
                    </a>
                </div>
                @break
                @case('big_header')
                <div class="about__text-group">
                    <h1 class="about__text-subtitle">
                        {{ $constructorItem->getAttributes()['header'] ?? '' }}
                    </h1>
                    <div class="about__text about__text--large-margin-bottom about__text--width-700">
                        {!! $constructorItem->getAttributes()['big_header_text_'.$locale] ?? '' !!}
                    </div>
                </div>
                @break
                @case('small_header')
                <div class="about__text-group">
                    <h2 class="about__text-subtitle">
                        {{ $constructorItem->getAttributes()['header'] ?? '' }}
                    </h2>
                    <div class="about__text about__text--large-margin-bottom about__text--width-700">
                        {!! $constructorItem->getAttributes()['small_header_text_'.$locale] ?? '' !!}
                    </div>
                </div>
                @break

            @endswitch
        @endforeach
    </main>

@endsection
