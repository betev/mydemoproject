@extends('layouts.app')
@section('content')

    <main id="main" class="main">

        @include('parts.breadcrumbs')

        @php
            $default_image = [
                '1x' => asset('images/product-default-img@2x-min.jpg'),
                '2x' => asset('images/product-default-img@3x-min.jpg'),
            ];
        @endphp

        <catalog-page
                title="Результаты поиска:"
                load-more-btn-title="Открыть еще"
                :product-image-default='@json($default_image)'
                :actions="{
                    getProducts: '/api/get-products',
                    getNextProducts: '/api/get-next-products'
                }"
                :catalog-list-data="{
                    labelsTitle: {
                        hit: 'HIT',
                        sale: 'SALE',
                        special: 'SPECIAL OFFER'
                    },
                    sliderBtnLabels: {
                        prev: 'Предыдущее изображение',
                        next: 'Следующее изображение',
                    },
                    catalogItems: [
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: false,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: false,
                                sale: true,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: false,
                                sale: false,
                                special: true,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                        {
                            title: 'NAME',
                            link: '#!',
                            images: [
                                '{{ asset('images/temp/hit-3-min.jpg') }}',
                                '{{ asset('images/temp/hit-2-min.jpg') }}',
                                '{{ asset('images/temp/hit-1-min.jpg') }}',
                                '{{ asset('images/temp/hit-4-min.jpg') }}',
                            ],
                            labels: {
                                hit: true,
                                sale: false,
                                special: false,
                            },
                        },
                    ],
                }"
                :search-aside="{
                    searchPhraseLabel: 'По запросу',
                    searchPhrase: 'шелк',
                    searchPageLinks: [
                        {
                            title: 'Все',
                            count: '55',
                            link: '#!',
                        },
                        {
                            title: 'Ткани',
                            count: '20',
                            link: '#!',
                        },
                        {
                            title: 'Ковры',
                            count: '20',
                            link: '#!',
                        },
                        {
                            title: 'Комплектующие',
                            count: '15',
                            link: '#!',
                        },
                    ]
                }"
        >
        </catalog-page>

    </main>

@endsection
