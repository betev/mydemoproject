{{ AdminBar::setModel($model) }}

@extends('layouts.app')
@section('content')
    @php
        $link = $productType === 'fabric'
        ? route('catalog.collection.fabrics', [app()->getLocale(), $model->fabricCollection->id])
        : ($productType === 'component' ? route('catalog.component') : route('catalog.carpets'));
    @endphp
    <main id="main" class="main main--product">

        @include('parts.breadcrumbs')

        <article class="product">
            <div class="product__inner">
                <div class="product__link-wrap">
                    <a href="{{ $link }}" class="product__back-link">
                        <svg width="9" height="15">
                            <use xlink:href="#icon-breadcrumbs-chevron"></use>
                        </svg>
                        @if($productType === 'fabric')
                            <span>{{ __('product.К коллекциям') }}</span>
                        @endif
                        @if($productType === 'component')
                            <span>{{ __('product.К комплектующим') }}</span>
                        @endif
                        @if($productType === 'carpet')
                            <span>{{ __('product.К каталогу') }}</span>
                        @endif
                    </a>
                </div>

                <product-title
                        class="product__title--mobile"
                        default-title="{{ $title }}"
                        @if($productType === 'carpet')
                        default-variant="135 x 200"
                        @endif
                ></product-title>

                <div class="product__data">
                    <div class="product__carousel">
                        <carousel
                            :video='@json($video)'
                            :slides='@json($slides)'
                            :text="{
                                prevSlide: '{{ __('product.Предыдущий слайд') }}',
                                nextSlide: '{{ __('product.Следующий слайд') }}',
                            }"
                        ></carousel>
                    </div>

                    <div class="product__actions">
                        <div class="product__actions-row">
                            <product-title
                                    class="product__title--desktop"
                                    default-title="{{ $title }}"
                                    @if($productType === 'carpet')
                                    default-variant="135 x 200"
                                    @endif
                            ></product-title>
                        </div>

                        <div class="product__share">
                            <b class="product__share-title">{{ __('product.Поделиться в:') }}</b>
                            <div class="product__share-links">
                                <a
                                        class="product__share-link product__share-link--viber"
                                        href="viber://forward?text={{ __('М

не понравился этот товар. Посмотрите') }} {{url()->current()}}"
                                        title="{{ __('Поделиться в') }} Viber"
                                        target="_blank"
                                        rel="nofollow noopener"
                                >
                                    <svg width="30" height="30">
                                        <use xlink:href="#viber"></use>
                                    </svg>
                                </a>

                                <a
                                        class="product__share-link product__share-link--whatsapp"
                                        href="https://wa.me/?text={{ __('Мне понравился этот товар. Посмотрите') }} {{url()->current()}}"
                                        title="{{ __('Поделиться в') }} WhatsApp"
                                        target="_blank"
                                        rel="nofollow noopener"
                                >
                                    <svg width="30" height="30">
                                        <use xlink:href="#whatsapp"></use>
                                    </svg>
                                </a>

                                <a
                                        class="product__share-link product__share-link--mail"
                                        href="mailto:?subject={{ __('Товары Textoria&body=Мне понравился этот товар. Посмотрите') }} {{url()->current()}}"
                                        title="{{ __('Поделиться по электронной почте') }}"
                                >
                                    <svg width="32" height="23">
                                        <use xlink:href="#mail"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>

                        <div class="product__cart-actions">
                            @if($productType === 'fabric')
                                @foreach($cards as $card)
                                    <product-cart-action
                                            :default-price="{{ json_encode(floatval($card['price'])) }}"
                                            :labels="{{ json_encode($card['label']) }}"
                                            product-id="{{ $card['id'] }}"
                                            :max="{{ json_encode($card['max']) }}"
                                            :step="{{ json_encode($card['step']) }}"
                                            :unit="{{ json_encode($card['unit']) }}"
                                            :actions="{{ json_encode($card['actions']) }}"
                                            model="{{ $card['model'] }}"
                                            volume-measure="{{ $card['label']['title'] }}"
                                            product-name="{{ $title }}"
                                            :meters-ratio="{{ $card['meters_ratio'] }}"
                                    >
                                    </product-cart-action>
                                @endforeach

                            @endif

                            @if($productType === 'component')
                                <product-cart-action
                                        :default-price="{{ json_encode(floatval($card['price'])) }}"
                                        :labels="{{ json_encode($card['label']) }}"
                                        product-id="{{ $card['id'] }}"
                                        :max="{{ json_encode($card['max']) }}"
                                        :step="{{ json_encode($card['step']) }}"
                                        :actions="{{ json_encode($card['actions']) }}"
                                        model="{{ $card['model'] }}"
                                        volume-measure="{{ $card['label']['title'] }}"
                                        product-name="{{ $title }}"
                                >

                                </product-cart-action>
                            @endif

                            @if($productType === 'carpet')
                                <product-cart-action
                                        :default-price="{{ json_encode(floatval($card['price'])) }}"
                                        :labels="{{ json_encode($card['label']) }}"
                                        product-id="{{ $card['id'] }}"
                                        :max="{{ json_encode($card['max']) }}"
                                        :step="{{ json_encode($card['step']) }}"
                                        :actions="{{ json_encode($card['actions']) }}"
                                        :variants="{{ json_encode($card['variants']) }}"
                                        default-variant-id="{{ $card['id'] }}"
                                        model="{{ $card['model'] }}"
                                        volume-measure="{{ $card['label']['title'] }}"
                                        product-name="{{ $title }}"
                                >

                                </product-cart-action>
                            @endif
                        </div>
                        @if($productType === 'fabric')
                            <div class="product__care">
                                <b class="product__care-title">{{ __('product.Уход и чистка:') }}</b>
                                <ul class="product__care-items">
                                    @if ($cares)
                                        @foreach($cares as $care)
                                            <li title="{{ $care->description->{app()->getLocale()} ?? '' }}">
                                                <svg width="50" height="50" aria-label="{{ $care->name->{app()->getLocale()} ?? '' }}">
                                                    <use xlink:href="#{{ $care->slug }}"></use>
                                                </svg>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="product__description">
                        <h2 class="product__subtitle">{{ $descriptionTitle }}</h2>
                        <div class="product__description">
                            {!! $description !!}
                        </div>
                    </div>

                    <product-info
                            title="{{ __('product.Информация') }}"
                            :default-items="{{ json_encode($info) }}"
                    >
                    </product-info>
                </div>
            </div>
        </article>
    </main>

@endsection
