@extends('layouts.app')
@section('content')

    <main id="main" class="main">

        @include('parts.breadcrumbs')

        {{--В login-via-default указываем phone или email чтобы на странице отображалось соответствующее поле--}}

        <?php
            $actions = [
                'code' => route('login.code'),
                'login' => route('login'),
                'registration' => route('register'),
            ];
        ?>

        <div class="login-page">
            <div class="login-page__inner">
                <login-form
                    login-via-default="phone"
                    :confirmation-code-length="5"
                    :actions='@json($actions)'
                    :tabs-view="true"
                    :text="{
                        'phone': '{{ __('Телефон') }}',
                        'email': '{{ __('Электронная почта') }}',
                        'emailEng': 'email',
                        'getCode': '{{ __('Получить код') }}',
                        'or': 'или',
                        'loginViaEmail': '{{ __('Войти по почте') }}',
                        'loginViaPhone': '{{ __('Войти по телефону') }}',
                        'loginViaSocialNetworks': '{{ __('Войти через соцсети') }}',
                        'inputCode': '{{ __('Введите код') }}',
                        'codeWasSendPhone': '{{ __('Мы отправили код подтверждения на номер') }}',
                        'codeWasSendEmail': '{{ __('Мы отправили код подтверждения на E-mail') }}',
                        'changeInputValue': '{{ __('Изменить') }}',
                        'getNewCode': '{{ __('Получить новый код') }}',
                        'login': '{{ __('Войти') }}',
                        'registration': '{{ __('Регистрация') }}',
                        'auth': '{{ __('Авторизация') }}',
                        'toRegister': '{{ __('Зарегистрироваться') }}',
                        'fio': '{{ __('ФИО') }}',
                        'buyAsEntity': '{{ __('Покупка на Юрлицо') }}',
                        'address': '{{ __('Адрес') }}',
                        'addDeliveryAddress': '{{ __('Добавить адрес доставки') }}',
                        'city': '{{ __('Город') }}',
                        'street': '{{ __('Улица') }}',
                        'house': '{{ __('Дом') }}',
                        'flat': '{{ __('Квартира/офис') }}',
                        'transportCompany': '{{ __('Транспортная компания') }}',
                        'recipient': '{{ __('Получатель') }}',
                        'recipientPhone': '{{ __('Телефон получателя') }}',
                    }"
                    :social-networks-url='@json($socialNetworksUrl)'
                ></login-form>
            </div>
            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>

    </main>

@endsection
