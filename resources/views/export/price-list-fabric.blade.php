<?php
$exchange_rate = \App\Tools\CurrentCountry::get()->currency->exchange_rate;
$discount = auth()->user() && auth()->user()->customerType ? auth()->user()->customerType->discount_rate : 0;
?>
<table>
    <thead>
    <tr>
        <th><b>{{ __('Collection') }}</b></th>
        <th><b>{{ __('Характеристики') }}</b></th>
        <th><b>{{ __('Price per roll') }}</b></th>
        <th><b>{{ __('Price per meter') }}</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($collections as $collection)
        <tr>
            <td>{{ $collection->name }}</td>
            <td style="word-wrap:break-word">
                <ul>
                    <li>{{ __('Производитель').': '.$collection->fabrics[0]->manufacturer->name }}</li>
                    <li>{{ __('Тип ткани').': '.$collection->fabrics[0]->fabricType->name }}</li>
                    @if ($collection->fabrics[0]->information)
                        @foreach (explode(';', $collection->fabrics[0]->information) as $item)
                            @if (Illuminate\Support\Str::contains($item, '}'))
                                @php
                                $info = explode('}', $item);
                                @endphp
                                <li>
                                    {{ $info[0].': '. $info[1] }}
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </td>
            <td>{{ round(($collection->fabrics[0]->price_per_roll * $exchange_rate) * (100 - $discount) / 100, 2) }}</td>
            <td>{{ round(($collection->fabrics[0]->price_per_meter * $exchange_rate) * (100 - $discount) / 100, 2) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>