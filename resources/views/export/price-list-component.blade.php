<?php
$exchange_rate = \App\Tools\CurrentCountry::get()->currency->exchange_rate;
$discount = (auth()->user() && auth()->user()->customerType)
    ? auth()->user()->customerType->discount_rate
    : 0;
?>

<table>
    @if(!$components->toArray() == [])
    <thead>
    <tr>
        <th>{{ __('Code') }}</th>
        <th>{{ __('Name') }}</th>
        <th>{{ __('Price') }}</th>
        <th>{{ __('Some column name') }}</th>
    </tr>
    </thead>
    @endif

    <tbody>
    @foreach($components as $component)
        <tr>
            <td>{{ $component->code }}</td>
            <td>{{ $component->name }}</td>
            <td>{{ round(($component->price * $exchange_rate) * (100 - $discount) / 100, 2) }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>
