<table>
    <tr>
        <td><b>{{ __('Ключ') }}</b></td>
        <td><b>{{ __('Перевод') }}</b></td>
    </tr>
    @foreach($languages as $key => $value)
    <tr>
        <td>{{ $key }}</td>
        <td>{{ !is_array($value) ? $value : '' }}</td>
    </tr>
    @endforeach
</table>
