<table>
    <thead>

    </thead>
    <tbody>
    <tr>
        <td>Дата создания заказа</td>
        <td>Номер заказа</td>
        <td>ID клиента</td>
        <td>1 С ID клиента</td>
        <td>Имя Пользователя</td>
        <td>Email Пользователя</td>
        <td>Телефон Пользователя</td>
        <td>Наименование</td>
        <td>Цвет</td>
        <td>Метраж заказа</td>
        <td>Количество</td>
        <td>Цена</td>
        <td>Скидка</td>
        <td>Цена без скидки</td>
        <td>Адрес доставки</td>
        <td>Адрес самовывоза</td>
        <td>Комментарий</td>
        <td>Промокод</td>
        <td>Валюта</td>
    </tr>
    @foreach($all as $order)
        <tr>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
            <td style="background-color: #d2d4d4;"></td>
        </tr>
        <tr>
            <td>{{ $order['created_at'] }}</td>
            <td>{{ \Illuminate\Support\Str::upper(config('app.country_code')).'_'.$order['id'] }}</td>
            <td>{{ $order['userId'] ?? '' }}</td>
            <td>{{ $order['userOneSId'] ?? '' }}</td>
            <td>{{ $order['userName'] }}</td>
            <td>{{ $order['userEmail'] }}</td>
            <td>{{ $order['userPhone'] }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $order['price'] }}</td>
            <td>{{ $order['discount'] }}</td>
            <td>{{ $order['fullPrice'] }}</td>
            <td>{{ $order['deliveryAddress'] }}</td>
            <td>{{ $order['pickUpAddress'] }}</td>
            <td>{{ $order['comment'] }}</td>
            <td>{{ $order['promoName'] }}</td>
            <td>{{ $order['currencyTittle'] }}</td>
        </tr>
        @foreach($order['items'] as $item)
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>{{ $item['itemProductName'] }}</td>
                <td>{{ $item['itemColor'] }}</td>
                <td>{{ $item['itemUnit'] }}</td>
                <td>{{ $item['itemQuantity'] }}</td>
                <td>{{ $item['itemTotal'] }}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
