<?php
$exchange_rate = \App\Tools\CurrentCountry::get()->currency->exchange_rate;
$discount = auth()->user() && auth()->user()->customerType ? auth()->user()->customerType->discount_rate : 0;
?>
<table>
    @if(!$carpets->toArray() == [])
    <thead>
    <tr>
        <th>{{ __('Code') }}</th>
        <th>{{ __('Name') }}</th>
        <th>{{ __('Price') }}</th>
        <th>{{ __('Some column name') }}</th>
    </tr>
    </thead>
    @endif
    <tbody>
    @foreach($carpets as $carpet)
        <tr>
            <td>{{ $carpet->code }}</td>
            <td>{{ $carpet->name }}</td>
            <td>{{ round(($carpet->price * $exchange_rate) * (100 - $discount) / 100, 2) }}</td>
            <td></td>
        </tr>
    @endforeach
    </tbody>
</table>
