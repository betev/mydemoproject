<?php
$tableData =  $table_data ?? '[]';
$markers =  $markers ?? '[]';
$mapData = $map_data ?? '[]';
$locale = $locale ?? 'ru';
$meta = $meta?? [];
?>
@extends('layouts.app',['meta'=>$meta])
@section('content')

    <main id="main" class="about">
        @include('parts.breadcrumbs')
        <contacts-page
            :text="{
                    'address': '{{ __('Адрес') }}',
                    'phone': '{{ __('Телефон') }}',
                    'email': 'E-Mail',
                    'city': '{{ __('Город') }}',
                    'streetWithHouse': '{{ __('Улица/дом') }}',
                    'mobileTitle': '{{ __('Адрес/Телефон/E-mail') }}',
                }"
            :table-data='<?php echo json_encode($tableData) ?>'
            :map-data='<?php echo json_encode($mapData) ?>'
        ></contacts-page>

    </main>

@endsection

