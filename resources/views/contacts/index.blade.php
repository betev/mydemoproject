@extends('layouts.app')
@section('content')

    <main id="main" class="about">

        @include('parts.breadcrumbs')
        <contacts-page
                :text="{
                    'address': 'Адрес',
                    'phone': 'Телефон',
                    'email': 'E-Mail',
                    'city': 'Город',
                    'streetWithHouse': 'Улица/дом',
                    'mobileTitle': 'Адрес/Телефон/E-mail',
                }"
                :table-data="[
                    {
                        'id': 1,
                        'mapsMarkerId': 1,
                        'address': {
                            'country': 'Россия',
                            'town': 'Екатеринбург',
                            'street': 'ул. Ленина, д. 355',
                            'additional': 'У магазина Заря',
                        },
                        'phone': [
                            {
                                'url': '79489556523',
                                'title': '+ 7 948 95 565 23',
                            },
                            {
                                'url': '79097150544',
                                'title': '+ 7 909 71 505 44',
                            },
                        ],
                        'email': [
                            {
                                'url': 'textoria@pochta.ru',
                                'title': 'textoria@pochta.ru',
                            }
                        ],
                    },
                    {
                        'id': 2,
                        'address': {
                            'country': 'Украина',
                            'town': 'Киев',
                            'street': '',
                            'additional': '',
                        },
                        'phone': [
                            {
                                'url': '79011201523',
                                'title': '+ 7 901 12 015 23',
                            },
                        ],
                        'email': [
                            {
                                'url': 'textoria123@pochta.ru',
                                'title': 'textoria123@pochta.ru',
                            }
                        ],
                    },
                    {
                        'id': 3,
                        'mapsMarkerId': 2,
                        'address': {
                            'country': 'Казахстан',
                            'town': 'Астана',
                            'street': 'Название улицы, д. 23',
                            'additional': '',
                        },
                        'phone': [
                            {
                                'url': '78002350334',
                                'title': '+ 7 800 23 503 34',
                            },
                            {
                                'url': '79128019023',
                                'title': '+ 7 912 80 190 23',
                            },
                        ],
                        'email': [
                            {
                                'url': 'toria@yandex.ru',
                                'title': 'toria@yandex.ru',
                            }
                        ],
                    },
                ]"
                :map-data="{
                    'apiKey': 'AIzaSyD6wbSA91NaZlfmGKyV_TaFxI6Dpxr3BdM',
                    'center': {'lat': 56.8245, 'lng': 60.584},
                    'markers': [
                        {
                            'id': 1,
                            'position': {'lat': 56.825981, 'lng': 60.578748},
                            'img': '{{asset('images/contact-example-1-min.jpg')}}',
                            'title': 'Textoria',
                            'address': 'ул. Фрунзе, 101, Екатеринбург',
                            'phone': '+38 101 772 20 96',
                            'email': 'textoria@net.ua',
                        },
                        {
                            'id': 2,
                            'position': {'lat': 56.824517, 'lng': 60.592883},
                            'img': '{{asset('images/contact-example-2-min.jpg')}}',
                            'title': 'Textoria Second',
                            'address': 'ул. Белинского, 118А, Екатеринбург',
                            'phone': '+38 056 799 20 96',
                            'email': 'textoria2@net.ua',
                        },
                    ],
                }"
        ></contacts-page>

    </main>

@endsection
