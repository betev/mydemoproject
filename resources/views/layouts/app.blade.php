<!doctype html>
@php
    $locale = app()->getLocale();
    $currentRouterName = app('router')->currentRouteName();
    $routeParams = Route::getCurrentRoute()->parameters;
@endphp

<html lang="{{$locale}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta name="developer" content="flagstudio.ru">
    <meta name="cmsmagazine" content="3a145314dbb5ea88527bc9277a5f8274">
    <meta name="csrf-token" content="{!! csrf_token() !!}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
    <link rel="manifest" href="/site.webmanifest" crossorigin="use-credentials">
    <link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    @isset($meta)
        <meta name="title" content="{{ $meta->title ?? '' }}">
        <meta name="description" content="{{ $meta->description ?? '' }}">
        <meta name="keywords" content="{{ $meta->keywords ?? '' }}">
    @endisset

    <title>{{ isset($meta) && isset($meta->tag_title) ? $meta->tag_title : 'Textoria'  }}</title>

    <link href="{!! mix('/css/components.css') !!}" rel="stylesheet" type="text/css">
    @if($locale === 'kz')
        <link href="{!! mix('/css/fonts-minor.css') !!}" rel="stylesheet" type="text/css">
    @else
        <link href="{!! mix('/css/fonts.css') !!}" rel="stylesheet" type="text/css">
    @endif
    <link href="{!! mix('/css/app.css') !!}" rel="stylesheet" type="text/css">

    {!! $headScripts ?? '' !!}
</head>
<body>

{!! $beginScripts ?? '' !!}

@if (app()->environment('local') || optional(auth()->user())->isAdmin())
    {!! AdminBar::generate() !!}
@endif

<div class="main-wrapper">
    <div class="layout">
        <header id="header" class="header">
            <div class="header__inner">
                <div class="header__actions-wrapper header__actions-wrapper--left">
                    <main-menu-btn
                            label="Открыть меню"
                    >
                    </main-menu-btn>
                    <div class="header__lang">
                        <div class="header-lang">
                            @foreach($languageFields as $lang => $state)
                                @php
                                    $routeParams['locale'] = $lang;
                                @endphp
                                @if($state == 'On')
                                    <a
                                        href="{{ request()->root().'/'.str_replace($locale, $lang, request()->path()) }}"
                                        class="header-lang__item @if($locale == $lang) header-lang__item--current @endif"
                                    >
                                        {{strtoupper($lang)}}
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <a href="/{{ app()->getLocale() }}" class="header__logo" aria-label="{{ __('Перейти на главную страницу сайта') }}">
                    <svg
                        viewBox="0 0 177 44"
                        width="177"
                        height="44"
                        aria-label="Textoria logo"
                        class="header__logo-img"
                    >
                        <use xlink:href="#kz-logo" />
                    </svg>
                    @if($headerFields && $headerFields->show == 'On')
                        <span class="header__logo-slogan">
                            {{ $headerFields->text_over_logo}}
                        </span>
                    @endif
                </a>

                <div class="header__actions-wrapper header__actions-wrapper--right">
                    @php
                        $menu['menu'] = $accountData['menu']['profile'] ?? $accountData['menu']
                    @endphp
                    <header-account
                        class="header__account"
                        :account-data='@json($menu)'
                        :auth="{{auth()->user() ? 'true' : 'false'}}"
                        login-url="{{route('login')}}"
                        login-text="{{__('Войти')}}"
                    >
                    </header-account>

                    <header-cart
                            :product-amount="{{ count(\LukePOLO\LaraCart\Facades\LaraCart::getItems()) ?? '' }}"
                            cart-link="/{{ \Illuminate\Support\Facades\App::getLocale() }}/cart"
                            cart-link-label="{{ __('Перейти в корзину') }}"
                    >
                    </header-cart>
                </div>


            </div>

            <?php
                /*$phones = $contactsFields
                        ? array_map(function ($item) {
                        return $item->{$locale}->phones;
                    }, $contactsFields->{$locale}->phones)
                    : [];*/
                $languages = [];
                foreach ($languageFields as $lang => $state) {
                    if ($state == 'On') {
                        $languages[] = [
                            'url' => request()->root().'/'.str_replace($locale, $lang, request()->path()),
                            'slug' => $lang,
                            'title' => strtoupper($lang),
                            'isActive' => $lang === $locale,
                        ];
                    }
                }
            ?>

            <main-menu
                    :menu-items='@json(optional($headerFields)->menu)'
                    :download-catalog="{
                        link: '{!! $catalog !!}',
                        title: '{!! optional($headerFields)->catalog_text !!}',
                    }"
                    :profile-menu='@json($accountData['menu']['profile'] ?? $accountData['menu'])'
                    :search-data="{
                        action: '/{{ app()->getLocale() }}/search',
                        initialQuery: '{{ $searchQuery ?? '' }}',
                        labels: {
                            btn: '{{ __('Искать') }}',
                            input: '{{ __('Поиск по сайту') }}',
                        },
                    }"
                    :languages='@json($languages)'
            >
            </main-menu>
        </header>

        @yield('content')

    </div>
    <footer id="footer" class="footer">
        <div class="footer__inner">
            <ul class="footer__list footer__list--menu">
                @if($headerFields)
                @foreach($headerFields->menu as $item)
                    <li class="footer__list-item">
                       <a href="{{$item->url}}" class="footer__list-item-link">{{$item->title}}</a>
                    </li>
                @endforeach
                @endif
            </ul>

            <ul class="footer__list footer__list--contacts">



                @if($addresses)
                    @foreach($addresses as $address)
                        @if($address->is_active == 1)
                            <li class="footer__list-item footer__list-item--address">
                                {{ $address->address }}
                            </li>
                        @endif
                    @endforeach
                @endif

                @foreach($phones as $phone)
                <li class="footer__list-item">
                    <a href="tel:{{ $phone->phone }}" class="footer__list-item-link">{{ $phone->phone }}</a>
                </li>
                @endforeach

                <li class="footer__list-item">
                    <a href="mailto:{{ isset($addressFields->{$locale}->email) ? $addressFields->{$locale}->email : '' }}" class="footer__list-item-link">{{ isset($addressFields->{$locale}->email) ? $addressFields->{$locale}->email : '' }}</a>
                </li>
            </ul>

            <ul class="footer__list footer__list--socials">
                @if($socialFields)
                @foreach($socialFields->{$locale}->social as $social)
                <li class="footer__list-item">
                    <a href="{{$social->url}}" class="footer__list-item-link" target="_blank" rel="nofollow noopener">{{$social->title}}</a>
                </li>
                @endforeach
                @endif

            </ul>

            <ul class="footer__list footer__list--links">

                <li class="footer__list-item">
                    <a href="mailto:{{ optional($mailToDirectorFields)->director_email }}" class="footer__list-item-link">{{ optional($mailToDirectorFields)->button_name }}</a>
                </li>

                <li class="footer__list-item">
                    <a download href="{!! $privacy !!}" class="footer__list-item-link">{{ optional($privacyFields)->button_text }}</a>
                </li>

                <li class="footer__list-item">
                    <a href="https://flagstudio.ru/service/%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2" class="footer__list-item-link" title="Разработка сайтов на Laravel" target="_blank" rel="noopener">
                        {{ __('Разработка и дизайн') }}</a> - {{ __('Студия Флаг') }}
                </li>

                <li class="footer__list-item footer__list-item--copyright">
                    © Arben, 2019 - {{ date('Y') }}
                </li>

            </ul>

                <ul class="footer__list footer__list--law-data">

                    <li class="footer__list-item footer__list-item--juridical-info">
                        {!! optional($legalFields)->legal !!}
                        <a href="{{ $contract }}" target="_blank" class="footer__list-item-link">{{ optional($contractFields)->button_text }}</a>
                    </li>

                </ul>

            </div>


                <personal-warning
                    accept-btn-title="{{ __('Принять') }}">
                    <template v-slot:default>
                        {{ __('Мы используем данные файлы cookie, данные об IP-адресе и местоположении, разработанные третьими лицами для анализа событий на нашем сайте. Продолжая просмотр страниц сайта, вы принимаете условия его использования. Более подробные сведения можно посмотреть в') }}
                        <a class="personal-warning__link" href="{{ $privacy }}" target="_blank" title="{{ __('Политика конфиденциальности') }}">
                            {{ __('Политике конфиденциальности') }}
                        </a>.
                    </template>
                </personal-warning>
                <? // todo back - кажется по ТЗ заполненными могут быть либо popup-image-src либо subTitle, но не одновременно ?>
                @if(! app()->environment('local') && $subscription_popup_settings->fields->{app()->getLocale()}->showPopup)
                <popup-subscribe :show-email="@json($subscription_popup_settings->fields->{app()->getLocale()}->showEmail)"
                        action="/{{ app()->getLocale() }}/subscribe"
                        :appear-timeout="2000"
                        popup-image-src="{{ count($subscription_popup_settings->getMedia('popup_image_'.app()->getLocale()))
                                                ? $subscription_popup_settings->getMedia('popup_image_'.app()->getLocale())[0]->getUrl()
                                                : null }}"
                :popup-text="{
                    'title': '{{ $subscription_popup_settings->fields->{app()->getLocale()}->header ?? __('Подпишитесь на обновления') }}',
                    'subTitle': '{{ $subscription_popup_settings->fields->{app()->getLocale()}->body ?? __('Узнавайте первыми о поступлении новинок, а также скидках и специальных предложения!') }}',
                    'submitBtn': '{{ __('Подписаться') }}',
                    'inputLabel': 'E-mail:',
                    'agreementText': '{{ __('Я соглашаюсь с') }} <a href=\'{{ $privacy }}\' target=\'_blank\'>{{ __('политикой конфиденциальности') }}</a>',
                    'successTitle': '{{ __('Подписка оформлена!') }}',
                    'successBtn': '{{ __('Хорошо') }}',
                    'closeBtnLabel': '{{ __('Закрыть попап') }}',
                }"
                :mailing-data="{
                    'userEmail': 'mail@mail.ru'
                }"
                :errors-text="{
                    'emailEmpty':'{{ __('Обязательное поле') }}',
                    'emailFormat':'{{ __('Неверный формат Email') }}',
                }"
        >
            <span class="popup-subscribe__agreement-title">{{ __('Я соглашаюсь с') }} <a href="{{ $privacy }}" target="_blank">{{ __('политикой конфиденциальности') }}</a></span>
        </popup-subscribe>
        @endif
    </footer>
</div>

<div id="update-warning" class="update-warning" style="display: none">
    <p>{{ __('Вы пользуетесь устаревшей версией браузера. Данная версия браузера не поддерживает многие современные технологии, из-за чего страницы отображаются некорректно, а главное — на сайте могут работать не все функции.') }}
        <br>
        {{ __('Рекомендуем установить') }}
        <a href="https://www.google.com/chrome/" target="_blank" rel="nofollow noopener">
            Google Chrome
        </a>
        {{ __('— современный, быстрый и безопасный браузер.') }}
    </p>
    <button class="update-warning__close js--close-update-warning" type="button">
        {{ __('Закрыть') }}
    </button>
</div>

@include('sprite')

<script>
    window.config = @json(\App\JS\JS::jsonVariables(request()));
</script>

<script src="{!! mix('/js/check-support.js') !!}"></script>
<script src="{!! mix('/js/manifest.js') !!}"></script>
<script src="{!! mix('/js/vendor.js') !!}"></script>
<script src="{!! mix('/js/app.js') !!}"></script>

@if(! app()->environment('local') && config('app.jira_collector_id') && optional(auth()->user())->isAdmin())
    <script type="text/javascript" src="https://jira.flagstudio.ru/s/95ad89360eec1845f13b7a13ded5c0c4-T/-y6xh9q/73015/19eec8c46095745849ebdd927f182f88/2.0.23/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=ru-RU&collectorId={{ config('app.jira_collector_id') }}"></script>
@endif

{!! $endScripts ?? '' !!}

</body>
</html>
