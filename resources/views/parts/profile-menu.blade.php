<ul class="profile-menu">
    @for ($i = 0; $i < count($items); $i++)
        <li class="profile-menu__item @if($items[$i]['active']) profile-menu__item--active @endif">
            <a
                    {{$items[$i]['active'] ? '' : 'href=' . $items[$i]['url'] }}
                    class="profile-menu__item-link"
            >
                {{$items[$i]['title']}}
            </a>
        </li>
    @endfor
    <li class="profile-menu__item">
        <a
            href="{{ route('profile.price-list.download') }}"
            class="profile-menu__item-link"
        >
            {{ __('Скачать мой прайс-лист') }}
            <svg width="28" height="24" class="profile-menu__item-icon">
                <use xlink:href="#icon-download"></use>
            </svg>
        </a>
    </li>
</ul>
