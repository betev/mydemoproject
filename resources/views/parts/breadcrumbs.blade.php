@php
    $breadcrumbs = $controllerBreadcrumbs ?? $globalNavigation->breadcrumbs();
@endphp
@if(isset($breadcrumbs) && count($breadcrumbs))
    <nav class="breadcrumbs">
        <div class="breadcrumbs__inner">
            <ul class="breadcrumbs__list">
                @for ($i = 0; $i < count($breadcrumbs); $i++)
                <li class="breadcrumbs__item">
                    <a {{ $breadcrumbs[$i]['url'] ? 'href=' . $breadcrumbs[$i]['url'] : '' }} class="breadcrumbs__link">{{ $breadcrumbs[$i]['title'] }}</a>
                    @if($i < count($breadcrumbs) -1 )
                    <svg class="breadcrumbs__chevron" width="9" height="15">
                        <use xlink:href="#icon-breadcrumbs-chevron"></use>
                    </svg>
                    @endif
                </li>
                @endfor
            </ul>
        </div>
    </nav>
@endif
