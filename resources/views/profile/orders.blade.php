@extends('layouts.app')
@section('content')

    <main id="main" class="main main--profile">

        @include('parts.breadcrumbs')

        <div class="profile">
            <div class="profile__inner">
                <h1 class="profile__title visually-hidden">
                    {{ __('Мои заказы') }}
                </h1>

                <aside class="profile__aside">
                    @include('parts.profile-menu', $menu)
                </aside>

                <section class="profile__main">

                    <orders
                            :titles="{
                                head: {
                                    orderNumber: '{{ __('№ заказа') }}',
                                    orderDate: '{{ __('Дата') }}',
                                    orderSum: '{{ __('Сумма') }}',
                                    orderStatus: '{{ __('Статус') }}',
                                },
                                status: {
                                    confirm: '{{ __('Подтвержден') }}',
                                    not_confirm: '{{ __('Не подтвержден') }}',
                                    paid: '{{ __('Оплачен') }}',
                                    shipped: '{{ __('Отгружен') }}',
                                    cancelled: '{{ __('Отменён') }}'
                                },
                                order: '{{ __('Заказ') }}',
                                fromDate: '{{ __('от') }}',
                                total: '{{ __('Итого') }}',
                                price: '{{ __('Стоимость') }}',
                                downloadBtnText: '{{ __('Скачать счет на оплату') }}',
                                cancelOrderBtn: '{{ __('Отменить заказ') }}',
                                commentTitle: '{{ __('Комментарий к заказу') }}',

                            }"
                            :order-list="{{ $orders }}"
                            :cancel-popup="{
                                title: '{{ __('Вы уверены, что хотите отменить заказ?') }}',
                                confirmBtnTitle: '{{ __('Да, все верно') }}',
                                refuseBtnTitle: '{{ __('Нет') }}',
                            }"
                    ></orders>

                </section>
            </div>
        </div>

    </main>

@endsection
