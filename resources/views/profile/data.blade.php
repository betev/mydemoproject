@extends('layouts.app')
@section('content')

    <main id="main" class="main main--profile">

        @include('parts.breadcrumbs')

        <div class="profile">
            <div class="profile__inner">
                <aside class="profile__aside">
                    @include('parts.profile-menu', $menu)
                </aside>
                <section class="profile__main profile__main--user-data">
                    <p class="profile__registration-date">

                        {{__('profile.Дата регистрации:')}}
                        <span class="profile__registration-date-value">{{auth()->user()->created_at->format('d-m-Y')}}</span>
                    </p>
                    <profile-user-data
                        action="{{$profileUserDataPage->getPrivatePersonActionUrl()}}"
                        type="general"
                        :text='@json($profileUserDataPage->getPrivatePersonText())'
                        :user-fields='@json($profileUserDataPage->getPrivatePersonUserFields())'
                    ></profile-user-data>
                    <profile-user-data
                        action="{{$profileUserDataPage->getLegalEntityActionUrl()}}"
                        type="company"
                        pay-via-company-text="@lang('profile.Покупка на Юрлицо')"
                        :text='@json($profileUserDataPage->getLegalEntityText())'
                        :user-fields='@json($profileUserDataPage->getLegalEntityUserFields())'
                    ></profile-user-data>

                    <p class="profile__addresses-title">
                        {{__('profile.Выбрать адрес доставки')}}
                    </p>
                    {{--В поле validate_rule указываем phone для телефонов и email дла email-лов--}}

                    <profile-address
                        save-action="{{$profileUserDataPage->getAddressSaveActionUrl()}}"
                        delete-action="{{$profileUserDataPage->getAddressDeleteActionUrl()}}"
                        select-default-action="{{$profileUserDataPage->getAddressSelectDefaultActionUrl()}}"
                        :text='@json($profileUserDataPage->getAddressesText())'
                        :addresses='@json($profileUserDataPage->getAddressesFields())'
                        :address-model='@json($profileUserDataPage->getAddressModel())'
                    ></profile-address>
                </section>
            </div>
        </div>

    </main>

@endsection
