@extends('layouts.app')
@section('content')

    <main id="main" class="main main--profile">

        @include('parts.breadcrumbs')

        <div class="profile">
            <div class="profile__inner">
                <h1 class="profile__title visually-hidden">
                    {{ __('Мои новости') }}
                </h1>

                <aside class="profile__aside">
                    @include('parts.profile-menu', $menu)
                </aside>

                <section class="profile__main">

                    <price-list
                            :labels="{
                                priceList: '{{ __('Прайс-лист') }}',
                                name: '{{ __('Товар') }}',
                                information: '{{ __('Информация') }}',
                                price: '{{ __('Цена') }}',
                                pricePerMeter: '{{ __('Цена за метр') }}',
                                pricePerRoll: '{{ __('Цена за рулон') }}',
                            }"
                            :filters-raw="[
                                {
                                    title: '{{ __('Ткани') }}',
                                    key: 'textile',
                                    active: true,
                                },
                                {{--
                                {
                                    title: '{{ __('Ковры') }}',
                                    key: 'carpets',
                                    active: false,
                                },
                                {
                                    title: '{{ __('Комплектующие') }}',
                                    key: 'parts',
                                    active: false,
                                },
                                --}}
                            ]"
                            :prices="{
                                textile: {{ json_encode($fabrics) }},
                                carpets: {{ json_encode($carpets) }},
                                parts: {{ json_encode($components) }},
                            }"
                    >
                    </price-list>

                </section>
            </div>
        </div>

    </main>

@endsection
