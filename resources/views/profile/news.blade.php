@extends('layouts.app')
@section('content')

    <main id="main" class="main main--profile">

        @include('parts.breadcrumbs')

        <div class="profile">
            <div class="profile__inner">
                <h1 class="profile__title visually-hidden">
                    {{ __('Мои новости') }}
                </h1>

                <aside class="profile__aside">
                    @include('parts.profile-menu', $menu)
                </aside>

                <section class="profile__main">

                    <my-news
                            :actions="{
                                loadMore: '{{ $news->nextPageUrl() }}',
                            }"
                            :labels="{
                                openNews: '{{ __('Развернуть или свернуть новость') }}',
                            }"

                            :my-news='@json($news)'
                    ></my-news>

                </section>
            </div>
        </div>

    </main>

@endsection
