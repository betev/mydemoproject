@extends('layouts.app')
@section('content')

    @php
        $locale = app()->getLocale();
    @endphp

    <main id="main" class="main">
        @if(($cards = $mainPage->getCards($locale)) && count($cards))
            <section class="cards">
                <div class="cards__inner">
                    <div class="cards__wrap cards__wrap--{{ count($cards) }}">

                        @foreach($cards as $card)
                            <div class="cards__item">

                                <img
                                        loading="lazy"
                                        src="{{ $card['imgSrc'] }}"
                                        class="cards__image"
                                        alt="{{ $card['btnTitle'] }}"
                                >

                                <a href="/{{ $locale }}{{ $card['link'] }}" class="btn cards__btn">{{ $card['btnTitle'] }}</a>

                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

        @if (($cards = $mainPage->getSectionTwo($locale)) && $cards['header'])
            @php $header = $cards['header'];
                unset($cards['header']); @endphp
            @if (count($cards)) {
            <section class="advantages">
                <div class="advantages__inner">
                    <h2 class="advantages__title">{{ $header }}</h2>


                    <div class="advantages__cards-wrap">

                        @foreach($cards as $card)
                            <div class="advantages__card">
                                <div class="advantages__card-img-wrap">
                                    <img
                                            loading="lazy"
                                            src="{{ $card['imgSrc'] }}"
                                            alt="advantages"
                                            class="advantages__card-img"
                                            width="360"
                                            height="275"
                                    >
                                </div>

                                <p class="advantages__card-description">
                                    {{ $card['block_two_text'] }}
                                </p>
                            </div>
                        @endforeach


                    </div>
                </div>
            </section>
            @endif
        @endif

        @php
            $default_image = [
                '1x' => asset('images/product-default-img@2x-min.jpg'),
                '2x' => asset('images/product-default-img@3x-min.jpg'),
            ];
        @endphp

        @if (($hits = $mainPage->getSaleHitCards($locale)) && count($hits))
            <?php //dd($hits); ?>
            <sales-hits
                    title="{{ $mainPage->getSectionThree($locale)['header'] }}"
                    sub-title="{{ $mainPage->getSectionThree($locale)['subheader'] }}"
                    :carousel-buttons-labels="{
                    prev: 'Предыдущие товары',
                    next: 'Следующие товары',
                }"
                    :cards='@json($hits)'
                    :product-image-default='@json($default_image)'
            >
            </sales-hits>
        @endif

    </main>

@endsection
