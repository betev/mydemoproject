<!doctype html>
<html style="width: 100%;">
    <head>
        <title>Счет на оплату</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            html {
                font-family: Arial, sans-serif;
            }
            table {
                border-collapse: collapse;
            }
            .bill-layout__head {
                margin-bottom: 5px;
            }
            .bill-layout__head-content {
                border: 1px solid #000000;
                text-align: center;
                font-size: 11px;
            }
            .bill-layout__requisites {
                border: 1px solid #000000;
            }
            .bill-layout__requisites-param-title {
                font-size: 11px;
                font-weight: 400;
            }
            .bill-layout__recipient {
                font-weight: 600;
                text-transform: uppercase;
                font-family: "Times New Roman", serif;
                font-size: 12px;
            }
            .bill-layout__requisites-bank {
                font-weight: 600;
                font-family: "Times New Roman", serif;
                font-size: 12px;
            }
            .bill-layout__requisites-title {
                text-align: center;
                font-size: 13px;
                font-weight: 600;
            }
            .bill-layout__requisites-bordered-cell {
                display: inline-block;
                padding: 8px 37px;
                border: 2px solid;
                font-weight: 600;
                font-size: 12px;
            }
            .bill-layout__requisites-bordered-cell-small-paddings {
                display: inline-block;
                padding: 8px 18px;
                border: 2px solid;
                font-family: "Times New Roman", serif;
                font-weight: 600;
                font-size: 12px;
            }
            .bill-layout__document-title {
                font-size: 18px;
                font-weight: 600;
            }
            .bill-layout__requisites-param-title-underline {
                font-size: 12px;
                text-decoration: underline;
            }
            .bill-layout__sides-requisites-title {
                font-size: 13px;
                font-weight: 700;
            }
            .bill-layout__sides-requisites-text {
                padding-top: 2px;
                padding-left: 13px;
                font-size: 12px;
            }
            .bill-layout__sides-requisites-text > p {
                margin: 0;
            }
            .bill-layout__data-table {
                width: 100%;
                border: 2px solid #000000;
            }
            .bill-layout__data-table th {
                padding: 1px 3px;
                background-color: #eeeeee;
                border-bottom: 1px solid #000000;
                border-right: 1px solid #000000;
                font-size: 13px;
                font-weight: 600;
            }
            .bill-layout__data-table td {
                font-size: 12px;
                border-bottom: 1px solid #000000;
                border-right: 1px solid #000000;
            }
            .bill-layout__total-table {
                font-size: 12px;
                font-weight: 600;
            }
            .bill-layout__total-table td {
                text-align: right;
            }
        </style>
    </head>
    <body style="padding: 35px;">
        <table class="bill-layout__head">
            <tr>
                <td style="width: 5%"></td>
                <td style="width: 90%" class="bill-layout__head-content">
                    Увага! Оплата цього рахунку означає погодження з умовами поставки товарів. Повідомлення про оплату є обов'язковим,
                    в іншому випадку не гарантується наявність товарів на складі. Товар відпускається за фактом надходження коштів на п/р
                    Постачальника, самовивозом, за наявності довіреності та паспорта.
                </td>
                <td style="width: 5%"></td>
            </tr>
        </table>
        <div class="bill-layout__requisites">
            <table>
                <tr>
                    <td
                        colspan="3"
                        style="width: 100%"
                        class="bill-layout__requisites-title"
                    >
                        Зразок заповнення платіжного доручення
                    </td>
                </tr>
                <tr>
                    <td style="width: 8%" class="bill-layout__requisites-param-title">
                        Одержувач
                    </td>
                    <td style="width: 42%" class="bill-layout__recipient">
                        {{ $company->fields->ua->name }}
                    </td>
                    <td style="width: 50%"></td>
                </tr>
                <tr>
                    <td style="width: 8%" class="bill-layout__requisites-param-title">
                        Код
                    </td>
                    <td style="width: 42%" class="bill-layout__recipient">
                        <span class="bill-layout__requisites-bordered-cell">
                            {{ $company->fields->ua->edrpou }}
                        </span>
                    </td>
                    <td style="width: 50%"></td>
                </tr>
            </table>
            <table style="width: 100%; margin-bottom: 4px">
                <tr>
                    <td
                        colspan="3"
                        style="width: 100%"
                        class="bill-layout__requisites-param-title"
                    >
                        Банк одержувача
                    </td>
                </tr>
                <tr>
                    <td style="width: 30%" class="bill-layout__requisites-param-title">
                        <span class="bill-layout__requisites-bank">
                            {{ $company->fields->ua->bank_name }}
                        </span>

                    </td>
                    <td style="width: 25%;" align="right">
                        <div style="display: inline-block; margin-top: -3px; margin-right: 10px">
                            <div style="text-align: left;">
                                <span class="bill-layout__requisites-param-title">
                                    Код банку
                                </span>
                            </div>
                            <div>
                                <span
                                    class="bill-layout__requisites-bordered-cell-small-paddings"
                                >
                                    {{ $company->fields->ua->bik }}
                                </span>
                            </div>
                        </div>
                    </td>
                    <td style="width: 45%" class="bill-layout__recipient">
                        <div>
                            <div>
                                <span class="bill-layout__requisites-param-title">
                                    КРЕДИТ рах. N
                                </span>
                            </div>
                            <div>
                                <span class="bill-layout__requisites-bordered-cell-small-paddings">
                                    {{ $company->fields->ua->account }}
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <table style="width: 100%; margin-top: 30px;">
            <tr>
                <td colspan="2" style="border-bottom: 2px solid #000000; padding-bottom: 3px;">
                    <span class="bill-layout__document-title">
                        Рахунок на оплату № {{ $order->id }} від {{ $order->created_at->format('d-m-Y') }}
                    </span>
                </td>
            </tr>
            <tr>
                <td style="width: 14%; padding-top: 10px; vertical-align: baseline;">
                    <span class="bill-layout__requisites-param-title-underline">
                        Постачальник:
                    </span>
                </td>
                <td style="width: 86%; padding-top: 10px;">
                    <div class="bill-layout__sides-requisites-title">
                        {{ $company->fields->ua->name }}
                    </div>
                    <div class="bill-layout__sides-requisites-text">
                        <p>П/р {{ $company->fields->ua->account }}, {{ $company->fields->ua->bank_name }}, МФО 320627</p>
                        <p>{{ $company->fields->ua->address }}</p>
                        <p>код за ЄДРПОУ {{ $company->fields->ua->edrpou }}, ІПН {{ $company->fields->ua->inn }}</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 14%; vertical-align: baseline;">
                    <span class="bill-layout__requisites-param-title-underline">
                        Покупець:
                    </span>
                </td>
                <td style="width: 86%;">
                    <div class="bill-layout__sides-requisites-title">
                        @foreach ($order->user->fields->legal_entity as $field)
                            @if ($field->name == 'nazvanie')
                                {{ $field->value }}
                            @endif
                        @endforeach
                    </div>
                </td>
            </tr>
        </table>
        <div style="font-size: 12px; margin-top: 20px; margin-bottom: 6px;">
            Договор:
            <span style="font-size: 13px; padding-left: 30px;">
                № 901-З від 09.01.2019
            </span>
        </div>

        @php
            $table_data = [
                [
                    'index' => 1,
                    'title' => 'Тканина меблева в асорт KENGOO',
                    'count' => '175,2',
                    'dimension' => 'пог.м',
                    'price' => '145,00',
                    'total' => '25 404,00',
                ],[
                    'index' => 2,
                    'title' => 'Тканина меблева в асорт MERCAN',
                    'count' => '31 п',
                    'dimension' => 'пог.м',
                    'price' => '150,00',
                    'total' => '4 650,00',
                ],[
                    'index' => 3,
                    'title' => 'Тканина меблева в асорт TORINO',
                    'count' => '127,9 п',
                    'dimension' => 'пог.м',
                    'price' => '145,00',
                    'total' => '18 801,30',
                ],
            ];
                $fmt = (new NumberFormatter('uk', NumberFormatter::SPELLOUT));
                $fmt->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-cardinal-feminine");
                $vat = round($order->price * .2, 2)
        @endphp

        <table class="bill-layout__data-table">
            <tr>
                <th>
                    №
                </th>
                <th>
                    Товари (роботи, послуги)
                </th>
                <th colspan="2">
                    Кількість
                </th>
                <th>
                    Ціна з ПДВ
                </th>
                <th>
                    Сума з ПДВ
                </th>
            </tr>
            @foreach($order->items as $row)
                <tr>
                    <td style="width: 7%; text-align: center;">{{ $row->product->code }}</td>
                    <td style="width: 59%; text-align: left;">{{ $row->product->name }}</td>
                    <td style="width: 8%; text-align: right;">{{ $row->quantity }}</td>
                    <td style="width: 6%; text-align: left;">{{ $row->unit }}</td>
                    <td style="width: 9%; text-align: right;">{{ $row->price }}</td>
                    <td style="width: 11%; text-align: right;">{{ $row->price * $row->quantity }}</td>
                </tr>
            @endforeach
        </table>
        <table
            style="width: 100%; margin-top: 8px; margin-bottom: 14px;"
            class="bill-layout__total-table"
        >
            <tr>
                <td style="width: 66%;">Разом:</td>
                <td style="width: 34%;">{{ $order->price }}</td>
            </tr>
            <tr>
                <td style="width: 66%;">У тому числі ПДВ: </td>
                <td style="width: 34%;">{{ $vat }}</td>
            </tr>
        </table>
        <div style="font-size: 11px;">Всього найменувань 3, на суму {{ $order->price }} грн.</div>
        <div style="font-weight: 600; font-size: 13px;">
            @php
            @endphp
            {{ \Illuminate\Support\Str::ucfirst($fmt->format(floor($order->price))) }}
            гривень
            {{ str_pad(round(($order->price - floor($order->price)) * 100), 2, 0, STR_PAD_LEFT) }}
            копійок
        </div>
        <div style="font-weight: 600; font-size: 13px;">
            У т.ч. ПДВ: {{ \Illuminate\Support\Str::ucfirst($fmt->format(floor($vat)), 1) }} гривень
            {{ str_pad(round(($vat - floor($vat)) * 100), 2, 0, STR_PAD_LEFT) }} копійок
        </div>
        <hr style="width: 100%; height: 2px; border: none; background-color: #000000; margin: 9px 0 11px;">
        <table style="width: 100%;">
            <tr>
                <td style="width: 50%;"></td>
                <td style="width: 50%; border-bottom: 1px solid #000000; position: relative">
                    <span style="font-weight: 600; font-size: 13px;">Виписав(ла):</span>
                    <img
                        style="position: absolute; left: 130px; top: -15px;"
                        width="70"
                        src="{{ $company->getFirstMedia('invoice_stamp_ua')
                                    ? $company->getFirstMedia('invoice_stamp_ua')->getPath()
                                    : '' }}"
                    >
                    <img
                        style="position: absolute; right: 55px; top: -25px;"
                        width="110"
                        src="{{ $company->getFirstMedia('invoice_signature_ua')
                                    ? $company->getFirstMedia('invoice_signature_ua')->getPath()
                                    : '' }}"
                    >
                </td>
            </tr>
        </table>
    </body>
</html>
