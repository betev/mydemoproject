<!doctype html>
<html style="width: 100%;">
<head>
    <title>Счет на оплату</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style>
      html {
        font-family: "Times New Roman", sans-serif;
        font-size: 11px;
        line-height: 1.5;
        font-weight: bold;
      }

      table {
        border-collapse: collapse;
      }

      .bill {
        width: 100%;
      }

      .bill__address {
        padding: 2px;
        line-height: 1.4;
        font-size: 12px;
      }

      .bill__address-name {
        margin-left: 10px;
        top: -10px;
        padding-left: 5px;
        padding-right: 5px;
        position: relative;
        display: inline-block;
        background: white;
      }

      .bill__invoice-row {
        padding-left: 10px;
        border: 0;
      }

      .bill__invoice-row div {
        display: inline-block;
        width: 100%;
        text-align: center;
        font-size: 18px;
        background: #C0C0C0;
      }

      .bill__invoice-coupon {
        padding-left: 10px;
        padding-bottom: 20px;
      }

      .bill__invoice-coupon div {
        display: inline-block;
        width: 100%;
        text-align: center;
        font-size: 16px;
      }

      .bill__bank {
        padding-top: 10px;
        padding-bottom: 20px;
      }

      .bill__invoice-date-text {
        padding-left: 10px;
        font-weight: 400;
      }

      .bill__invoice-date {
        text-align: right;
      }

      .bill__invoice-page {
        font-weight: 400;
      }

      .bill__col {
        padding-bottom: 20px;
      }

      .bill__col-shift {
        padding-left: 10px;
        padding-bottom: 20px;
      }

      .bill_items-table {
        width: 100%;
      }

      .bill_items-table table {
        width: 100%;
      }

      .bill_items-table td {
        padding: 2px;
      }
    </style>
</head>
<body style="padding: 50px 35px;">
<table border="0" cellspacing="0" cellpadding="0" class="bill">
    <tbody>
    <tr>
        <td colspan="7" rowspan="4" class="bill__address">
            <table style="width: 100%; border: 1px solid #000000;">
                <tr>
                    <td>
                        <div class="bill__address-name">Sprzedawca</div><br/>
                        TEXTORIA SP&Oacute;ŁKA Z OGRANICZONĄ<br/>
                        ODPOWIEDZIALNOŚCIĄ<br/>
                        ul. Wrzesińska 84<br/>
                        62-020 Swarzędz<br/>
                        NIP: PL 7010424981
                    </td>
                </tr>
            </table>
        </td>
        <td colspan="14" class="bill__invoice-row">
            <div>Faktura VAT</div>
        </td>
    </tr>

    <tr>
        <td colspan="14" class="bill__invoice-coupon">
            <div>nr FA/362/03/2021/KUPON</div>
        </td>
    </tr>
    <tr>
        <td colspan="7" rowspan="2" class="bill__invoice-date-text">
            Data wystawienia:<br/>
            Data dostawy / wykonania usługi:
        </td>
        <td colspan="7" rowspan="2" class="bill__invoice-date">
            2021-03-01<br/>
            2021-03-01
        </td>
    </tr>
    <tr>
    </tr>
    <tr>
        <td colspan="17"></td>
        <td colspan="3" class="bill__invoice-page">Strona:</td>
        <td colspan="1">1/1</td>
    </tr>
    <tr>
        <td colspan="21" class="bill__bank">Bank: Raiffeisen Bank Polska S.A.w Warszawie Nr rachunku: 04 1750 0012 0000 0000 3151 6398</td>
    </tr>
    <tr>
        <td colspan="7" class="bill__col">
            Nabywca:<br>
            WIESŁAW STACHOWIAK "DANKERT"
        </td>
        <td colspan="14" class="bill__col-shift">
            Odbiorca:<br>
            WIESŁAW STACHOWIAK "DANKERT"
        </td>
    </tr>
    <tr>
        <td colspan="7" class="bill__col">
            Leszczyńska 79/2<br>
            60-103 Poznań<br>
            NIP: 7831281548<br>
        </td>
        <td colspan="14" class="bill__col-shift">
            Leszczyńska 79/2<br>
            60-103 Poznań<br>
            NIP: 7831281548<br>
        </td>
    </tr>

    <tr>
        <td colspan="21" class="bill_items-table">
            <table>
                <tr style="border: 1px solid #000">
                    <td>Lp.</td>
                    <td>Nazwa towaru/usługi</td>
                    <td>Kod CN/ PKWiU</td>
                    <td>Ilość</td>
                    <td>J.m.</td>
                    <td>VAT</td>
                    <td>Cena netto</td>
                    <td style="text-align: right">Wartość netto</td>
                </tr>
                <tr style="font-weight: 400; font-size: 10px;">
                    <td>1</td>
                    <td>RANGO 13 ORANGE</td>
                    <td></td>
                    <td>3,8</td>
                    <td>m</td>
                    <td>23 %</td>
                    <td>13,50</td>
                    <td style="text-align: right">51,30</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr style="font-size: 12px;">
        <td colspan="21" class="bill_items-table">
            <table>
                <tr style="font-weight: 400; background: #C0C0C0;">
                    <td>Forma płatności</td>
                    <td>Termin</td>
                    <td>Kwota</td>
                    <td>Waluta</td>
                    <td colspan="2" style="text-align: center">Stawka</td>
                    <td>Netto</td>
                    <td>VAT</td>
                    <td style="text-align: right">Brutto</td>
                </tr>
                <tr style="font-weight: 400">
                    <td>przedpłata</td>
                    <td>2021-03-01</td>
                    <td>63,10</td>
                    <td>PLN</td>
                    <td style="background: #C0C0C0;">Razem:</td>
                    <td style="border-bottom: 1px solid #000;"></td>
                    <td style="font-weight: bold; border-bottom: 1px solid #000;">51,30</td>
                    <td style="font-weight: bold; border-bottom: 1px solid #000;">11,80</td>
                    <td style="text-align: right; font-weight: bold; border-bottom: 1px solid #000;">63,10</td>
                </tr>
                <tr style="font-weight: 400">
                    <td colspan="4"></td>
                    <td style="background: #C0C0C0;">W tym:</td>
                    <td>23%</td>
                    <td>51,30</td>
                    <td>11,80</td>
                    <td style="text-align: right">63,10</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan="21" style="padding-top: 30px"></td>
    </tr>

    <tr style="background: #C0C0C0;">
        <td colspan="10" style="padding-left: 2px; font-size: 12px;">Razem do zapłaty</td>
        <td colspan="11" style="text-align: right; padding-right: 10px; font-size: 12px;">63,10 PLN</td>
    </tr>

    <tr>
        <td colspan="21" style="text-align: right; font-style: italic; font-weight: 400; padding-right: 10px; font-size: 12px;">
            Słownie : sześćdziesiąt trzy PLN 10/100
        </td>
    </tr>

    <tr style="font-weight: 400; font-size: 12px; border-bottom: 1px solid #000;">
        <td colspan="10" style="padding-left: 2px">Zapłacono: 63,10 PLN</td>
        <td colspan="11" style="text-align: right; padding-right: 10px">Pozostaje: 0,00 PLN</td>
    </tr>

    <tr>
        <td colspan="21">
            <table style="width: 100%; padding-top: 5px; font-size: 12px;">
                <tr>
                    <td style="width: 30%; border-bottom: 1px solid #000; text-align: center;">
                        Katarzyna Nowicka
                    </td>
                    <td style="width: 10%;">

                    </td>
                    <td style="width: 20%; border-bottom: 1px solid #000;">

                    </td>
                    <td style="width: 10%;">

                    </td>
                    <td style="width: 30%; border-bottom: 1px solid #000;">

                    </td>
                </tr>

                <tr style="font-size: 11px; text-align: center; font-weight: 400;">
                    <td style="padding-top: 10px;">
                        Podpis osoby uprawnionej do wystawienia faktury
                    </td>
                    <td style="padding-top: 10px;">

                    </td>
                    <td style="padding-top: 10px;">
                        Data odbioru
                    </td>
                    <td style="padding-top: 10px;">

                    </td>
                    <td style="padding-top: 10px;">
                        Podpis osoby uprawnionej do odbioru faktury
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan="21" style="padding-top: 40px; font-weight: 400; text-align: right;">Comarch ERP Optima, v. 2021.4.1.1250</td>
    </tr>
    </tbody>
</table>
</body>
</html>

