@extends('layouts.app')
@section('content')

    <main id="main" class="main">
        @include('parts.breadcrumbs')

        <div class="checkout-page">
            <div class="checkout-page__inner">
                <section class="checkout-page__top-link-wrap">
                    <a href="/{{ app()->getLocale() }}/cart" class="checkout-page__link-to-back">
                        <i class="checkout-page__link-to-back-icon"></i>
                        {{ __('К корзине') }}
                    </a>
                </section>
                <section class="checkout-page__content">

                    <checkout-form
                        action="/{{ app()->getLocale() }}/create-order"
                        :text='@json($text)'
                        :errors-text='@json($errors_text)'
                        :pickup-offices-options='@json($pickup_offices_options)'
                        :delivery-addresses-options='@json($delivery_addresses_options)'
                        :company-fields='@json($company_fields)'
                        :delivery-fields='@json($delivery_fields)'
                        price="{{ $price }}"
                        :default-delivery-address='@json($default_delivery_address)'
                    >
                        <template v-slot:lead-time-text>
                            <p class="checkout-form__text checkout-form__text--lead">
                                {{ __('Срок подготовки отреза необходимого метража ткани 3 рабочих дня') }}
                            </p>
                        </template>

                        <template v-slot:consent-text>
                            <p class="checkout-form__text checkout-form__text--consent">
                                {{ __('Нажимая на кнопку «Оформить заказ»,
                                вы принимаете условия ') }}<a href="{{ $contract }}" target="_blank">{{ __('публичной оферты') }}</a>
                            </p>
                        </template>
                    </checkout-form>
                </section>
            </div>
        </div>

    </main>

@endsection
