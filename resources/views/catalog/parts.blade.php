@extends('layouts.app')
@section('content')

    <main id="main" class="main">

        @include('parts.breadcrumbs')

        @php
            $default_image = [
                '1x' => asset('images/product-default-img@2x-min.jpg'),
                '2x' => asset('images/product-default-img@3x-min.jpg'),
            ];
        @endphp

        <catalog-page
                title="{{ $title }}"
                load-more-btn-title="{{ $loadButton }}"
                :actions="{{ json_encode($actions) }}"
                :sorting="{{ json_encode($sorting) }}"
                :catalog-list-data="{{ json_encode($catalog) }}"
                :filter-data="{{ json_encode($filters) }}"
                :product-image-default='@json($default_image)'
        >
        </catalog-page>

    </main>

@endsection
