@extends('layouts.app')

@section('content')
<?php
    $mainSection = $page->getMainSection();
    $knowledgeBase = $page->getKnowledgeBase();
    $educationMethods = $page->getEducationalMethods();
    $formDetails = $page->getContactFormData();
?>
<main id="main" class="main">
    @include('parts.breadcrumbs')
    <div class="academy-page">
        <div class="academy-page__inner">
            @if($mainSection)
                <section class="academy-page__description">
                    <h1 class="academy-page__title">{{ $mainSection['header'] }}</h1>
                    <div class="academy-page__description-content">
                        <img
                            src="{{ $mainSection['imageUrl'] }}"
                            width="623"
                            height="418"
                            alt="{{ $mainSection['header'] }}"
                            class="academy-page__description-img"
                        />
                        <div
                            class="academy-page__description-text-box"
                             @if ($mainSection['bgColor']) style="background-color: {{ $mainSection['bgColor'] }}" @endif
                        >
                            <p class="academy-page__description-text">
                                {{ $mainSection['description'] }}
                            </p>
                        </div>
                    </div>
                </section>
            @endif
            @if($knowledgeBase)
                <section class="academy-page__knowledge-base">
                    <h2 class="academy-page__knowledge-base-title">{{ $knowledgeBase['header'] }}</h2>
                    <div class="academy-page__knowledge-base-cards">
                        @foreach($knowledgeBase['items'] as $card)
                            <article class="academy-page__knowledge-base-card">
                                <img
                                    src="{{ $card->image }}"
                                    width="70"
                                    height="70"
                                    alt=""
                                    class="academy-page__knowledge-base-card-img"
                                />
                                <div class="academy-page__knowledge-base-card-text-box">
                                    <h3 class="academy-page__knowledge-base-card-title">
                                        {{ $card->cardTitle }}
                                    </h3>
                                    <p class="academy-page__knowledge-base-card-description">
                                        {{ $card->cardDescription }}
                                    </p>
                                </div>
                                @if($card->singleLink != '0')
                                    <a
                                        href="{{ route('academy.courses.show', ['course' => $card->singleLink]) }}"
                                        class="academy-page__knowledge-base-card-single-link"
                                    ></a>
                                @elseif (count($card->links))
                                    <ul class="academy-page__knowledge-base-card-links-list">
                                        @foreach($card->links as $link)
                                            <li class="academy-page__knowledge-base-card-link-box">
                                                <a
                                                    class="academy-page__knowledge-base-card-link"
                                                    href="{{ $link->link }}"
                                                >
                                                        {{ $link->title }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </article>
                        @endforeach
                    </div>
                </section>
            @endif
            @if($educationMethods)
                <section class="academy-page__education-methods">
                    <h2 class="academy-page__education-methods-title">{{ $educationMethods['header'] }}</h2>
                    <div class="academy-page__education-methods-cards">
                        @foreach($educationMethods['items'] as $card)
                            <article class="academy-page__education-methods-card">
                                <object
                                    type="image/svg+xml"
                                    data="{{ $card->image }}"
                                    width="70"
                                    height="70"
                                    class="academy-page__education-methods-card-img"
                                >
                                    <img
                                        src="{{ $card->image }}"
                                        width="70"
                                        height="70"
                                        alt=""
                                        class="academy-page__education-methods-card-img"
                                    />
                                </object>
                                <h3 class="academy-page__education-methods-card-title">
                                    {{ $card->cardTitle }}
                                </h3>
                                <p class="academy-page__education-methods-card-description">
                                    {{ $card->cardDescription }}
                                </p>
                                </article>
                        @endforeach
                    </div>
                </section>
            @endif
            @if($formDetails)
                <academy-form
                    :form-details='@json($formDetails)'
                    send-form-action="{{ route('courses.apply') }}"
                >
                </academy-form>
            @endif
        </div>
    </div>
</main>
@endsection
