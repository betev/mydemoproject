@extends('layouts.app')
@section('content')
    <main id="main" class="main">
        @include('parts.breadcrumbs')
        <div class="academy-page">
            <div class="academy-page__inner">
                <section class="academy-page__description">
                    <h1 class="academy-page__title">{{ $course['title'] }}</h1>
                    <div class="academy-page__description-content">
                        <img
                            src="{{ $course['image_url'] }}"
                            width="623"
                            height="418"
                            alt="изображение"
                            class="academy-page__description-img"
                        />
                        <div
                            class="academy-page__description-text-box"
                        >
{{--                            <p class="academy-page__description-text">--}}
                                {!! $course['image_description'] !!}
{{--                            </p>--}}
                        </div>
                    </div>
                </section>
{{--                <section>--}}
{{--                    <h2 class="academy-page__knowledge-base-title">{{ $knowledgeBase['header'] }}</h2>--}}
{{--                </section>--}}
                <section>
                    <h2 class="academy-page__knowledge-base-title">{{ $course['property_header'] }}</h2>
                </section>
{{--                @if($knowledgeBase)--}}
{{--                    <section class="academy-page__knowledge-base">--}}
{{--                        <h2 class="academy-page__knowledge-base-title">{{ $knowledgeBase['header'] }}</h2>--}}
{{--                        <div class="academy-page__knowledge-base-cards">--}}
{{--                            @foreach($knowledgeBase['items'] as $card)--}}
{{--                                <article class="academy-page__knowledge-base-card">--}}
{{--                                    <img--}}
{{--                                        src="{{ $card->image }}"--}}
{{--                                        width="70"--}}
{{--                                        height="70"--}}
{{--                                        alt=""--}}
{{--                                        class="academy-page__knowledge-base-card-img"--}}
{{--                                    />--}}
{{--                                    <div class="academy-page__knowledge-base-card-text-box">--}}
{{--                                        <h3 class="academy-page__knowledge-base-card-title">--}}
{{--                                            {{ $card->cardTitle }}--}}
{{--                                        </h3>--}}
{{--                                        <p class="academy-page__knowledge-base-card-description">--}}
{{--                                            {{ $card->cardDescription }}--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                    @if (count($card->links))--}}
{{--                                        <ul class="academy-page__knowledge-base-card-links-list">--}}
{{--                                            @foreach($card->links as $link)--}}
{{--                                                <li class="academy-page__knowledge-base-card-link-box">--}}
{{--                                                    <a--}}
{{--                                                        class="academy-page__knowledge-base-card-link"--}}
{{--                                                        href="{{ $link->link }}"--}}
{{--                                                    >--}}
{{--                                                        {{ $link->title }}--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            @endforeach--}}
{{--                                        </ul>--}}
{{--                                    @endif--}}
{{--                                </article>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </section>--}}
{{--                @endif--}}
{{--                @if($educationMethods)--}}
{{--                    <section class="academy-page__education-methods">--}}
{{--                        <h2 class="academy-page__education-methods-title">{{ $educationMethods['header'] }}</h2>--}}
{{--                        <div class="academy-page__education-methods-cards">--}}
{{--                            @foreach($educationMethods['items'] as $card)--}}
{{--                                <article class="academy-page__education-methods-card">--}}
{{--                                    <object--}}
{{--                                        type="image/svg+xml"--}}
{{--                                        data="{{ $card->image }}"--}}
{{--                                        width="70"--}}
{{--                                        height="70"--}}
{{--                                        class="academy-page__education-methods-card-img"--}}
{{--                                    >--}}
{{--                                        <img--}}
{{--                                            src="{{ $card->image }}"--}}
{{--                                            width="70"--}}
{{--                                            height="70"--}}
{{--                                            alt=""--}}
{{--                                            class="academy-page__education-methods-card-img"--}}
{{--                                        />--}}
{{--                                    </object>--}}
{{--                                    <h3 class="academy-page__education-methods-card-title">--}}
{{--                                        {{ $card->cardTitle }}--}}
{{--                                    </h3>--}}
{{--                                    <p class="academy-page__education-methods-card-description">--}}
{{--                                        {{ $card->cardDescription }}--}}
{{--                                    </p>--}}
{{--                                </article>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </section>--}}
{{--                @endif--}}
{{--                @if($formData)--}}
{{--                    <academy-form--}}

{{--                    />--}}
{{--                @endif--}}
            </div>
        </div>
    </main>
@endsection
