@extends('layouts.app')
@section('content')
{{--    @php dd($course) @endphp--}}
    <main id="main" class="main">
        @include('parts.breadcrumbs')
        <div class="academy-page">
            <div class="academy-page__inner">
                <section class="course-page__description-box">
                    <h1 class="academy-page__title">{{ $course['page_header'] }}</h1>
                    <div class="academy-page__description-content">
                        <img
                            src="{{ $course['image_url'] }}"
                            width="623"
                            height="418"
                            alt="{{ $course['image_description'] }}"
                            class="academy-page__description-img"
                        />
                        <div
                            class="course-page__description-box-text"
                        >
                            {!! $course['image_block_text'] !!}
                        </div>
                    </div>
                </section>
                <section class="course-page__text-block">
                    <h2 class="academy-page__knowledge-base-title">{{ $course['title'] }}</h2>
                    <div
                        class="course-page__description-box-text"
                    >
                        {!! $course['description'] !!}
                    </div>
                </section>
                <div class="curse-page__video-box {{ 'curse-page__video-box--' . $course['video_alignment'] }}"> {{-- left, right, center, full_width --}}
                    {!! $course['link_to_video'] !!}
                </div>
                <section>
                    <h2 class="academy-page__knowledge-base-title">{{ $course['property_header'] }}</h2>
                    <div
                        class="course-page__properties-list"
                    >
                        @foreach($course['properties'] as $property)
                            <article class="course-page__property">
                                <img
                                    src="{{ $property['iconUrl'] }}"
                                    width="110"
                                    height="110"
                                    alt="{{ $property['header'] }}"
                                    aria-hidden="true"
                                    class="course-page__property-icon"
                                />
                                <h3 class="course-page__property-title">
                                    {{ $property['header'] }}
                                </h3>
                                <div class="course-page__property-description-box">
                                    {!! $property['description'] !!}
                                </div>
                            </article>
                        @endforeach
                                                    <p class="academy-page__description-text">
{{--                        {!! $course['property_description'] !!}--}}
                        {{--                            </p>--}}
                    </div>
                </section>
                {{--                @if($knowledgeBase)--}}
                {{--                    <section class="academy-page__knowledge-base">--}}
                {{--                        <h2 class="academy-page__knowledge-base-title">{{ $knowledgeBase['header'] }}</h2>--}}
                {{--                        <div class="academy-page__knowledge-base-cards">--}}
                {{--                            @foreach($knowledgeBase['items'] as $card)--}}
                {{--                                <article class="academy-page__knowledge-base-card">--}}
                {{--                                    <img--}}
                {{--                                        src="{{ $card->image }}"--}}
                {{--                                        width="70"--}}
                {{--                                        height="70"--}}
                {{--                                        alt=""--}}
                {{--                                        class="academy-page__knowledge-base-card-img"--}}
                {{--                                    />--}}
                {{--                                    <div class="academy-page__knowledge-base-card-text-box">--}}
                {{--                                        <h3 class="academy-page__knowledge-base-card-title">--}}
                {{--                                            {{ $card->cardTitle }}--}}
                {{--                                        </h3>--}}
                {{--                                        <p class="academy-page__knowledge-base-card-description">--}}
                {{--                                            {{ $card->cardDescription }}--}}
                {{--                                        </p>--}}
                {{--                                    </div>--}}
                {{--                                    @if (count($card->links))--}}
                {{--                                        <ul class="academy-page__knowledge-base-card-links-list">--}}
                {{--                                            @foreach($card->links as $link)--}}
                {{--                                                <li class="academy-page__knowledge-base-card-link-box">--}}
                {{--                                                    <a--}}
                {{--                                                        class="academy-page__knowledge-base-card-link"--}}
                {{--                                                        href="{{ $link->link }}"--}}
                {{--                                                    >--}}
                {{--                                                        {{ $link->title }}--}}
                {{--                                                    </a>--}}
                {{--                                                </li>--}}
                {{--                                            @endforeach--}}
                {{--                                        </ul>--}}
                {{--                                    @endif--}}
                {{--                                </article>--}}
                {{--                            @endforeach--}}
                {{--                        </div>--}}
                {{--                    </section>--}}
                {{--                @endif--}}
                {{--                @if($educationMethods)--}}
                {{--                    <section class="academy-page__education-methods">--}}
                {{--                        <h2 class="academy-page__education-methods-title">{{ $educationMethods['header'] }}</h2>--}}
                {{--                        <div class="academy-page__education-methods-cards">--}}
                {{--                            @foreach($educationMethods['items'] as $card)--}}
                {{--                                <article class="academy-page__education-methods-card">--}}
                {{--                                    <object--}}
                {{--                                        type="image/svg+xml"--}}
                {{--                                        data="{{ $card->image }}"--}}
                {{--                                        width="70"--}}
                {{--                                        height="70"--}}
                {{--                                        class="academy-page__education-methods-card-img"--}}
                {{--                                    >--}}
                {{--                                        <img--}}
                {{--                                            src="{{ $card->image }}"--}}
                {{--                                            width="70"--}}
                {{--                                            height="70"--}}
                {{--                                            alt=""--}}
                {{--                                            class="academy-page__education-methods-card-img"--}}
                {{--                                        />--}}
                {{--                                    </object>--}}
                {{--                                    <h3 class="academy-page__education-methods-card-title">--}}
                {{--                                        {{ $card->cardTitle }}--}}
                {{--                                    </h3>--}}
                {{--                                    <p class="academy-page__education-methods-card-description">--}}
                {{--                                        {{ $card->cardDescription }}--}}
                {{--                                    </p>--}}
                {{--                                </article>--}}
                {{--                            @endforeach--}}
                {{--                        </div>--}}
                {{--                    </section>--}}
                {{--                @endif--}}
                {{--                @if($formData)--}}
                {{--                    <academy-form--}}

                {{--                    />--}}
                {{--                @endif--}}
            </div>
        </div>
    </main>
@endsection
