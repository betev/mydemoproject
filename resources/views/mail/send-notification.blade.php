@component('mail::message',['notificationIcon' => $notificationIcon])
    <h1>{{ __('Тема письма').': '.$title }}</h1>{!! $body !!}
@endcomponent
