@php
    $model_route = [
        \App\Models\Fabric::class => 'fabrics',
        \App\Models\Carpet::class => 'carpets',
        \App\Models\Component::class => 'components'
    ];
@endphp
@component('mail::message')<h1>Создан новый заказ</h1>
    Создан новый заказ <a href="{{ env('APP_URL') }}/admin/resources/orders/{{ $order->id }}">{{ $order->id }}</a> <a href="{{ env('APP_URL') }}/admin/resources/users/{{ $order->user->id }}">{{ __('Свяжитесь с покупателем для уточнения деталей.') }}</a>
    <table class="table table-hover">
        <tr>
            <th>{{ __('Товар') }}</th>
            <th>{{ __('Количество') }}</th>
            <th>{{ __('Цена') }}</th>
        </tr>
        @foreach ($order->items as $item)
            <tr>
                <td>
                    <a href="{{ env('APP_URL') }}/admin/resources/{{ $model_route[$item->product_type] }}/{{ $item->product_id }}">
                        {{ $item->product->name }}
                    </a>
                </td>
                <td>{{ $item->quantity }}</td>
                <td>{{ $item->price }}</td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td class="text-right">{{ __('Итого:') }}</td>
            <td>{{ $order->price }} {{ $order->currency->iso }}</td>
        </tr>
    </table>
@endcomponent