@component('mail::message')
    <h1>{{ __('Отмена заказа') }}</h1>

{{ __('Отменен заказ') }} {{ $order->id }}
@endcomponent