@extends('layouts.app')
@section('content')
    @php
        $locale = app()->getLocale();
        $settings = \App\Models\Settings::where('slug', 'error_pages')->first();
        $fields = $settings->fields->{$locale};
    @endphp

    <main id="main" class="main">
        <section class="error-page">
            <h1 class="error-page__title">{{ $fields->page_503->header ?: __('Ошибка сервера: 503') }}</h1>
            <p class="error-page__subtitle">{{ $fields->page_503->subheader ?: __('Мы приносим извинения за неудобства') }}</p>
            <p class="error-page__message">{{ $fields->page_503->text ?: __('Сервер сайта на данный момент недоступен') }}</p>
        </section>
    </main>

@endsection