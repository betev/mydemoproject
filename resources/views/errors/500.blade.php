@extends('layouts.app')
@section('content')
    @php
        $locale = app()->getLocale();
        $settings = \App\Models\Settings::where('slug', 'error_pages')->first();
        $fields = $settings->fields->{$locale};
    @endphp

    <main id="main" class="main">
        <section class="error-page">
            <h1 class="error-page__title">{{ $fields->page_500->header ?: __('Ошибка сервера: 500') }}</h1>
            <p class="error-page__subtitle">{{ $fields->page_500->subheader ?: __('Мы приносим извинения за неудобства') }}</p>
            <p class="error-page__message">{{ $fields->page_500->text ?: __('Идет работа над созданием чего-то лучшего, это не займет много времени.') }}</p>
        </section>
    </main>

@endsection