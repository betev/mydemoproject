@extends('layouts.app')
@section('content')
    @php
        $locale = app()->getLocale();
        $settings = \App\Models\Settings::where('slug', 'error_pages')->first();
        $fields = $settings->fields->{$locale} ;
    @endphp

    <main id="main" class="main">
        <section class="error-page">
            <h1 class="error-page__title">{{ $fields->page_404->header ?: __('Ошибка: 404') }}</h1>
            <p class="error-page__subtitle">{{ $fields->page_404->subheader ?: __('Мы приносим извинения за неудобства') }}</p>
            <p class="error-page__message">{{ $fields->page_404->text ?: __('Похоже, вы заблудились. Страница, которую вы ищете, не существует') }}</p>
            <svg
                    width="8"
                    height="21"
                    class="error-page__arrow"
            >
                <use xlink:href="#filter-arrow" />
            </svg>
            <a href="/" class="error-page__link">{{ __('Вернуться на главную страницу') }}</a>
        </section>
    </main>

@endsection