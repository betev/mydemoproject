<script>
    if(performance.navigation.type == 2){
        location.reload(true);
    }
</script>
@extends('layouts.app')
@section('content')

    <main id="main" class="main">

        @include('parts.breadcrumbs')

        <div class="cart-page">
            <div class="cart-page__inner">

                <cart
                        cart-title="{{ __('product.Моя корзина')}}"
                        cart-total-title="{{ __('product.Итого') }}"
                        currency="{{ \App\Tools\CurrentCountry::get()->currency->iso }}"
                        total="{{ __('Всего') }}"
                        submit-btn-title="{{ __('product.Перейти к оформлению') }}"

                        :actions='@json($actions)'

                        :promocode-info='@json($promocode_info)'

                        :labels='@json($labels)'

                        :cart-state='@json($cart_state)'
                ></cart>

            </div>
        </div>
    </main>

@endsection
