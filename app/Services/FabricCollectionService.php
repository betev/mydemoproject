<?php

namespace App\Services;

use App\Models\FabricCollection;
use Illuminate\Support\Str;

class FabricCollectionService
{
    public static function getIdByName(string $name): int
    {
        $name = trim($name);

        $fabricCollection = FabricCollection::where('name', $name)->orWhere('slug', Str::slug($name))->first();

        if (!$fabricCollection) {
            $fabricCollection = new FabricCollection;
            $fabricCollection->name = $name;
            $fabricCollection->slug = Str::slug($name);
            $fabricCollection->save();
        }

        return $fabricCollection->id;
    }
}
