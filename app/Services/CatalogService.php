<?php

namespace App\Services;

use App\Http\Resources\CatalogCarpetResource;
use App\Http\Resources\CatalogComponentResource;
use App\Http\Resources\CatalogFabricCollectionResource;
use App\Http\Resources\CatalogFabricResource;
use App\Http\Resources\ColorsResource;
use App\Http\Resources\ComponentCategoriesResource;
use App\Http\Resources\FabricCollectionsResource;
use App\Http\Resources\FabricTypesResource;
use App\Http\Resources\SizesResource;
use App\Models\FabricCollection;
use App\Models\Settings;
use App\Repositories\CarpetRepository;
use App\Repositories\ComponentRepository;
use App\Repositories\FabricCollectionRepository;
use App\Repositories\FabricRepository;

class CatalogService
{
    const PER_PAGE = 15;

    protected $locale;
    protected $seo_settings;

    public function __construct()
    {
        $this->locale = app()->getLocale() ?? 'kz';
        $this->seo_settings = Settings::whereSlug('product_seo_settings')->first();
    }

    public function fabric(FabricCollection $collection = null): array
    {
        $catalog_meta = [];
        if ($this->seo_settings) {
            $catalog_meta['title'] = ($this->seo_settings->fields->{$this->locale}->title_fabric) ?? '';
            $catalog_meta['tag_title'] = ($this->seo_settings->fields->{$this->locale}->tag_title_fabric) ?? '';
            $catalog_meta['description'] = ($this->seo_settings->fields->{$this->locale}->description_fabric) ?? '';
        }
        $data = [
            'filters' => [
                'showAvailableFilter' => true,
                'showAvailableFilterTitle' => __('catalog.В наличии'),
                'filterBlocks' => !$collection
                    ? [
                    [
                        'title' => __('catalog.Типы тканей'),
                        'open' => false,
                        'options' => FabricTypesResource::collection(FabricRepository::getTypes()),
                    ],
                    [
                        'title' => __('catalog.Цвета'),
                        'open' => true,
                        'options' => ColorsResource::collection(FabricRepository::getColors()),
                    ],
                ]
                    : [
                        [
                            'title' => __('catalog.Цвета'),
                            'open' => true,
                            'options' => ColorsResource::collection(FabricRepository::getColors()),
                        ]
                    ]
            ],
            'meta' => (object)$catalog_meta
        ];

        $breadcrumbs = [
            'controllerBreadcrumbs' => [
                ['url' => '/'.app()->getLocale().'/', 'title' => __('product.Главная')],
                ['url' => '/'.app()->getLocale().'/fabrics/collections', 'title' => __('Коллекции тканей')],
            ]
        ];
        if ($collection !== null) {
            $breadcrumbs['controllerBreadcrumbs'][] = ['url' => '/'.app()->getLocale().'/fabrics/collections/'.$collection->id, 'title' => $collection->name];
        }
        $route = $collection !== null ? route('catalog.fabric.get', [$collection->id]) : route('catalog.fabric');

        $common = $this->catalog(
            FabricRepository::class,
            CatalogFabricResource::class,
            $route,
            $collection
        );
        return array_merge($data, $common, $breadcrumbs);
    }

    public function fabricCollection()
    {
        $catalog_meta = [];
        if ($this->seo_settings) {
            $catalog_meta['title'] = ($this->seo_settings->fields->{$this->locale}->title_fabric) ?? '';
            $catalog_meta['tag_title'] = ($this->seo_settings->fields->{$this->locale}->tag_title_fabric) ?? '';
            $catalog_meta['description'] = ($this->seo_settings->fields->{$this->locale}->description_fabric) ?? '';
        }
        $data = [
            'filters' => [
                'showAvailableFilter' => true,
                'showAvailableFilterTitle' => __('catalog.В наличии'),
                'filterBlocks' => [
                    [
                        'title' => __('catalog.Цвета'),
                        'open' => true,
                        'options' => ColorsResource::collection(FabricRepository::getColors()),
                    ],
                    [
                        'title' => __('catalog.Коллекции'),
                        'open' => false,
                        'options' => FabricCollectionsResource::collection(FabricRepository::getCollections()),
                    ],
                ]
            ],
            'meta' => (object)$catalog_meta,
        ];

        $breadcrumbs = [
            'controllerBreadcrumbs' => [
                ['url' => '/'.app()->getLocale().'/', 'title' => __('product.Главная')],
                ['url' => '/'.app()->getLocale().'/fabrics/collections', 'title' => __('Коллекции тканей')],
            ]
        ];

        $common = $this->catalog(
            FabricCollectionRepository::class,
            CatalogFabricCollectionResource::class,
            route('catalog.collections.fabric')
        );
        return array_merge($data, $common, $breadcrumbs);
    }

    public function fabrics($collection = null)
    {
        $catalog_meta = [];
        if ($this->seo_settings) {
            $catalog_meta['title'] = ($this->seo_settings->fields->{$this->locale}->title_fabric) ?? '';
            $catalog_meta['tag_title'] = ($this->seo_settings->fields->{$this->locale}->tag_title_fabric) ?? '';
            $catalog_meta['description'] = ($this->seo_settings->fields->{$this->locale}->description_fabric) ?? '';
        }
        $data = [
            'filters' => [
                'showAvailableFilter' => true,
                'showAvailableFilterTitle' => __('catalog.В наличии'),
                'filterBlocks' => [
                    [
                        'title' => __('catalog.Типы тканей'),
                        'open' => false,
                        'options' => FabricTypesResource::collection(FabricRepository::getTypes()),
                    ],
                    [
                        'title' => __('catalog.Цвета'),
                        'open' => true,
                        'options' => ColorsResource::collection(FabricRepository::getColors()),
                    ],
                ]
            ],
            'meta' => (object)$catalog_meta
        ];

        $common = $this->catalog(
            FabricRepository::class,
            CatalogFabricResource::class,
            route('catalog.fabric')
        );
        return array_merge($data, $common);
    }

    public function carpet()
    {
        $catalog_meta = [];
        if ($this->seo_settings) {
            $catalog_meta['title'] = ($this->seo_settings->fields->{$this->locale}->title_carpet) ?? '';
            $catalog_meta['tag_title'] = ($this->seo_settings->fields->{$this->locale}->tag_title_carpet) ?? '';
            $catalog_meta['description'] = ($this->seo_settings->fields->{$this->locale}->description_carpet) ?? '';
        }
        $data = [
            'filters' => [
                'showAvailableFilter' => true,
                'showAvailableFilterTitle' => __('catalog.В наличии'),
                'filterBlocks' => [
                    [
                        'title' => __('catalog.Тип дизайна'),
                        'open' => false,
                        'options' => [],
//                        'options' => FabricTypesResource::collection(CarpetRepository::getTypes()),
                    ],
                    [
                        'title' => __('catalog.Цвета'),
                        'open' => false,
                        'options' => ColorsResource::collection(CarpetRepository::getColors()),
                    ],
                    [
                        'title' => __('catalog.Размер'),
                        'open' => true,
                        'options' => SizesResource::collection(CarpetRepository::getSizes()),
                    ],
                ]
            ],
            'meta' => (object)$catalog_meta
        ];

        $common = $this->catalog(
            CarpetRepository::class,
            CatalogCarpetResource::class,
            route('catalog.carpets.get')
        );
        return array_merge($data, $common);
    }

    public function component()
    {
        $catalog_meta = [];
        if ($this->seo_settings) {
            $catalog_meta['title'] = ($this->seo_settings->fields->{$this->locale}->title_component) ?? '';
            $catalog_meta['tag_title'] = ($this->seo_settings->fields->{$this->locale}->tag_title_component) ?? '';
            $catalog_meta['description'] = ($this->seo_settings->fields->{$this->locale}->description_component) ?? '';
        }
        $data = [
            'filters' => [
                'showAvailableFilter' => true,
                'showAvailableFilterTitle' => __('catalog.В наличии'),
                'filterBlocks' => [
                    [
                        'title' => __('catalog.Категории'),
                        'open' => true,
                        'options' => ComponentCategoriesResource::collection(ComponentRepository::getCategories()),
                    ],
                ]
            ],
            'meta' => (object)$catalog_meta
        ];

        $common = $this->catalog(
            ComponentRepository::class,
            CatalogComponentResource::class,
            route('catalog.component.get')
        );
        return array_merge($data, $common);
    }

    /**
     * @param string $class
     * @param mixed $collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function next(string $class, string $resource, $collection = null)
    {
        $perPage = request('amountPerPage') ?? self::PER_PAGE;
        $catalog = $class::getAll($perPage, $collection);

        $response = [];
        $response['status'] = '200';
        $response['data']['next_set_link'] = $catalog->nextPageUrl();
        $response['data']['products'] = $resource::collection($catalog);

        return response()->json($response);
    }

    /**
     * @param string $class
     * @param string $resource
     * @param FabricCollection $collection
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByFilters(string $class, string $resource)
    {
        $filters = request()->get('params');
        $perPage = request('amountPerPage') ?? self::PER_PAGE;
        $catalog = $filters ? $class::withFilters($filters, $perPage) : $class::getAll($perPage);
        $response = [];
        $response['status'] = '200';
        $response['data']['next_set_link'] = $catalog->nextPageUrl();
        $response['data']['products'] = $resource::collection($catalog);
        return response()->json($response);
    }

    public function fabricsByFilters($class, $resource, $collection = null)
    {
        $filters = request()->get('params');
        $perPage = request('amountPerPage') ?? self::PER_PAGE;
        $catalog = $filters ? $class::withFilters($filters, $perPage, $collection) : $class::getAll($perPage, $collection);

        $response = [];
        $response['status'] = '200';
        $response['data']['next_set_link'] = $catalog->nextPageUrl();
        $response['data']['products'] = $resource::collection($catalog);

        return response()->json($response);
    }

    /**
     * @param $repository
     * @param $resource
     * @param mixed $route
     * @param null|mixed $collection
     *
     * @return array
     */
    private function catalog($repository, $resource, $route, $collection = null)
    {
        $perPage = request('amountPerPage') ?? self::PER_PAGE;
        $catalog = $repository::getAll($perPage, $collection);

        return [
            'title' => __('catalog.Каталог ковров Textoria'),
            'loadButton' => __('catalog.Открыть еще'),
            'actions' => [
                'getProducts' => $route,
                'getNextProducts' => $catalog->nextPageUrl(),
            ],
            'sorting' => [
                'showObjectsTitle' => __('catalog.Показать'),
                'priceSortingTitle' => __('catalog.Сортировать по цене'),
            ],
            'catalog' => [
                'labelsTitle' => [
                    'hit' => 'HIT',
                    'sale' => 'SALE',
                    'special' => 'SPECIAL OFFER'
                ],
                'sliderBtnLabels' => [
                    'prev' => __('catalog.Предыдущее изображение'),
                    'next' => __('catalog.Следующее изображение'),
                ],
                'catalogItems' => $resource::collection($catalog),
            ],
        ];
    }
}
