<?php


namespace App\Services;

use App\Models\Carpet;
use App\Models\Component;
use App\Models\Country;
use App\Models\Fabric;
use App\Models\Settings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class ProductService
{
    protected $currentCountry;
    protected $currency;

    public function __construct()
    {
        $this->currentCountry = Country::whereSlug('kz')->first();
        $this->currency = $this->currentCountry->currency;
    }

    /**
     * @param Fabric $fabric
     *
     * @return array
     */
    public function showFabric(Fabric $fabric): array
    {
        $locale = app()->getLocale();
        $fabricSettings = Settings::where('slug', 'textile_purchases_setting')->first();
        $footage_multiplicity = $fabricSettings && $fabricSettings->fields->footage ? $fabricSettings->fields->footage : '0,1';
        $roll_multiplicity = $fabricSettings && $fabricSettings->fields->roll ? $fabricSettings->fields->roll : 30;
        $description = $fabric->getTranslation('description', $locale, true);
        $information = [
            [
                'title' => __('product.Тип ткани'),
                'value' => $fabric->fabricType && $fabric->fabricType->name
                    ? $fabric->fabricType->getTranslation('name', $locale, true)
                    : '',
            ],
            [
                'title' => __('product.Страна производства'),
                'value' => $fabric->manufacturer->getTranslation('name', $locale, true),
            ],
        ];
        $fabric_information = $fabric->getTranslation('information', app()->getLocale()) !== ''
            ? $fabric->getTranslation('information', app()->getLocale())
            : $fabric->getTranslation('information', 'kz');
        if ($fabric_info = $fabric_information) {
            if ($fabric_info !== '') {
                foreach (explode(';', $fabric_info) as $info) {
                    if (Str::contains($info, '}')) {
                        $info_part = explode('}', $info);
                        $information[] = [
                            'title' => __(trim($info_part[0])),
                            'value' => __(trim($info_part[1]))
                        ];
                    }
                }
            }
        }
        $data = [
            'cards' => [
                [
                    'name' => $fabric->name ?: Str::upper($fabric->slug),
                    'description' => $description,
                    'model' => 'App\Models\Fabric',
                    'price' => round($fabric->getRollPriceAttribute() * $this->currency->exchange_rate, 2),
                    'label' => [
                        'plus' => __('product.Добавить товар'),
                        'minus' => __('product.Убавить товар'),
                        'amount' => __('product.Количество'),
                        'currency' => ' '.$this->currency->iso,
                        'price' => __('product.Цена за метр'),
                        'total' => __('product.Итого'),
                        'cart' => __('product.Добавить в корзину'),
                        'cartAdded' => __('product.Добавлено!'),
                        'title' => __('product.Рулон', ['number' => $roll_multiplicity]),
                        'max' => __('product.Максимально - XX рулонов', ['number' => 30]),
                        'error' => __('product.Ошибка сети. Проверьте ваше подключение к интернету.'),
                    ],
                    'id' => $fabric->id,
                    'max' => 30,
                    'unit' => 'roll',
                    'step' => 1,
                    'actions' => [
                        'addToCart' => route('add-to-cart'),
                    ],
                    'meters_ratio' => (float)$roll_multiplicity,
                ],
                [
                    'name' => $fabric->name ?: Str::upper($fabric->slug),
                    'description' => $description,
                    'model' => 'App\Models\Fabric',
                    'price' => round($fabric->getMeterPriceAttribute() * $this->currency->exchange_rate, 2),
                    'label' => [
                        'plus' => __('product.Добавить товар'),
                        'minus' => __('product.Убавить товар'),
                        'amount' => __('product.Количество'),
                        'currency' => ' '.$this->currency->iso,
                        'price' => __('product.Цена за метр'),
                        'total' => __('product.Итого'),
                        'cart' => __('product.Добавить в корзину'),
                        'cartAdded' => __('product.Добавлено!'),
                        'title' => __('product.Метраж', ['number' => $footage_multiplicity]),
                        'max' => __('product.Максимально - XX п/м', ['number' => 30]),
                        'error' => __('product.Ошибка сети. Проверьте ваше подключение к интернету.'),
                    ],
                    'id' => $fabric->id,
                    'max' => 30,
                    'unit' => 'meter',
                    'step' => (float)$footage_multiplicity,
                    'actions' => [
                        'addToCart' => route('add-to-cart'),
                    ],
                    'meters_ratio' => (float)$footage_multiplicity,
                ]
            ],
            'cares' => json_decode($fabric->cares),
            'descriptionTitle' => __('product.Описание'),
            'info' => $information,
        ];
        return array_merge($data, $this->getCommonFields($fabric));
    }

    /**
     * @param Carpet $carpet
     *
     * @return array
     */
    public function showCarpet(Carpet $carpet): array
    {
        $locale = app()->getLocale();
        $data = [
            'card' => [
                'name' => $carpet->name->$locale ?? '',
                'description' => $carpet->description->$locale ?? '',
                'model' => 'App\Models\Carpet',
                'price' => number_format($carpet->price * $this->currency->exchange_rate, 2),
                'label' => [
                    'plus' => __('product.Добавить товар'),
                    'minus' => __('product.Убавить товар'),
                    'amount' => __('product.Количество'),
                    'currency' => 'р',
                    'price' => __('product.Цена'),
                    'total' => __('product.Итого'),
                    'cart' => __('product.Добавить в корзину'),
                    'cartAdded' => __('product.Добавлено!'),
                    'title' => __('product.Количество'),
                    'max' => '',
                    'error' => __('product.Ошибка сети. Проверьте ваше подключение к интернету.'),
                ],
                'id' => $carpet->id,
                'max' => 30,
                'step' => 1,
                'actions' => [
                    'addToCart' => route('add-to-cart'),
                ],
                'variants' => [
                    [
                        'id' => '1',
                        'price' => 8091,
                        'title' => '135 x 200',
                        'specifications' => [
                            [
                                'title' => __('product.Тип дизайна'),
                                'value' => 'абстрактный современный',
                            ],
                            [
                                'title' => __('product.Вид производства'),
                                'value' => 'машинный',
                            ],
                            [
                                'title' => __('product.Тип ковра'),
                                'value' => 'тканый безворсовой',
                            ],
                            [
                                'title' => __('product.Состав (%)'),
                                'value' => 'акрил (PC) - 65%,<br>хлопок (CO) - 20%,<br>полиэстер (PES) - 15%',
                            ],
                            [
                                'title' => __('product.Размер ковра, см'),
                                'value' => '135х200 +/- 5',
                            ],
                            [
                                'title' => __('product.Вес нетто, кг'),
                                'value' => '2,9 +/- 0,1',
                            ],
                            [
                                'title' => __('product.Вес брутто, кг'),
                                'value' => '3,2 +/- 0,1',
                            ],
                            [
                                'title' => __('product.Размер ковра в упаковке, длина см'),
                                'value' => '140',
                            ],
                            [
                                'title' => __('product.Размер ковра в упаковке, диаметр см'),
                                'value' => '14',
                            ],
                            [
                                'title' => __('product.Страна производства'),
                                'value' => 'Турция',
                            ],
                            [
                                'title' => __('product.Бренд'),
                                'value' => 'ARBEN. DECOR MAGIC',
                            ],
                        ],
                    ],
                    [
                        'id' => '2',
                        'price' => 10020,
                        'title' => '160 x 230',
                        'specifications' => [
                            [
                                'title' => __('product.Тип дизайна'),
                                'value' => 'абстрактный современный',
                            ],
                            [
                                'title' => __('product.Вид производства'),
                                'value' => 'машинный',
                            ],
                            [
                                'title' => __('product.Тип ковра'),
                                'value' => 'тканый безворсовой',
                            ],
                            [
                                'title' => __('product.Состав (%)'),
                                'value' => 'акрил (PC) - 65%,<br>хлопок (CO) - 20%,<br>полиэстер (PES) - 15%',
                            ],
                            [
                                'title' => __('product.Размер ковра, см'),
                                'value' => '160х230 +/- 5',
                            ],
                            [
                                'title' => __('product.Вес брутто, кг'),
                                'value' => '3,5 +/- 0,1',
                            ],
                            [
                                'title' => __('product.Вес брутто, кг'),
                                'value' => '3,8 +/- 0,1',
                            ],
                            [
                                'title' => __('product.Размер ковра в упаковке, длина см'),
                                'value' => '180',
                            ],
                            [
                                'title' => __('product.Размер ковра в упаковке, диаметр см'),
                                'value' => '20',
                            ],
                            [
                                'title' => __('product.Страна производства'),
                                'value' => 'Турция',
                            ],
                            [
                                'title' => __('product.Бренд'),
                                'value' => 'ARBEN. DECOR MAGIC',
                            ],
                        ],
                    ],
                ],
            ],
            'descriptionTitle' => __('product.Описание'),
            'info' => [
                [
                    'title' => __('product.Тип дизайна'),
                    'value' => 'абстрактный современный',
                ],
                [
                    'title' => __('product.Вид производства'),
                    'value' => 'машинный',
                ],
                [
                    'title' => __('product.Тип ковра'),
                    'value' => 'тканый безворсовой',
                ],
                [
                    'title' => __('product.Состав (%)'),
                    'value' => 'акрил (PC) - 65%,<br>хлопок (CO) - 20%,<br>полиэстер (PES) - 15%',
                ],
                [
                    'title' => __('product.Размер ковра, см'),
                    'value' => '135х200 +/- 5',
                ],
                [
                    'title' => __('product.Вес нетто, кг'),
                    'value' => '2,9 +/- 0,1',
                ],
                [
                    'title' => __('product.Вес брутто, кг'),
                    'value' => '3,2 +/- 0,1',
                ],
                [
                    'title' => __('product.Размер ковра в упаковке, длина см'),
                    'value' => '140',
                ],
                [
                    'title' => __('product.Размер ковра в упаковке, диаметр см'),
                    'value' => '14',
                ],
                [
                    'title' => __('product.Страна производства'),
                    'value' => 'Турция',
                ],
                [
                    'title' => __('product.Бренд'),
                    'value' => 'ARBEN. DECOR MAGIC',
                ],
            ],
        ];
        return array_merge($data, $this->getCommonFields($carpet));
    }

    /**
     * @param Component $component
     *
     * @return array
     */
    public function showComponent(Component $component): array
    {
        $locale = app()->getLocale();
        $data = [
            'card' => [
                'model' => 'App\Models\Component',
                'price' => (int)$component->price * $this->currency->exchange_rate,
                'name' => $component->name->{$locale} ?? '',
                'description' => $component->description->{$locale} ?? '',
                'label' => [
                    'plus' => __('product.Добавить товар'),
                    'minus' => __('product.Убавить товар'),
                    'amount' => __('product.Количество'),
                    'currency' => 'р',
                    'price' => __('product.Цена'),
                    'total' => __('product.Итого'),
                    'cart' => __('product.Добавить в корзину'),
                    'cartAdded' => __('product.Добавлено!'),
                    'title' => __('product.Количество'),
                    'max' => '',
                    'error' => __('product.Ошибка сети. Проверьте ваше подключение к интернету.'),
                ],
                'id' => $component->id,
                'max' => 30,
                'step' => 1,
                'actions' => [
                    'addToCart' => route('add-to-cart'),
                ],
            ],
            'descriptionTitle' => __('product.Оплата и доставка'),
            'info' => [
                [
                    'title' => __('product.Длина, мм'),
                    'value' => '420',
                ],
                [
                    'title' => __('product.Ширина, мм'),
                    'value' => '23',
                ],
                [
                    'title' => __('product.Высота, мм'),
                    'value' => '23',
                ],
                [
                    'title' => __('product.Усилие сжатия (растяжения), Н'),
                    'value' => '400',
                ],
                [
                    'title' => __('product.Вес, кг'),
                    'value' => '0,29',
                ],
                [
                    'title' => __('product.Производитель'),
                    'value' => 'Турция',
                ],
                [
                    'title' => __('product.Материал'),
                    'value' => 'Сталь',
                ],
                [
                    'title' => __('product.Покрытие'),
                    'value' => 'Цинк',
                ],
                [
                    'title' => __('product.Высота коробки'),
                    'value' => '200',
                ],
                [
                    'title' => __('product.Количество в упаковке, шт'),
                    'value' => '3000',
                ],
                [
                    'title' => __('product.Диаметр, мм'),
                    'value' => '101',
                ],
            ],
        ];
        return array_merge($data, $this->getCommonFields($component));
    }

    private function getCommonFields(Model $model)
    {
        $categoryTitle = '';
        switch (get_class($model)) {
            case Fabric::class:
                $route = '/fabrics/collections';
                $productType = 'fabric';
                $categoryTitle = __('Коллекции тканей');
                $breadcrumbs = [
                    ['url' => '/'.app()->getLocale().'/', 'title' => __('product.Главная')],
                    ['url' => '/'.app()->getLocale().$route, 'title' => $categoryTitle],
                    ['url' => '/'.app()->getLocale().$route.'/'.$model->fabricCollection->id, 'title' => $model->fabricCollection->name],
                    ['url' => route('product', [$model->id, $model->slug]), 'title' => $model->name ?? Str::upper($model->slug)]
                ];
                break;
            case Carpet::class:
                $route = '/carpets';
                $productType = 'carpet';
                $categoryTitle = __('Ковры');
                $breadcrumbs = [
                    ['url' => '/'.app()->getLocale().'/', 'title' => __('product.Главная')],
                    ['url' => '/'.app()->getLocale().$route, 'title' => $categoryTitle],
                    ['url' => route('product', [$model->id, $model->slug]), 'title' => Str::upper($model->slug)]
                ];
                break;
            case Component::class:
                $route = '/components';
                $productType = 'component';
                $categoryTitle = __('Комплектующие');
                $breadcrumbs = [
                    ['url' => '/'.app()->getLocale().'/', 'title' => __('product.Главная')],
                    ['url' => '/'.app()->getLocale().$route, 'title' => $categoryTitle],
                    ['url' => route('product', [$model->id, $model->slug]), 'title' => Str::upper($model->slug)]
                ];
                break;
        }
        return [
            'productType' => $productType,
            'model' => $model,
            'title' => $model->name ?: Str::upper($model->slug),
            'video' => $this->getVideo($model),
            'slides' => $this->getSlides($model),
            'description' => $model->getTranslation('description', app()->getLocale()) !== ''
                ? $model->getTranslation('description', app()->getLocale())
                : $model->getTranslation('description', 'kz'),
            'controllerBreadcrumbs' => $breadcrumbs
        ];
    }

    private function getVideo(Model $model)
    {
        $video = $model->getFirstMedia($model::MEDIA_VIDEO);
        return $video
            ? [
                'thumb' => asset('/images/product.jpg'),
                'medium' => asset('/images/product.jpg'),
                'mp4' => $video->getUrl(),
                'alt' => '',
            ]
            : null;
    }

    private function getSlides(Model $model)
    {
        return $model->getMedia($model::MEDIA_IMAGE)->map(function ($media) {
            return [
                'image_144' => $media->getUrl('144'),
                'image_290' => $media->getUrl('290'),
                'image_553' => $media->getUrl('553'),
                'image_710' => $media->getUrl('710'),
                'image_1110' => $media->getUrl('1110'),
                'image_1420' => $media->getUrl('1420'),
            ];
        });
    }
}
