<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the Throwable types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * A list of the inputs that are never flashed for validation Throwables.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     *
     * @throws \Throwable
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof TokenMismatchException) {
            if ($request->wantsJson()) {
                return response()->json([
                    'status' => 419,
                    'message' => 'Сессия истекла из-за бездействия. Пожалуйста, перезагрузите страницу и попробуйте еще раз',
                ], 419);
            }
        }

        if ($e instanceof ModelNotFoundException && $request->wantsJson()) {
            return response()->json([
                'status' => 404,
                'message' => 'Not Found'
            ], 404);
        }

        return parent::render($request, $e);
    }

    /**
     * @param ValidationException $e
     * @param \Illuminate\Http\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $response = parent::convertValidationExceptionToResponse($e, $request);

        if ($response instanceof JsonResponse) {
            $original = $response->getOriginalContent();
            $original['message'] = $e->validator->messages()->first() ?: __($original['message']);
            $response->setContent(json_encode($original));
        }

        return $response;
    }
}
