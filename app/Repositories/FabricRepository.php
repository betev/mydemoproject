<?php


namespace App\Repositories;

use App\Models\Color;
use App\Models\Fabric;
use App\Models\FabricCollection;
use App\Models\FabricType;
use App\Models\FilterColor;
use App\Models\Settings;
use App\Tools\CurrentCountry;

class FabricRepository extends AbstractRepository
{
    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public static function getAll(int $perPage, FabricCollection $collection = null)
    {
        $orderSettings = Settings::whereSlug(Settings::FABRIC_LIST_SETTINGS)->first();
        $country = config('app.country_code');
        $fabrics =  Fabric::where('publish', true);
        if ($collection !== null) {
            $fabrics->where('fabrics.fabric_collection_id', $collection->id);
        }
        if ($orderSettings && $orderSettings->fields) {
            if ($orderSettings->fields->field === 'marker') {
                $fabrics->leftJoin('fabric_marker', 'fabric_marker.fabric_id', '=', 'fabrics.id');
                $fabrics->orderBy('fabric_marker.marker_id', $orderSettings->fields->order)
                    ->orderBy('fabrics.name', $orderSettings->fields->order);
            } else {
                $fabrics->orderBy($orderSettings->fields->field, $orderSettings->fields->order);
            }
        }
        $route = $collection !== null
            ? route('catalog.fabric.get', [$collection->id])
            : route('catalog.fabric');
        return $fabrics->paginate($perPage)
            ->withPath($route);
    }

    /**
     * @return mixed
     */
    public static function getTypes()
    {
        $country = (new CurrentCountry())->getCountry()->slug;
        $types = Fabric::groupBy('fabrics.fabric_type_id')
            ->where('publish', true)
            ->get('fabric_type_id')
            ->toArray();

        return FabricType::whereIn('id', array_flatten($types))->orderBy('name', 'asc')->get();
    }

    /**
     * @return mixed
     */
    public static function getCollections()
    {
        $country = (new CurrentCountry())->getCountry()->slug;
        $collections = Fabric::groupBy('fabrics.fabric_collection_id')
            ->where('publish', true)
            ->get('fabric_collection_id')
            ->toArray();

        return FabricCollection::whereIn('id', array_flatten($collections))->orderBy('name', 'asc')->get();
    }

    /**
     * @return mixed
     */
    public static function getColors()
    {
        $colors = Fabric::groupBy('color_id')
            ->get('color_id')
            ->toArray();

        return Color::whereIn('id', array_flatten($colors))->orderBy('name', 'asc')->get();
    }

    /**
     * @param array $filters
     * @param int $perPage
     * @param FabricCollection $collection
     *
     * @return mixed
     */
    public static function withFilters(array $filters, int $perPage, FabricCollection $collection = null)
    {
        $orderSettings = Settings::whereSlug(Settings::FABRIC_LIST_SETTINGS)->first();
        $country = (new CurrentCountry())->getCountry()->slug;
        extract(self::getAllFilters($filters['filterBlocks']));

        $fabric = Fabric::where('publish', true);
        if ($collection !== null) {
            $fabric->where('fabric_collection_id', $collection->id);
        }
        if ($collections) {
            $fabric->whereIn('fabric_collection_id', array_pluck($collections, ['name']));
        }
        if ($types) {
            $fabric->whereIn('fabric_type_id', array_pluck($types, ['name']));
        }
        if ($colors) {
            $fabric->whereIn('color_id', array_pluck($colors, ['name']));
        }
        if ($filters['filterEnabled']) {
            $fabric->where('presence', true);
        }
        if ($filters['filterSearch']) {
            $typesIds = FabricType::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');
            $colorsIds = Color::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');
            $filterColorsIds = FilterColor::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');

            $fabric->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->orWhere('code', 'ilike', '%'.$filters['filterSearch'].'%')
                ->orWhereIn('fabric_type_id', $typesIds)
                ->orWhereIn('color_id', $colorsIds)
                ->orWhereIn('filter_color_id', $filterColorsIds);
        }
        if ($orderSettings && $orderSettings->fields) {
            if ($orderSettings->fields->field === 'marker') {
                $fabric->leftJoin('fabric_marker', 'fabric_marker.fabric_id', '=', 'fabrics.id');
                $fabric->orderBy('fabric_marker.marker_id', $orderSettings->fields->order)
                    ->orderBy('name', $orderSettings->fields->order);
            } else {
                $fabric->orderBy($orderSettings->fields->field, $orderSettings->fields->order);
            }
        }
        $route = $collection !== null
            ? route('catalog.fabric.next', ['collection' => $collection->id])
            : route('catalog.fabric');

        return $fabric->paginate($perPage)
                ->withPath($route);
    }

    /**
     * @param array $filters
     *
     * @return array
     */
    private static function getAllFilters(array $filters): array
    {
        return [
            'types' => array_filter($filters[0]['options'], function ($val) {
                return $val['checked'] == true;
            }),
            'colors' => array_filter($filters[1]['options'], function ($val) {
                return $val['checked'] == true;
            }),
            'collections' => isset($filters[2]) ? array_filter($filters[2]['options'], function ($val) {
                return $val['checked'] == true;
            }) : [],
        ];
    }

    public function search(string $text)
    {
        return Fabric::search($text)->take(1000)->get();
    }
}
