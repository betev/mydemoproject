<?php


namespace App\Repositories;

use App\Models\Color;
use App\Models\Fabric;
use App\Models\FabricCollection;
use App\Models\FabricType;
use App\Models\FilterColor;
use App\Models\Settings;
use App\Tools\CurrentCountry;

class FabricCollectionRepository extends AbstractRepository
{
    /**
     * @param int $perPage
     *
     * @return mixed
     */
    public static function getAll(int $perPage)
    {
        $orderSettings = Settings::whereSlug(Settings::FABRIC_LIST_SETTINGS)->first();
        $country = (new CurrentCountry())->getCountry()->slug;
        $query = FabricCollection::whereHas('fabrics', function ($q) use ($country) {
            $q->where('presence', true);
        });
        if ($orderSettings && $orderSettings->fields) {
            if ($orderSettings->fields->field === 'marker') {
                $query->orderBy('name', $orderSettings->fields->order);
            } else {
                $query->orderBy($orderSettings->fields->field, $orderSettings->fields->order);
            }
        }
        return $query->paginate($perPage);
    }

    /**
     * @param array $filters
     * @param int $perPage
     *
     * @return mixed
     */
    public static function withFilters(array $filters, int $perPage)
    {
        $country = (new CurrentCountry())->getCountry()->slug;
        extract(self::getAllFilters($filters['filterBlocks']));

        $collection = FabricCollection::whereHas('fabrics', function ($q) use ($country) {
            $q->where('presence', true);
        });

        if ($types) {
            $collection->whereHas('fabrics', function ($q) use ($types) {
                $q->whereIn('fabric_type_id', array_pluck($types, ['name']));
            });
        }
        if ($collections) {
            $collection->whereIn('id', array_pluck($collections, ['name']));
        }
        if ($colors) {
            $collection->whereHas('fabrics', function ($q) use ($colors) {
                $q->whereIn('color_id', array_pluck($colors, ['name']));
            });
        }
        if ($filters['filterEnabled']) {
            $collection->whereHas('fabrics', function ($q) use ($colors) {
                $q->where('presence', true);
            });
        }
        if ($filters['filterSearch']) {
            $typesIds = FabricType::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');
            $colorsIds = Color::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');
            $filterColorsIds = FilterColor::query()
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->get()->pluck('id');

            $textFilter = Fabric::query()
                ->whereIn(
                    'fabric_collection_id',
                    $collection->get()->pluck('id')->toArray()
                )
                ->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                ->orWhere('code', 'ilike', '%'.$filters['filterSearch'].'%')
                ->orWhereIn('fabric_type_id', $typesIds)
                ->orWhereIn('color_id', $colorsIds)
                ->orWhereIn('filter_color_id', $filterColorsIds)
                ->get()->pluck('fabric_collection_id');
            $collection->where('name', 'ilike', '%'.$filters['filterSearch'].'%')
                        ->orWhereIn('id', $textFilter);
        }

        return $collection->orderBy('name', 'asc')->paginate($perPage)
            ->withPath(route('catalog.fabric.collections.get'));
    }

    /**
     * @param array $filters
     *
     * @return array
     */
    private static function getAllFilters(array $filters): array
    {
        return [
            'types' => array_filter($filters[0]['options'], function ($val) {
                return $val['checked'] == true;
            }),
            'colors' => $filters[1] ? array_filter($filters[1]['options'], function ($val) {
                return $val['checked'] == true;
            }) : null,
            'collections' => $filters[2] ? array_filter($filters[2]['options'], function ($val) {
                return $val['checked'] == true;
            }) : null,
        ];
    }
}
