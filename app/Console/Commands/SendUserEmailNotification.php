<?php

namespace App\Console\Commands;

use App\Mail\SendNotification;
use App\Models\Notification;
use App\Models\Subscriber;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendUserEmailNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notifications to users at specific time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = Notification::where('send_date_time', '<', now()->toDateTime())
            ->where('sent', false)
            ->where('to_email', true)
            ->get();

        $subscribersEmails = [];

        if (count($notifications)) {
            foreach ($notifications as $notification) {
                if ($notification->send_to_all_users) {
                    $subscribersEmails = Subscriber::pluck('email')->all();
                    if (count($subscribersEmails)) {
                        foreach ($subscribersEmails as $email) {
                            Mail::to($email)->send(new SendNotification($notification));
                            if (!Mail::failures()) {
                                $notification->sent = true;
                                $notification->save();
                            }
                        }
                    }
                }

                $subscribersEmailsForGroup = [];
                if (count($notification->subscriberGroups)) {
                    $subscriberGroups = $notification->subscriberGroups;
                    foreach ($subscriberGroups as $group) {
                        if (count($group->customerTypes)) {
                            foreach ($group->customerTypes as $type) {
                                $emails = User::where('customer_type_id', $type->id)->pluck('email');
                                foreach ($emails as $email) {
                                    if (!in_array($email, $subscribersEmails)) {
                                        $subscribersEmailsForGroup[] = $email;
                                    }
                                }
                            }
                        }
                        if (count($group->users)) {
                            foreach ($group->users as $user) {
                                if (!in_array($user->email, $subscribersEmails)) {
                                    $subscribersEmailsForGroup[] = $user->email;
                                }
                            }
                        }
                        if (count($group->subscribers)) {
                            foreach ($group->subscribers as $subscriber) {
                                if (!in_array($subscriber->email, $subscribersEmails)) {
                                    $subscribersEmailsForGroup[] = $subscriber->email;
                                }
                            }
                        }
                    }
                }
                if ($subscribersEmailsForGroup) {
                    foreach ($subscribersEmailsForGroup as $email) {
                        Mail::to($email)->send(new SendNotification($notification));
                        if (!Mail::failures()) {
                            $notification->sent = true;
                            $notification->save();
                        }
                    }
                }
            }
        }
    }
}
