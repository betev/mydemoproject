<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ForceSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:force {--class=*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run force run seeder. Example: php artisan seed:force --class=PageSeeder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $class = $this->option('class');
        $seederClass = app()->make($class[0]);
        $seederClass->run(true);
    }
}
