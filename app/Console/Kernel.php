<?php

namespace App\Console;

use App\Console\Commands\SendUserEmailNotification;
use App\Console\Commands\UploadImages;
use App\Console\Commands\UploadImagesToEmptyModels;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UploadImages::class,
        SendUserEmailNotification::class,
        UploadImagesToEmptyModels::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if ($this->app->isLocal() || mb_strtolower(app()->environment()) === 'dev') {
            $schedule->command('telescope:prune')->cron('0 23 */2 * *');
        }
        $schedule->command('images:upload')->hourly();
        $schedule->command('send:email-notification')->everyFiveMinutes();
        $schedule->command('delete:duplicates')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
