<?php

namespace App\Providers;

use App\Http\CustomControllerDispatcher;
use App\Tools\CurrentCountry;
use Illuminate\Routing\Contracts\ControllerDispatcher as ControllerDispatcherContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment('production')) {
            error_reporting(E_ALL ^ E_NOTICE);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal() || mb_strtolower(app()->environment()) === 'dev') {
            $this->app->register(TelescopeServiceProvider::class);
        }

        $this->app->singleton(CurrentCountry::class, function ($app) {
            return new CurrentCountry;
        });

        $this->app->bind(ControllerDispatcherContract::class, function ($app) {
            return new CustomControllerDispatcher($app);
        });
    }
}
