<?php

namespace App\Providers;

use Anaseqal\NovaImport\NovaImport;
use App\Models\FabricCollection;
use App\Models\OrderItem;
use App\Models\User;
use App\Observers\FabricCollectionObserver;
use App\Observers\OrderItemObserver;
use App\Observers\UserObserver;
use Flagstudio\NovaContacts\NovaContacts;
use Flagstudio\NovaInstructions\NovaInstructions;
use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Vyuldashev\NovaPermission\NovaPermissionTool;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    const RU = 'ru';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        $locales = [
            'ru',
            'ua',
            'by',
            'pl',
            'kz'
        ];
        \Spatie\NovaTranslatable\Translatable::defaultLocales($locales);

        Nova::style('admin', public_path('css/nova.css'));
        User::observe(UserObserver::class);
        Nova::serving(function (ServingNova $event) {
            app()->setLocale('ru');
            OrderItem::observe(OrderItemObserver::class);
            FabricCollection::observe(FabricCollectionObserver::class);
        });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return $user->isAdmin();
        });
    }

    /**
     * Get the cards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new NovaContacts,
            new NovaInstructions,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            new NovaImport,
            //\Vyuldashev\NovaPermission\NovaPermissionTool::make(),
            (new NovaPermissionTool())->canSee(function ($request) {
                /** @var User $user */
                $user = $request->user();
                return $user->email === 'forspam@flagstudio.ru';
            })
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Laravel\Nova\Http\Controllers\ResourceUpdateController', 'App\Http\Controllers\Nova\ResourceUpdateController');
    }
}
