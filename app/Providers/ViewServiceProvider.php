<?php

namespace App\Providers;

use App\Http\ViewComposers\AppComposer;
use App\Http\ViewComposers\ProfileComposer;
use App\Http\ViewComposers\QrComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.app', 'parts.breadcrumbs', 'catalog-qr-code'], AppComposer::class);
        View::composer(['catalog-qr-code'], QrComposer::class);
        View::composer(['profile.*', 'auth.login'], ProfileComposer::class);
    }
}
