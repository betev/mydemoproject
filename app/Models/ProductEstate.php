<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

abstract class ProductEstate extends Model implements HasMedia
{
    use InteractsWithMedia;

//    protected static function booted()
//    {
//        //static::addGlobalScope(new CountryScope);
//        //static::addGlobalScope(new PublishedScope);
//
//        /*static::creating(function (ProductEstate $model) {
//            if (!$model->country) {
//                $model->country()->associate(CurrentCountry::get());
//            }
//        });*/
//    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(600)
            ->height(480)
            ->sharpen(10);

        $this->addMediaConversion('144')
            ->width(144)
            ->height(144)
            ->sharpen(10);

        $this->addMediaConversion('248')
            ->width(248)
            ->height(248)
            ->sharpen(10);

        $this->addMediaConversion('280')
            ->width(280)
            ->height(280)
            ->sharpen(10);

        $this->addMediaConversion('290')
            ->width(290)
            ->height(290)
            ->sharpen(10);

        $this->addMediaConversion('330')
            ->width(330)
            ->height(330)
            ->sharpen(10);

        $this->addMediaConversion('360')
            ->width(360)
            ->height(360)
            ->sharpen(10);

        $this->addMediaConversion('553')
            ->width(553)
            ->height(553)
            ->sharpen(10);

        $this->addMediaConversion('560')
            ->width(560)
            ->height(560)
            ->sharpen(10);

        $this->addMediaConversion('660')
            ->width(660)
            ->height(660)
            ->sharpen(10);

        $this->addMediaConversion('710')
            ->width(710)
            ->height(710)
            ->sharpen(10);

        $this->addMediaConversion('720')
            ->width(720)
            ->height(720)
            ->sharpen(10);

        $this->addMediaConversion('1110')
            ->width(1110)
            ->height(1110)
            ->sharpen(10);

        $this->addMediaConversion('1420')
            ->width(1420)
            ->height(1420)
            ->sharpen(10);
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function getRouteKeyName()
    {
        return 'id';
    }

    /*public function scopePublished($query)
    {
        return $query->where('archived', false);
    }

    public function scopeInStock($query)
    {
        return $query->where('in_stock', true);
    }*/
}
