<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Translatable\HasTranslations;

class Care extends Model implements HasMedia
{
    use InteractsWithMedia;
    use HasTranslations;

    const MEDIA_CARE_IMAGE ="care_image";

    protected $fillable = [
        'name',
        'description',
    ];

    public $translatable = [
        'name',
        'description',
    ];

    protected $casts = [
        'fields' => 'object',
    ];

    public function fabrics(): BelongsToMany
    {
        return $this->belongsToMany(Fabric::class, 'care_fabric', 'care_id', 'fabric_id');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(static::MEDIA_CARE_IMAGE)
            ->singleFile();
    }
}
