<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CareFabric extends Pivot
{
    public $timestamps = false;

    protected $table = 'care_fabric';

    protected $autoincrement = false;

    protected $fillable = ['care_id', 'fabric_id'];
}
