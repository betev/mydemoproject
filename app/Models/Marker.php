<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Translatable\HasTranslations;

class Marker extends Model
{
    use HasTranslations;

    protected $table = 'markers';

    protected $fillable = ['name'];

    public $translatable = ['name'];

    public function fabrics(): BelongsToMany
    {
        return $this->belongsToMany(Fabric::class, 'fabric_marker', 'marker_id', 'fabric_id');
    }
}
