<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FabricCollection extends Model
{
    protected $table = 'fabric_collections';

    protected $fillable = ['name','id'];

    public function fabrics(): HasMany
    {
        return $this->hasMany(Fabric::class);
    }

    public function promoCodes(): BelongsToMany
    {
        return $this->belongsToMany(PromoCode::class, 'fabric_collection_promo_code');
    }

    public function marker()
    {
        return $this->belongsTo(Marker::class);
    }
}
