<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use InteractsWithMedia;

    const ADMIN_ROLE = 'admin';
    const REGULAR_ROLE = 'regular';

    const PRICE_LIST_REGULAR = 'price_list_regular';
    const PRICE_LIST_LARGE = 'price_list_large';
    const PRICE_LIST_SMALL = 'price_list_small';
    const PRICE_LIST_INDIVIDUAL = 'price_list_individual';

    const PRIVATE_PERSON_FIELD = 'private_person';
    const LEGAL_ENTITY_FIELD = 'legal_entity';
    const ADDRESSES_FIELD = 'addresses';

    const GOOGLE_SOCIAL_LOGIN = 'google';
    const FACEBOOK_SOCIAL_LOGIN = 'facebook';
    const INSTAGRAM_SOCIAL_LOGIN = 'instagram';

    const MEDIA_PRICE_LIST = 'price_list_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'code', 'phone', 'user_legal_detail_id', 'fields', 'customer_type_id', 'one_s_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'social' => 'array',
        'fields' => 'object',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->hasRole(self::ADMIN_ROLE);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->orderBy('id', 'desc');
    }

    public function promoCodes()
    {
        return $this->belongsToMany(PromoCode::class, 'promo_code_user');
    }

    public function customerType()
    {
        return $this->belongsTo(\App\Models\CustomerType::class);
    }

    public function subscriberGroup()
    {
        return $this->belongsToMany(SubscriberGroup::class, 'subscriber_group_users');
    }

    public function notifications()
    {
        return $this->hasManyThrough(Notification::class, SubscriberGroup::class);
    }

    public function getIsLegalEntityAttribute()
    {
        $fields = (array)$this->fields;
        return isset($fields['legal_entity']) && $fields['legal_entity'][0]->value !== null;
    }

    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection(User::MEDIA_PRICE_LIST)
            ->singleFile();
    }
}
