<?php

namespace App\Models;

use App\Elastic\Configurators\CarpetIndexConfigurator;
use App\Elastic\Rules\CarpetSearchRule;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use ScoutElastic\Searchable;
use Spatie\Translatable\HasTranslations;

class Carpet extends ProductEstate
{
    use HasTranslations,
        SoftDeletes,
        Searchable;

    const MEDIA_IMAGE = "carpet_image";
    const MEDIA_VIDEO = "carpet_video";

    protected $table = 'carpets';

    protected $softDelete = true;

    protected $fillable = [
        'code',
        'name',
        'manufacturer_id',
        'size',
        'color_id',
        'slug',
        'description',
        'information',
    ];

    protected $indexConfigurator = CarpetIndexConfigurator::class;

    protected $searchRules = [CarpetSearchRule::class];

    protected $mapping = [
        'properties' => [
            'name_ru' => [
                'type' => 'text'
            ],
            'name_ua' => [
                'type' => 'text'
            ],
            'name_by' => [
                'type' => 'text'
            ],
            'name_pl' => [
                'type' => 'text'
            ],
            'name_kz' => [
                'type' => 'text'
            ],
            'color_ru' => [
                'type' => 'text'
            ],
            'color_ua' => [
                'type' => 'text'
            ],
            'color_by' => [
                'type' => 'text'
            ],
            'color_pl' => [
                'type' => 'text'
            ],
            'color_kz' => [
                'type' => 'text'
            ],
            'slug' => [
                'type' => 'text'
            ],
        ],
    ];

    public function toSearchableArray()
    {
        return [
            'name_ru' => $this->getTranslation('name', 'ru') ?? null,
            'name_ua' => $this->getTranslation('name', 'ua') ?? null,
            'name_by' => $this->getTranslation('name', 'by') ?? null,
            'name_pl' => $this->getTranslation('name', 'pl') ?? null,
            'name_kz' => $this->getTranslation('name', 'kz') ?? null,
            'color_ru' => $this->color->getTranslation('name', 'ru') ?? null,
            'color_ua' => $this->color->getTranslation('name', 'ua') ?? null,
            'color_by' => $this->color->getTranslation('name', 'by') ?? null,
            'color_pl' => $this->color->getTranslation('name', 'pl') ?? null,
            'color_kz' => $this->color->getTranslation('name', 'kz') ?? null,
            'slug' => @$this->slug,
        ];
    }

    protected $casts = [
        'deleted_at' => 'datetime',
        'name' => 'object',
        'information' => 'object',
        'description' => 'object'
    ];

    public $translatable = [
        'name',
        'information',
        'description',
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'product');
    }

    public function markers(): BelongsToMany
    {
        return $this->belongsToMany(Marker::class)
            ->using(CarpetMarker::class);
    }

    public function manufacturer(): BelongsTo
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
