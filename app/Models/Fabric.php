<?php

namespace App\Models;

use App\Elastic\Configurators\FabricIndexConfigurator;
use App\Elastic\Rules\FabricSearchRule;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use ScoutElastic\Searchable;
use Spatie\Translatable\HasTranslations;

class Fabric extends ProductEstate
{
    use HasTranslations,
        SoftDeletes,
        Searchable;

    protected $table = 'fabrics';

    protected $softDelete = true;

    protected $fillable = [
        'code',
        'name',
        'manufacturer_id',
        'fabric_type_id',
        'fabric_collection_id',
        'filter_color_id',
        'color_id',
        'slug',
        'information',
        'description',
    ];

    protected $indexConfigurator = FabricIndexConfigurator::class;

    protected $searchRules = [FabricSearchRule::class];

    protected $mapping = [
        'properties' => [
            'description_ru' => [
                'type' => 'text'
            ],
            'description_ua' => [
                'type' => 'text'
            ],
            'description_by' => [
                'type' => 'text'
            ],
            'description_pl' => [
                'type' => 'text'
            ],
            'description_kz' => [
                'type' => 'text'
            ],
            'slug' => [
                'type' => 'text'
            ],
            'fabric_collection' => [
                'type' => 'text'
            ],
            'fabric_type_ru' => [
                'type' => 'text'
            ],
            'fabric_type_ua' => [
                'type' => 'text'
            ],
            'fabric_type_by' => [
                'type' => 'text'
            ],
            'fabric_type_pl' => [
                'type' => 'text'
            ],
            'fabric_type_kz' => [
                'type' => 'text'
            ]
        ],
    ];

    public function toSearchableArray()
    {
        return [
            'description_ru' => $this->getTranslation('description', 'ru') ?? null,
            'description_ua' => $this->getTranslation('description', 'ua') ?? null,
            'description_by' => $this->getTranslation('description', 'by') ?? null,
            'description_pl' => $this->getTranslation('description', 'pl') ?? null,
            'description_kz' => $this->getTranslation('description', 'kz') ?? null,
            'fabric_type_ru' => isset($this->fabricType->name) ? $this->fabricType->getTranslation('name', 'ru') : null,
            'fabric_type_ua' => isset($this->fabricType->name) ? $this->fabricType->getTranslation('name', 'ua') : null,
            'fabric_type_by' => isset($this->fabricType->name) ? $this->fabricType->getTranslation('name', 'by') : null,
            'fabric_type_pl' => isset($this->fabricType->name) ? $this->fabricType->getTranslation('name', 'pl') : null,
            'fabric_type_kz' => isset($this->fabricType->name) ? $this->fabricType->getTranslation('name', 'kz') : null,
            'color_ru' => isset($this->color->name) ? $this->color->getTranslation('name', 'ru') : null,
            'color_ua' => isset($this->color->name) ? $this->color->getTranslation('name', 'ua') : null,
            'color_by' => isset($this->color->name) ? $this->color->getTranslation('name', 'by') : null,
            'color_pl' => isset($this->color->name) ? $this->color->getTranslation('name', 'pl') : null,
            'color_kz' => isset($this->color->name) ? $this->color->getTranslation('name', 'kz') : null,
            'fabric_collection' => isset($this->fabricCollection->name) ? $this->fabricCollection->name : null,
            'slug' => @$this->slug,
        ];
    }

    const MEDIA_IMAGE = "fabric_image";
    const MEDIA_VIDEO = "fabric_video";

    protected $casts = [
        'deleted_at' => 'datetime',
        'fields' => 'object',
        'name' => 'object',
        'description' => 'object',
        'information' => 'object'
    ];

    public $translatable = [
        'information',
        'description',
        'name'
    ];

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'product');
    }

    public function markers(): BelongsToMany
    {
        return $this->belongsToMany(Marker::class)
            ->using(FabricMarker::class);
    }

    public function cares(): BelongsToMany
    {
        return $this->belongsToMany(
            Care::class,
            'care_fabric',
            'fabric_id',
            'care_id',
            'id',
            'id'
        );
    }

    public function manufacturer(): BelongsTo
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function fabricType(): BelongsTo
    {
        return $this->belongsTo(FabricType::class);
    }

    public function fabricCollection(): BelongsTo
    {
        return $this->belongsTo(FabricCollection::class);
    }

    public function filterColor(): BelongsTo
    {
        return $this->belongsTo(FilterColor::class);
    }

    public function savedInOrder()
    {
        return $this->morphMany(OrderItem::class, 'product');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function isHit()
    {
        return $this->markers()->where('slug', 'hit')->exists();
    }

    public function isSale()
    {
        return $this->markers()->where('slug', 'sale')->exists();
    }

    public function isSpecial()
    {
        return $this->markers()->where('slug', 'special-offer')->exists();
    }

    public function getMeterPriceAttribute()
    {
        return auth()->user() !== null && UserFabric::where(['fabric_code' => $this->code, 'user_id' => auth()->id()])->first() !== null
            ? UserFabric::where(['fabric_code' => $this->code, 'user_id' => auth()->id()])->first()->price_per_meter
            : $this->price_per_meter;
    }

    public function getRollPriceAttribute()
    {
        return auth()->user() !== null && UserFabric::where(['fabric_code' => $this->code, 'user_id' => auth()->id()])->first() !== null
            ? UserFabric::where(['fabric_code' => $this->code, 'user_id' => auth()->id()])->first()->price_per_roll
            : $this->price_per_roll;
    }
}
