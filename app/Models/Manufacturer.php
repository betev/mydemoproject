<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class Manufacturer extends Model
{
    use HasTranslations;

    protected $table = 'manufacturers';

    protected $fillable = ['name'];

    public $translatable = ['name'];

    public function carpets(): HasMany
    {
        return $this->hasMany(Carpet::class);
    }

    public function fabrics(): HasMany
    {
        return $this->hasMany(Fabric::class);
    }

    public function components(): HasMany
    {
        return $this->hasMany(Component::class);
    }
}
