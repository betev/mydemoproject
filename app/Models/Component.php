<?php

namespace App\Models;

use App\Elastic\Configurators\ComponentIndexConfigurator;
use App\Elastic\Rules\ComponentSearchRule;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use ScoutElastic\Searchable;
use Spatie\Translatable\HasTranslations;

class Component extends ProductEstate
{
    use HasTranslations,
        SoftDeletes,
        Searchable;

    protected $fillable = [
        'code',
        'name',
        'manufacturer_id',
        'category_id',
        'price',
        'presence',
        'publish',
        'delete',
        'slug',
        'information',
        'description',
    ];

    protected $indexConfigurator = ComponentIndexConfigurator::class;

    protected $searchRules = [ComponentSearchRule::class];

    protected $softDelete = true;

    protected $mapping = [
        'properties' => [
            'name_ru' => [
                'type' => 'text'
            ],
            'name_ua' => [
                'type' => 'text'
            ],
            'name_by' => [
                'type' => 'text'
            ],
            'name_pl' => [
                'type' => 'text'
            ],
            'name_kz' => [
                'type' => 'text'
            ],
            'slug' => [
                'type' => 'text'
            ],
        ],
    ];

    public function toSearchableArray()
    {
        return [
            'name_ru' => $this->getTranslation('name', 'ru') ?? null,
            'name_ua' => $this->getTranslation('name', 'ua') ?? null,
            'name_by' => $this->getTranslation('name', 'by') ?? null,
            'name_pl' => $this->getTranslation('name', 'pl') ?? null,
            'name_kz' => $this->getTranslation('name', 'kz') ?? null,
            'slug' => @$this->slug,
            'code' => $this->code
        ];
    }

    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    public $translatable = [
        'name',
        'information',
        'description',
    ];

    const MEDIA_IMAGE = "component_image";
    const MEDIA_VIDEO = "component_video";

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'product');
    }

    public function markers(): BelongsToMany
    {
        return $this->belongsToMany(Marker::class)
            ->using(ComponentMarker::class);
    }

    public function manufacturer(): BelongsTo
    {
        return $this->belongsTo(Manufacturer::class);
    }

    public function color(): BelongsTo
    {
        return $this->belongsTo(Color::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(ComponentCategory::class);
    }
}
