<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class FabricType extends Model
{
    use HasTranslations;

    protected $table = 'fabric_types';

    protected $fillable = ['name'];

    public $translatable = ['name'];

    public function fabrics(): HasMany
    {
        return $this->hasMany(Fabric::class);
    }
}
