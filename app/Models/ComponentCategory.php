<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ComponentCategory extends Model
{
    use HasTranslations;

    public $translatable = [
        'name'
    ];

    public function getTitleAttribute()
    {
        return $this->name->ru;
    }

    public function components()
    {
        return $this->hasMany(Component::class);
    }
}
