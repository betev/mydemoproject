<?php

namespace App\Mail;

use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendNotification extends Mailable
{
    use Queueable, SerializesModels;

    protected $notification;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($media = $this->notification->getMedia(Notification::NOTIFICATION_FILE)) {
            foreach ($media as $item) {
                $this->attach($item->getPath());
            }
        }
        return $this
            ->subject($this->notification->title)
            ->markdown('mail.send-notification', [
                'title' => $this->notification->title,
                'body' => $this->notification->text,
                //'notificationIcon' => $this->notification->getFirstMediaUrl(Notification::NOTIFICATION_ICON) ?? '',
            ]);
    }
}
