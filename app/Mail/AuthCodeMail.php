<?php

namespace App\Mail;

use App\Models\AuthCode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AuthCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var AuthCode
     */
    public AuthCode $authCode;

    /**
     * Create a new message instance.
     *
     * @param AuthCode $authCode
     */
    public function __construct(AuthCode $authCode)
    {
        $this->authCode = $authCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(__('Ваш код для входа'))
            ->markdown('mail.auth-code', [
                'authCode' => $this->authCode,
            ]);
    }
}
