<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ClientCreatedOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $message_subject;
    public $body;

    /**
     * Create a new message instance.
     *
     * @param mixed $subject
     * @param mixed $body
     *
     * @return void
     */
    public function __construct(Order $order, $subject, $body)
    {
        $this->order = $order;
        $this->message_subject = $subject;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->message_subject)
            ->markdown('mail.client-order-confirmation', [
                'order' => $this->order, 'body' => $this->body
            ]);
    }
}
