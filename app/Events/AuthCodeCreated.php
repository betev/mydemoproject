<?php

namespace App\Events;

use App\Models\AuthCode;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AuthCodeCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var AuthCode
     */
    public AuthCode $authCode;

    /**
     * Create a new event instance.
     *
     * @param AuthCode $authCode
     */
    public function __construct(AuthCode $authCode)
    {
        $this->authCode = $authCode;
    }
}
