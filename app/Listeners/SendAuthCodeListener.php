<?php

namespace App\Listeners;

use App\AuthCode\AuthCodeSenderFactory;
use App\Events\AuthCodeCreated;
use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAuthCodeListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param AuthCodeCreated $event
     *
     * @throws Exception
     *
     * @return void
     */
    public function handle(AuthCodeCreated $event)
    {
        if (!$authCode = $event->authCode) {
            return;
        }

        $authCodeSender = AuthCodeSenderFactory::getAuthCodeSender($authCode->type);
        $authCodeSender->send($authCode);
    }
}
