<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string',
            'type' => [
                'required',
                'string',
                Rule::in([CodeRequest::EMAIL_TYPE, CodeRequest::PHONE_TYPE]),
            ],
            'login' => 'required|string',
        ];
    }
}
