<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CodeRequest extends FormRequest
{
    const EMAIL_TYPE = 'email';
    const PHONE_TYPE = 'phone';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|string',
            'type' => [
                'required',
                Rule::in([static::EMAIL_TYPE, static::PHONE_TYPE]),
            ],
        ];
    }
}
