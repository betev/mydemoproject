<?php


namespace App\Http;

use Illuminate\Container\Container;
use Illuminate\Support\Str;
use ReflectionFunctionAbstract;

class CustomControllerDispatcher extends \Illuminate\Routing\ControllerDispatcher
{
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * Resolve the given method's type-hinted dependencies.
     *
     * @param array $parameters
     * @param \ReflectionFunctionAbstract $reflector
     *
     * @return array
     */
    public function resolveMethodDependencies(array $parameters, ReflectionFunctionAbstract $reflector)
    {
        $instanceCount = 0;

        $values = array_values($parameters);

        $skippableValue = new \stdClass;

        foreach ($reflector->getParameters() as $key => $parameter) {
            $instance = $this->transformDependency($parameter, $parameters, $skippableValue);

            if ($instance !== $skippableValue) {
                $instanceCount++;

                $this->spliceIntoParameters($parameters, $key, $instance);
            } elseif (!isset($values[$key - $instanceCount]) &&
                $parameter->isDefaultValueAvailable()) {
                $this->spliceIntoParameters($parameters, $key, $parameter->getDefaultValue());
            } else {
                $paramName = Str::snake($parameter->name);
                // matching inputted params with controller params
                if (isset($parameters[$paramName])) {
                    $parameters [$key] = $parameters[$paramName];
                    unset($parameters[$paramName]);
                    ksort($parameters, SORT_STRING);
                }
            }
        }

        return $parameters;
    }
}
