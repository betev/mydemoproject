<?php

/*
 * You can define global functions in this file
 */


use App\Tools\Session;
use Illuminate\Support\Facades\Route;

function route($name, $parameters = [], $absolute = true)
{
    $routeParameterNames = optional(Route::getRoutes()->getByName($name))->parameterNames() ?: [];

    if (in_array('locale', $routeParameterNames)) {
        $parameters = array_merge($parameters, [
            'locale' => app()->getLocale(),
        ]);
    }

    if (in_array('country', $routeParameterNames)) {
        $country = Route::current()->parameter('country') ?: session(Session::KEY_COUNTRY);

        if (!$country) {
            if (in_array(config('app.env'), ['local', 'dev'])) {
                $country = 'ru';
            } else {
                abort(404);
            }
        }

        $parameters = array_merge($parameters, [
            'country' => $country,
        ]);
    }

    return app('url')->route($name, $parameters, $absolute);
}
