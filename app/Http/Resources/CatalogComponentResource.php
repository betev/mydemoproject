<?php

namespace App\Http\Resources;

use App\Models\Component;
use Illuminate\Http\Resources\Json\JsonResource;

class CatalogComponentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->name,
            'link' => route('accessories', [$this->id, $this->slug]),
            'images' => [
                $this->getFirstMediaUrl(Component::MEDIA_IMAGE),
            ],
            'labels' => [
                'hit' => true,
                'sale' => false,
                'special' => false,
            ],
        ];
    }
}
