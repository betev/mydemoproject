<?php


namespace App\Http\Resources;

use App\Models\Fabric;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CatalogFabricResource extends JsonResource
{
    public function toArray($request)
    {
        $defaultLocale = config('app.locale');
        $media = $this->getMedia(Fabric::MEDIA_IMAGE);
        $imageUrls = null;
        if (count($media)) {
            $imageUrls = [
                'image_248' => $media[0]->getUrl('248'),
                'image_330' => $media[0]->getUrl('330'),
                'image_660' => $media[0]->getUrl('660')
            ];
        }
        return [
            'title' => $this->getTranslation('name', $defaultLocale) ?: Str::upper($this->slug),
            'link' => route('product', [$this->id, $this->slug]),
            'images' => $imageUrls ? [$imageUrls] : [],
            'labels' => [
                'hit' => $this->isHit(),
                'sale' => $this->isSale() ? true : false,
                'special' => $this->isSpecial() ? true : false,
            ],
        ];
    }
}
