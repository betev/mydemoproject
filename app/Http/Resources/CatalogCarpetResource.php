<?php

namespace App\Http\Resources;

use App\Models\Carpet;
use Illuminate\Http\Resources\Json\JsonResource;

class CatalogCarpetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->name,
            'link' => route('carpets', [$this->id, $this->slug]),
            'images' => [
                $this->getFirstMediaUrl(Carpet::MEDIA_IMAGE),
            ],
            'labels' => [
                'hit' => true,
                'sale' => false,
                'special' => false,
            ],
        ];
    }
}
