<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        foreach ($request->all() as $item) {
        }

        return [
            'title' => $this->size,
            'name' => $this->size,
            'checked' => false,
        ];
    }
}
