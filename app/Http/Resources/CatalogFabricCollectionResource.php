<?php

namespace App\Http\Resources;

use App\Models\Fabric;
use App\Models\Marker;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class CatalogFabricCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $defaultLocale = config('app.locale');
        $imageUrls = null;
        $fabrics = $this->fabrics;
        foreach ($fabrics as $fabric) {
            $media = $fabric->getMedia(Fabric::MEDIA_IMAGE);
            if (count($media)) {
                $imageUrls = [
                    'image_248' => $media[0]->getUrl('248'),
                    'image_330' => $media[0]->getUrl('330'),
                    'image_660' => $media[0]->getUrl('660')
                ];
                break;
            }
        }
        return [
            'title' => $this->name ?: Str::upper($this->slug),
            'link' => route('catalog.collection.fabrics', [$this->id]),
            'images' => $imageUrls ? [$imageUrls] : [],
            'labels' => [
                'hit' => $this->marker_id === Marker::whereSlug('hit')->first()->id,
                'sale' => $this->marker_id === Marker::whereSlug('sale')->first()->id,
                'special' => $this->marker_id === Marker::whereSlug('special-offer')->first()->id,
            ],
        ];
    }
}
