<?php

namespace App\Http\ViewComposers;

use App\Models\Settings;
use Illuminate\View\View;

class QrComposer
{
    public function compose(View $view)
    {
        $settingsHeader = Settings::whereSlug(Settings::HEADER_SLUG)->first();
        $catalog = optional($settingsHeader)->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_CATALOG));
        $view->with([
            'catalog' => $catalog
        ]);
    }

}
