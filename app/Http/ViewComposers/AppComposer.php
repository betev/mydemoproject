<?php


namespace App\Http\ViewComposers;

use App\JS\JS;
use App\Models\Locale;
use App\Models\Settings;
use App\Services\Navigation\NavigationFacade;
use App\Tools\CurrentCountry;
use Illuminate\Support\Str;
use Illuminate\View\View;

class AppComposer
{
    public function compose(View $view)
    {
        $locale = app()->getLocale();
        JS::translations(
            resource_path('lang/' . $locale . '/messages.json'),
            resource_path('lang/' . $locale . '/catalog.php'),
            resource_path('lang/' . $locale . '/product.php'),
            resource_path('lang/' . $locale . '.json'),
        );

        JS::provideToScript([
            'translations' => JS::allTranslations(),
        ]);

        $settings = Settings::whereSlug(Settings::METRICS_SLUG)->first();
        $fields = $settings && $settings->fields ? $settings->fields : [];

        $settingsHeader = Settings::whereSlug(Settings::HEADER_SLUG)->first();
        $headerFields = $settingsHeader && $settingsHeader->fields ? $settingsHeader->locale_fields : [];
//        $catalogueCollectionName = 'MEDIA_CATALOG_'.Str::upper($locale);
//        $catalog = optional($settingsHeader)
//            ->getFirstMediaUrl(Settings::getMediaName(
//                (new \ReflectionClass(Settings::class))
//                    ->getConstant($catalogueCollectionName))
//            );
        $catalog = route('get-catalogue-link');
        //$catalog = optional($settingsHeader)->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_CATALOG));

        $settingsSocial = Settings::whereSlug(Settings::SOCIAL_SLUG)->first();
        $socialFields = $settingsSocial && $settingsSocial->fields ? $settingsSocial->fields : [];

        $settingsContacts = Settings::whereSlug(Settings::CONTACTS_SLUG)->first();
        $contactsFields = $settingsContacts && $settingsContacts->fields ? $settingsContacts->fields: [];
        $addressFields = $settingsContacts && $settingsContacts->fields ? $settingsContacts->fields : [];
        $addresses = $addressFields && isset($addressFields->{$locale}) && isset($addressFields->{$locale}->addresses)
            ? $addressFields->{$locale}->addresses
            : [];
        foreach ($addresses as $address) {
            $address->address = $address->attributes->address;
            $address->is_active = $address->attributes->is_active;
            unset($address->key, $address->layout, $address->attributes);
        }
        $phones = $contactsFields && isset($contactsFields->{$locale}) && isset($contactsFields->{$locale}->phones)
            ? $settingsContacts->fields->{$locale}->phones
            : [];
        if ($phones) {
            foreach ($phones as $phone) {
                $phone->phone = $phone->attributes->phone;
                unset($phone->key, $phone->layout, $phone->attributes);
            }
        }

        $settingsMailToDirector = Settings::whereSlug(Settings::MAIL_TO_DIRECTOR_SLUG)->first();
        $mailToDirectorFields = $settingsMailToDirector && $settingsMailToDirector->fields ? $settingsMailToDirector->locale_fields: [];

        $settingsLegal = Settings::whereSlug(Settings::LEGAL_SLUG)->first();
        $legalFields = $settingsLegal && $settingsLegal->fields ? $settingsLegal->locale_fields: [];

        $settingsContract = Settings::whereSlug(Settings::CONTRACT_SLUG)->first();
        $contractFields = $settingsContract && $settingsContract->fields ? $settingsContract->locale_fields: [];
        $contract = $settingsContract && $settingsContract->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_CONTRACT))
            ? $settingsContract->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_CONTRACT))
            : '#!';

        $settingsPrivacy = Settings::whereSlug(Settings::PRIVACY_SLUG)->first();
        $privacyFields = $settingsPrivacy && $settingsPrivacy->fields ? $settingsPrivacy->locale_fields: [];
        $privacy = optional($settingsPrivacy) ->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_PRIVACY));

        $subscription_popup_settings = Settings::whereSlug(Settings::SUBSCRIPTION_MODAL_SETTINGS)->first();

        $locales = Locale::where('active', true)->get();
        $defaultLocale = config('app.locale');
        $languageFields = [];
        $languageFields[$defaultLocale] = 'On';
        if ($locales) {
            foreach ($locales as $locale) {
                if ($locale->code !== $defaultLocale) {
                    $languageFields[$locale->code] = 'On';
                }
            }
        }

        $globalNavigation = NavigationFacade::getNavigation();

        $accountData['menu'] = collect($globalNavigation->tree())->flatMap(function ($e) {
            $result = [];
            if (auth()->check()) {
                if (!empty($e['children'])) {
                    foreach ($e['children'] as $child) {
                        if ($child['title'] === __('Личный кабинет')) {
                            $result['profile'] = $child['children'];
                        } else {
                            $result[] = $child;
                        }
                    }
                    return $result;
                }
            } else {
                foreach ($e['children'] as $child) {
                    return $result[] = [$child];
                }
            }
            return $result;
        })->toArray();

        $view->with([
            'beginScripts' => isset($fields) && isset($fields->{CurrentCountry::get()->slug})
                ? $fields->{CurrentCountry::get()->slug}->scripts_begin
                : '',
            'endScripts' => isset($fields) && isset($fields->{CurrentCountry::get()->slug})
                ? $fields->{CurrentCountry::get()->slug}->scripts_end
                : '',
            'headScripts' => isset($fields) && isset($fields->{CurrentCountry::get()->slug})
                ? $fields->{CurrentCountry::get()->slug}->scripts_head
                : '',
            'headerFields' => $headerFields,
            'socialFields' => $socialFields,
            'contactsFields' => $contactsFields,
            'addressFields' => $addressFields,
            'mailToDirectorFields' => $mailToDirectorFields,
            'legalFields' => $legalFields,
            'catalog' => $catalog,
            'contract' => $contract,
            'contractFields' => $contractFields,
            'privacy' => $privacy,
            'privacyFields' => $privacyFields,
            'languageFields' => $languageFields,
            'accountData' => $accountData,
            //'breadcrumbs' => $breadcrumbs,
            'globalNavigation' => $globalNavigation,
            'addresses' => $addresses,
            'phones' => $phones,
            'subscription_popup_settings' => $subscription_popup_settings
        ]);
    }
}
