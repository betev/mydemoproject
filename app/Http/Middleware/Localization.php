<?php

namespace App\Http\Middleware;

use App\Models\Country;
use App\Models\Locale;
use App\Tools\Session;
use Closure;
use Illuminate\Support\Facades\App;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->route()->parameter('locale');

        $country = Country::where('slug', config('app.country_code'))->first();
        $locales = Locale::all()->pluck('code')->toArray();

        if (!in_array($locale, $locales)) {
            $locale = config('app.locale') ?? config('translatable.fallback_locale');
        }

        App::setLocale($locale);

        session()->put(Session::KEY_LOCALE, $locale);
        session()->save();

        return $next($request);
    }
}
