<?php

namespace App\Http\Controllers;

use App\Http\Resources\CatalogFabricCollectionResource;
use App\Models\Fabric;
use App\Models\FabricCollection;
use App\Models\Settings;
use App\Repositories\FabricCollectionRepository;
use App\Repositories\FabricRepository;
use App\Services\CatalogService;
use Illuminate\Http\Request;
use App\Http\Resources\CatalogFabricResource;

class CatalogController extends Controller
{
    private CatalogService $service;

    public function __construct(CatalogService $service)
    {
        $this->service = $service;
    }

    public function fabric(FabricCollection $collection = null)
    {
        return view('catalog.index', $this->service->fabric($collection));
    }

    public function fabrics()
    {
        return $this->service->fabric();
    }

    public function fabricCollection()
    {
        return view('catalog.index', $this->service->fabrics());
//        return view('catalog.index', $this->service->fabricCollection());
    }

    public function next(FabricCollection $collection = null)
    {
        return $this->service->next(FabricRepository::class, CatalogFabricResource::class, $collection);
    }

    public function filters(Request $request, FabricCollection $collection)
    {
        return $this->service->fabricsByFilters(FabricRepository::class, CatalogFabricResource::class, $collection);
    }

    public function getFabricCollections(Request $request)
    {
        return $this->service->getByFilters(FabricCollectionRepository::class, CatalogFabricCollectionResource::class);
    }

    public function filteredFabrics(Request $request)
    {
        return $this->service->fabricsByFilters(FabricRepository::class, CatalogFabricResource::class);
    }

    public function getCatalogLink()
    {
        $settingsHeader = Settings::whereSlug(Settings::HEADER_SLUG)->first();
        $catalog = optional($settingsHeader)->getFirstMediaUrl(Settings::getMediaName(Settings::MEDIA_CATALOG));
        return redirect($catalog);
    }
}
