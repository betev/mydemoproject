<?php

namespace App\Http\Controllers;

use App\Models\Carpet;
use App\Models\Component;
use App\Models\Fabric;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private ProductService $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }

    /**
     * @param Fabric $fabric
     * @param mixed $locale
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fabric(Fabric $fabric, Request $request)
    {
        return view('product.show', $this->service->showFabric($fabric));
    }

    public function carpet(Carpet $carpet)
    {
        return view('product.show', $this->service->showCarpet($carpet));
    }

    public function component(Component $component)
    {
        return view('product.show', $this->service->showComponent($component));
    }
}
