<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{

    /**
     * Redirect to provider
     *
     * @param $provider
     * @param mixed $locale
     *
     * @return mixed
     */
    public function show($locale, $provider)
    {
        try {
            $scopes = config("services.$provider.scopes") ?? [];
            if (count($scopes) === 0) {
                return Socialite::driver($provider)->redirect();
            }
            return Socialite::driver($provider)->scopes($scopes)->redirect();
        } catch (\Exception $e) {
            abort(404);
        }
    }
}
