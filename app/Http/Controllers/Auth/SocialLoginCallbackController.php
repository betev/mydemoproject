<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Facebook\SignedRequestParser;
use App\Services\SocialUserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginCallbackController extends Controller
{
    /**
     * @var SocialUserService
     */
    private SocialUserService $socialUserService;
    /**
     * @var SignedRequestParser
     */
    private SignedRequestParser $signedRequestParser;

    public function __construct(SocialUserService $socialUserService, SignedRequestParser $signedRequestParser)
    {
        $this->socialUserService = $socialUserService;
        $this->signedRequestParser = $signedRequestParser;
    }

    /**
     * @param $locale
     * @param $provider
     *
     * @return Application|RedirectResponse|Redirector
     */
    public function show($provider)
    {
        $data = Socialite::driver($provider)->user();
        try {
            return $this->socialUserService->handleSocialUser($provider, $data);
        } catch (Exception $e) {
            return redirect('login')->withErrors(['authentication_deny' => 'Login with '.ucfirst($provider).' failed. Please try again.']);
        }
    }

    /**
     * @param Request $request
     * @param mixed $provider
     *
     * @return JsonResponse|null
     */
    public function destroy(Request $request, $provider): ?JsonResponse
    {
        if ($request->method() === 'GET') {
            $confirmationCode = $request->query('id');
            $user = User::whereRaw("(social-> '{$provider}' ->> 'confirmation_code') = '{$confirmationCode}'")->exists();

            if ($user) {
                $user->delete();
                return response('User data was successfully deleted', 200);
            }
            return response('Invalid confirmation code', 404);
        }

        if ($provider === User::FACEBOOK_SOCIAL_LOGIN) {
            $data = $this->signedRequestParser->parse($request->input('signed_request'));
            if (!$data) {
                return null;
            }

            $user = User::whereRaw("(social-> '{$provider}' ->> 'id')::dec = '{$data['user_id']}'")->first();

            $deletionResponse = $this->socialUserService->deleteFacebookSocialData($user, $data);

            return response()->json($deletionResponse);
        }

        return null;
    }
}
