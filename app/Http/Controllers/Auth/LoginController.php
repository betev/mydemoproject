<?php

namespace App\Http\Controllers\Auth;

use App\Events\AuthCodeCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\CodeRequest;
use App\Http\Requests\LoginRequest;
use App\Models\AuthCode;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        //$this->redirectTo = url()->previous();
    }

    public function code(CodeRequest $request)
    {
        $userColumn = $request->type === 'phone' ? 'phone' : 'email';
        $login = $request->type === 'phone'
            ? '+'.preg_replace('/[^0-9]/', '', $request->login)
            : $request->login;
        $user = User::where($userColumn, $login)->first();

        if (!$user) {
            return response(['message' => __('Пользователь не найден. Проверьте правильность данных или зарегистрируйтесь')], 422);
        }
        $authCode = AuthCode::firstOrNew([
            'login' => $request->login,
            'type' => $request->type,
        ]);
        $authCode->generateCode();
        $authCode->expires_at = now()->addMinutes(5);
        $authCode->save();

        event(new AuthCodeCreated($authCode));

        if (in_array(config('app.env'), ['local','dev'])) {
            $code = $authCode->code;
        }

        return response()->json([
            'status' => 'success',
            'data' => [],
        ]);
    }

    public function login(LoginRequest $request)
    {
        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }

        $data = $request->validated();

        $userColumn = $data['type'] === 'phone' ? 'phone' : 'email';
        $user = User::where($userColumn, $data['login'])->first();

        if (!$user) {
            return response()->json([
                'data' => [
                    'is_registered' => false,
                ]
            ]);
        }

        $authCode = AuthCode::where('login', $data['login'])
            ->where('type', $data['type'])
            ->where('code', $data['code'])
            ->where('expires_at', '>', now())
            ->first();

        if (!$authCode) {
            $response = [
                'status' => 404,
                'message' => 'Неверный код',
            ];

            return response()->json($response, $response['status']);
        }

        auth()->login($user);

        if ($userColumn === 'email') {
            $user->markEmailAsVerified();
        }

        $authCode->remove();

        return response()->json([
            'data' => [
                'is_registered' => true,
                'link' =>  redirect()->intended('/'.app()->getLocale().'/')->getTargetUrl(),
            ],
        ]);
    }

    public function showLoginForm()
    {
        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }

        $socialNetworksUrl = [
            'instagram' => route('social-login.show', ['provider' => User::INSTAGRAM_SOCIAL_LOGIN]),
            'facebook' => route('social-login.show', ['provider' => User::FACEBOOK_SOCIAL_LOGIN]),
            'google' => route('social-login.show', ['provider' => User::GOOGLE_SOCIAL_LOGIN]),
        ];

        return view('auth.login')->with(compact('socialNetworksUrl'));
    }

    public function showLoginCheckoutForm()
    {
        if (!session()->has('url.intended')) {
            session(['url.intended' => url()->previous()]);
        }

        $socialNetworksUrl = [
            'instagram' => route('social-login.show', ['provider' => User::INSTAGRAM_SOCIAL_LOGIN]),
            'facebook' => route('social-login.show', ['provider' => User::FACEBOOK_SOCIAL_LOGIN]),
            'google' => route('social-login.show', ['provider' => User::GOOGLE_SOCIAL_LOGIN]),
        ];
        return view('auth.checkout')->with(compact('socialNetworksUrl'));
    }
}
