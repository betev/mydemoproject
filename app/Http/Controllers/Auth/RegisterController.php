<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Tools\CurrentCountry;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     *
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return response()->json([
            'data' => [
                'link' => redirect()->intended('/'.app()->getLocale().'/')->getTargetUrl(),
            ],
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'phone' => 'required|string|max:30|unique:users,phone',
            /*'password' => 'required|string|same:confirm_password',
            'confirm_password' => 'required|string'*/
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     *
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $address_fields = $this->addressFields();
        if (isset($data['address']) && $data['address'][0]['value'] !== '') {
            $address_fields['id'] = Uuid::uuid4()->getHex()->toString();
            $address_fields['is_default'] = true;
            foreach ($data['address'] as $field) {
                foreach ($address_fields['fields'] as $key => $address_field) {
                    if ($address_field['name'] == $field['name']) {
                        $address_fields['fields'][$key]['value'] = $field['value'];
                    }
                }
            }
        }
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => '+'.preg_replace('/[^0-9]/', '', $data['phone']),
            'customer_type_id' => 1,
            'country_id' => CurrentCountry::get()->id,
            'fields' => [
                'private_person' => [
                    [
                        'name' => 'name',
                        'type' => 'text',
                        'error' => false,
                        'title' => 'ФИО',
                        'value' => $data['name'],
                        'required' => true,
                        'length_value' => null,
                        'validate_rule' => null
                    ],
                    [
                        'name' => 'phone',
                        'type' => 'text',
                        'error' => false,
                        'title' => 'Телефон',
                        'value' => $data['phone'],
                        'required' => true,
                        'length_value' => null,
                        'validate_rule' => null
                    ],
                    [
                        'name' => 'email',
                        'type' => 'email',
                        'error' => false,
                        'title' => 'email',
                        'value' => $data['email'],
                        'required' => true,
                        'length_value' => null,
                        'validate_rule' => null
                    ]
                ],
                'addresses' => $address_fields['id']
                    ? [$address_fields['id'] => $address_fields]
                    : null
            ],
        ]);

        $user->assignRole(User::REGULAR_ROLE);

        return $user;
    }

    public function redirectPath()
    {
        return redirect()->intended('/'.app()->getLocale().'/');
    }

    public function addressFields()
    {
        return [
            'id' => null,
            'fields' => [
                [
                    'title' => __('Город'),
                    'value' => '',
                    'error' => false,
                    'name' => 'city',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Улица'),
                    'value' => '',
                    'error' => false,
                    'name' => 'street',
                    'type' => '',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Дом'),
                    'value' => '',
                    'error' => false,
                    'name' => 'house_number',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Квартира/офис'),
                    'value' => '',
                    'error' => false,
                    'name' => 'apartment',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Транспортная компания'),
                    'value' => '',
                    'error' => false,
                    'name' => 'delivery_company',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Получатель'),
                    'value' => '',
                    'error' => false,
                    'name' => 'recipient',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => '',
                ],
                [
                    'title' => __('Телефон получателя'),
                    'value' => '',
                    'error' => false,
                    'name' => 'phone',
                    'type' => 'text',
                    'required' => true,
                    'validate_rule' => 'phone',
                ],
            ],
            'is_default' => false,
            'index' => null,
            'saved' => false,
        ];
    }
}
