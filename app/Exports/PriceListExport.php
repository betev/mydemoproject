<?php

namespace App\Exports;

use App\Exports\Sheets\CarpetsPriceListSheet;
use App\Exports\Sheets\ComponentsPriceListSheet;
use App\Exports\Sheets\FabricsPriceListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PriceListExport implements WithMultipleSheets, ShouldAutoSize
{
    use Exportable;

    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new FabricsPriceListSheet();
//        $sheets[] = new CarpetsPriceListSheet();
//        $sheets[] = new ComponentsPriceListSheet();
        return $sheets;
    }
}
