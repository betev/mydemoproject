<?php

namespace App\Exports;

use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ExportTranslations implements FromView, WithColumnWidths
{
    use Exportable;

    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 60,
            'B' => 60,
        ];
    }

    public function view(): View
    {
        $simple = json_decode(file_get_contents(resource_path('lang/'.$this->locale.'.json')), true);
        $catalog = include resource_path('lang/'.$this->locale.'/catalog.php');
        $product = include resource_path('lang/'.$this->locale.'/product.php');
        $profile = include resource_path('lang/'.$this->locale.'/profile.php');
        $catalogArray = $this->mergeAllArraysWithinFile($catalog, 'catalog.');
        $productArray = $this->mergeAllArraysWithinFile($product, 'product.');
        $profileArray = $this->mergeAllArraysWithinFile($profile, 'profile.');
        return view('export.languages', [
            'languages' => array_merge($simple, $catalogArray, $productArray, $profileArray)
        ]);
    }

    public function mergeAllArraysWithinFile($arrays, $prefix = '')
    {
        $finalArray = [];
        foreach ($arrays as $key => $item) {
            if (is_array($item)) {
                foreach ($item as $internalKey => $value) {
                    $finalArray[$prefix.$internalKey] = $value;
                }
            } else {
                $finalArray[$prefix.$key] = $item;
            }
        }
        return $finalArray;
    }
}
