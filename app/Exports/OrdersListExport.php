<?php

namespace App\Exports;

use App\Exports\Sheets\OrdersListSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OrdersListExport implements WithMultipleSheets, ShouldAutoSize
{
    use Exportable;

    public function __construct($models)
    {
        $this->models = $models;
    }

    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new OrdersListSheet($this->models);
        return $sheets;
    }
}
