<?php

namespace App\Exports\Sheets;

use App\Models\Component;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ComponentsPriceListSheet implements FromView, WithTitle
{
    public function view(): View
    {
        return view('export.price-list-component', [
            'components' => Component::where('fields->'.app()->getLocale().'->publish', true)->get()
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        $title = __('Комплектующие');
        return $title;
    }
}
