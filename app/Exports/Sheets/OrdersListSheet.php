<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

class OrdersListSheet implements FromView, WithTitle, ShouldAutoSize
{
    protected $excelFields;

    public function __construct($models)
    {
        $this->excelFields = ['all' => $models->map(function ($order) {
            $promoName = '';
            if (isset($order->promoCode)) {
                $promoName = $order->promoCode->code;
            }
            return [
                'created_at' => $order->created_at,
                'id' => $order->id,
                'userName' => $order->user->name ?? '',
                'userId' => $order->user->id ?? '',
                'userOneSId' => $order->user->one_s_id ?? '',
                'userEmail' => $order->user->email ?? '',
                'userPhone' => (string)$order->user->phone ?? '',
//                    ? preg_replace('/\D/', '', $order->user->phone)
//                    : '',
                'items' => $order->items->map(function ($item) {
                    $unitNames = [
                        'roll' => 'Рулон',
                        'item' => 'Рулон',
                        'meter' => 'Метр'
                    ];

                    $itemUnit = str_contains($item->unit, 'Метраж') ? str_replace('Метраж', 'Метр', $item->unit) : $item->unit;
                    $itemUnit = str_replace('product.', '', $itemUnit);

                    if (isset($item->product)) {
                        return [
                            'itemProductName' => $item->product->name !== ''
                                ? $item->product->name
                                : Str::upper(str_replace('-', ' ', $item->product->slug)),
                            'itemUnit' => $unitNames[$item->unit] ?? $itemUnit,
                            'itemQuantity' => $item->quantity,
                            'itemTotal' => number_format((float)$item->quantity * (float)$item->price, 2),
                            'itemColor' => $item->product->filterColor->name ?: ''
                        ];
                    }
                    return [
                        'itemProductName' => '',
                        'itemUnit' => $unitNames[$item->unit] ?? $itemUnit,
                        'itemQuantity' => $item->quantity,
                        'itemTotal' => number_format((float)$item->quantity * (float)$item->price, 2),
                        'itemColor' => ''
                    ];
                }),
                'price' => $order->price,
                'discount' => $order->discount ?? 0,
                'fullPrice' => $order->fullPrice ?? 0,
                'deliveryAddress' => $order->DeliveryAddress ?? '',
                'pickUpAddress' => $order->PickUpAddress ?? '',
                'comment' => $order->comment ?? '',
                'promoName' => $promoName,
                'currencyTittle' => $order->currency->iso,
            ];
        })];
//        dd($this->excelFields);
    }

    public function view(): View
    {
        return view('export.export-orders', $this->excelFields);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        $title = __('Ткани');
        return $title;
    }
}
