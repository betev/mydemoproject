<?php

namespace App\Exports\Sheets;

use App\Models\Carpet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class CarpetsPriceListSheet implements FromView, WithTitle
{
    public function view(): View
    {
        return view('export.price-list-carpet', [
            'carpets' => Carpet::where('fields->'.app()->getLocale().'->publish', true)->get()
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        $title = __('Ковры');
        return $title;
    }
}
