<?php

namespace App\Exports\Sheets;

use App\Models\FabricCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;

class FabricsPriceListSheet implements FromView, WithTitle, ShouldAutoSize
{
    public function view(): View
    {
        return view('export.price-list-fabric', [
            'collections' => FabricCollection::whereHas('fabrics', function ($q) {
                return $q->where('publish', true);
            })->orderBy('name', 'asc')->get()
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        $title = __('Ткани');
        return $title;
    }
}
