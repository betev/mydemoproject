<?php

namespace App\Elastic\Rules;

use ScoutElastic\SearchRule;

class ComponentSearchRule extends SearchRule
{
    /**
     * {@inheritdoc}
     */
    public function buildHighlightPayload()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function buildQueryPayload()
    {
        return [
            'must' => [
                'multi_match' => [
                    'query' => $this->builder->query,
                    'fuzziness' => 'AUTO',
                    'fields' => [
                        'name_ru',
                        'name_ua',
                        'name_by',
                        'name_pl',
                        'name_kz',
                        'code'
                    ]
                ]
            ]
        ];
    }
}
