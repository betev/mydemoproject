<?php

namespace App\Elastic\Rules;

use ScoutElastic\SearchRule;

class CarpetSearchRule extends SearchRule
{
    /**
     * {@inheritdoc}
     */
    public function buildHighlightPayload()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function buildQueryPayload()
    {
        return [
            'must' => [
                'multi_match' => [
                    'query' => $this->builder->query,
                    'fuzziness' => 'AUTO',
                    'fields' => [
                        'name_ru',
                        'name_ua',
                        'name_by',
                        'name_pl',
                        'name_kz',
                        'color_ru',
                        'color_ua',
                        'color_by',
                        'color_pl',
                        'color_kz',
                    ]
                ]
            ]
        ];
    }
}
