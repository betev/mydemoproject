<?php

namespace App\Elastic\Rules;

use ScoutElastic\SearchRule;

class FabricSearchRule extends SearchRule
{
    /**
     * {@inheritdoc}
     */
    public function buildHighlightPayload()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function buildQueryPayload()
    {
        return [
            'must' => [
                'multi_match' => [
                    'query' => $this->builder->query,
                    'fuzziness' => 'AUTO',
                    'fields' => [
                        'description_ru',
                        'description_ua',
                        'description_by',
                        'description_pl',
                        'description_kz',
                        'slug',
                        'fabric_collection',
                        'fabric_type_ru',
                        'fabric_type_ua',
                        'fabric_type_by',
                        'fabric_type_pl',
                        'fabric_type_kz',
                        'color_ru',
                        'color_ua',
                        'color_by',
                        'color_pl',
                        'color_kz',
                    ]
                ]
            ]
        ];
    }
}
