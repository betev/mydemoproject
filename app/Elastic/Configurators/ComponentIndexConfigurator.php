<?php

namespace App\Elastic\Configurators;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class ComponentIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'components';

    /**
     * @var array
     */
    protected $settings = [
        
    ];
}
