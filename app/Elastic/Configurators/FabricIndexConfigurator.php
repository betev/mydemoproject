<?php

namespace App\Elastic\Configurators;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class FabricIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'fabrics';

    /**
     * @var array
     */
    protected $settings = [
        
    ];
}
