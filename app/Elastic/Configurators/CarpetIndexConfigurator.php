<?php

namespace App\Elastic\Configurators;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class CarpetIndexConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'carpets';

    /**
     * @var array
     */
    protected $settings = [
        
    ];
}
