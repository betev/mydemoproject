<?php

namespace App\Observers;

use App\Models\Fabric;
use App\Models\FabricCollection;
use Illuminate\Support\Facades\DB;

class FabricCollectionObserver
{
    public function created(FabricCollection $collection)
    {
        if ($collection->marker_id) {
            $fabrics = Fabric::where('fabric_collection_id', $collection->id)->get();
            foreach ($fabrics as $fabric) {
                $fabric->markers()->sync($collection->marker_id);
            }
        } else {
            DB::table('fabric_marker')
                ->whereIn('fabric_id', Fabric::where('fabric_collection_id', $collection->id)->select('id')->get())
                ->delete();
        }
    }

    public function updated(FabricCollection $collection)
    {
        if ($collection->marker_id) {
            DB::table('fabric_marker')
                ->whereIn('fabric_id', Fabric::where('fabric_collection_id', $collection->id)->select('id')->get())
                ->delete();
            $fabrics = Fabric::where('fabric_collection_id', $collection->id)->get();
            foreach ($fabrics as $fabric) {
                $fabric->markers()->sync($collection->marker_id);
            }
        } else {
            DB::table('fabric_marker')->whereIn('fabric_id', Fabric::where('fabric_collection_id', $collection->id)->select('id')->get())->delete();
        }
    }
}
