<?php

namespace App\AuthCode;

use App\Models\AuthCode;
use GreenSMS\GreenSMS;

class SmsSender implements AuthCodeSender
{
    protected $client;

    public function __construct()
    {
        $this->client = app()->make(GreenSMS::class, config('services.greensms'));
    }

    public function send(AuthCode $authCode)
    {
        try {
            if (!in_array(config('app.env'), ['local', 'dev'])) {
                $this->client->sms->send([
                    'to' => preg_replace("/[^0-9]/", "", $authCode->login),
                    'txt' => $authCode->code
                ]);
            }
        } catch (\Exception $e) {
            logger()
                ->channel('sms_auth')
                ->alert("Auth code {$authCode->code} for login {$this->login} was not sent! {$e->getMessage()}");
        }
    }
}
