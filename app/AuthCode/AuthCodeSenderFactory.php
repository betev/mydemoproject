<?php

namespace App\AuthCode;

class AuthCodeSenderFactory
{
    const TYPE_EMAIL = 'email';
    const TYPE_PHONE = 'phone';

    /**
     * @param string $type
     *
     * @throws \Exception
     *
     * @return AuthCodeSender
     */
    public static function getAuthCodeSender(string $type): AuthCodeSender
    {
        switch ($type) {
            case static::TYPE_EMAIL:
                return new MailSender;
            case static::TYPE_PHONE:
                return new SmsSender;
            default:
                throw new \Exception('Undefined type');
        }
    }
}
