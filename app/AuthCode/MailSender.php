<?php

namespace App\AuthCode;

use App\Mail\AuthCodeMail;
use App\Models\AuthCode;
use Illuminate\Support\Facades\Mail;

class MailSender implements AuthCodeSender
{
    public function send(AuthCode $authCode)
    {
        Mail::to($authCode->login)->send(new AuthCodeMail($authCode));
    }
}
