<?php

namespace App\AuthCode;

use App\Models\AuthCode;

interface AuthCodeSender
{
    public function send(AuthCode $authCode);
}
