<?php

use Laravel\Nova\Http\Middleware\Authenticate;
use Laravel\Nova\Http\Middleware\Authorize;
use Laravel\Nova\Http\Middleware\BootTools;
use Laravel\Nova\Http\Middleware\DispatchServingNovaEvent;

return [
    /*
    |--------------------------------------------------------------------------
    | Nova App Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to display the name of the application within the UI
    | or in other locations. Of course, you're free to change the value.
    |
    */

    'name' => env('APP_NAME', 'Nova Site'),

    /*
    |--------------------------------------------------------------------------
    | Nova App URL
    |--------------------------------------------------------------------------
    |
    | This URL is where users will be directed when clicking the application
    | name in the Nova navigation bar. You are free to change this URL to
    | any location you wish depending on the needs of your application.
    |
    */

    'url' => env('APP_URL', '/'),

    /*
    |--------------------------------------------------------------------------
    | Nova Path
    |--------------------------------------------------------------------------
    |
    | This is the URI path where Nova will be accessible from. Feel free to
    | change this path to anything you like. Note that this URI will not
    | affect Nova's internal API routes which aren't exposed to users.
    |
    */

    'path' => '/admin',

    /*
    |--------------------------------------------------------------------------
    | Nova Authentication Guard
    |--------------------------------------------------------------------------
    |
    | This configuration option defines the authentication guard that will
    | be used to protect your Nova routes. This option should match one
    | of the authentication guards defined in the "auth" config file.
    |
    */

    'guard' => env('NOVA_GUARD', null),

    /*
    |--------------------------------------------------------------------------
    | Nova Password Reset Broker
    |--------------------------------------------------------------------------
    |
    | This configuration option defines the password broker that will be
    | used when passwords are reset. This option should mirror one of
    | the password reset options defined in the "auth" config file.
    |
    */

    'passwords' => env('NOVA_PASSWORDS', null),

    /*
    |--------------------------------------------------------------------------
    | Nova Route Middleware
    |--------------------------------------------------------------------------
    |
    | These middleware will be assigned to every Nova route, giving you the
    | chance to add your own middleware to this stack or override any of
    | the existing middleware. Or, you can just stick with this stack.
    |
    */

    'middleware' => [
        'web',
        Authenticate::class,
        DispatchServingNovaEvent::class,
        BootTools::class,
        Authorize::class,
        \App\Http\Middleware\RenameUploadedFiles::class,
        'optimizeImages' => \Spatie\LaravelImageOptimizer\Middlewares\OptimizeImages::class,
        \Vyuldashev\NovaPermission\ForgetCachedPermissions::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Nova Pagination Type
    |--------------------------------------------------------------------------
    |
    | This option defines the visual style used in Nova's resource pagination.
    | You may choose between 3 types: "simple", "load-more" and "links".
    | Feel free to set this option to the visual style you like.
    |
    */

    'pagination' => 'simple',

    'tinymce_api_key' => env('TINYMCE_API_KEY'),

    'tinymce_options' => [
        'plugins' => [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table directionality',
            'emoticons template paste textcolor textpattern'
        ],
        'style_formats' => [
            [
                'title' => 'Headers',
                'items' => [
                    ['title' => 'Header 1', 'format' => 'h1'],
                    ['title' => 'Header 2', 'format' => 'h2'],
                    ['title' => 'Header 3', 'format' => 'h3'],
                    ['title' => 'Header 4', 'format' => 'h4'],
                    ['title' => 'Header 5', 'format' => 'h5'],
                    ['title' => 'Header 6', 'format' => 'h6']
                ]
            ],
            [
                'title' => 'Inline',
                'items' => [
                    ['title' => 'Bold', 'icon' => 'bold', 'format' => 'bold'],
                    ['title' => 'Italic', 'icon' => 'italic', 'format' => 'italic'],
                    ['title' => 'Underline', 'icon' => 'underline', 'format' => 'underline'],
                    [
                        'title' => 'Strikethrough',
                        'icon' => 'strikethrough',
                        'format' => 'strikethrough'
                    ],
                    ['title' => 'Superscript', 'icon' => 'superscript', 'format' => 'superscript'],
                    ['title' => 'Subscript', 'icon' => 'subscript', 'format' => 'subscript'],
                    ['title' => 'Code', 'icon' => 'code', 'format' => 'code']
                ]
            ],
            [
                'title' => 'Blocks',
                'items' => [
                    ['title' => 'Paragraph', 'format' => 'p'],
                    ['title' => 'Blockquote', 'format' => 'blockquote'],
                    ['title' => 'Pre', 'format' => 'pre']
                ]
            ],
            [
                'title' => 'Alignment',
                'items' => [
                    ['title' => 'Left', 'icon' => 'alignleft', 'format' => 'alignleft'],
                    ['title' => 'Center', 'icon' => 'aligncenter', 'format' => 'aligncenter'],
                    ['title' => 'Right', 'icon' => 'alignright', 'format' => 'alignright'],
                    ['title' => 'Justify', 'icon' => 'alignjustify', 'format' => 'alignjustify']
                ]
            ]
        ],
        'toolbar' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media',
        'use_lfm' => true,
        'lfm_url' => 'laravel-filemanager',
    ],


    'locales' => [
        'ru' => 'Russian',
        'ua' => 'Ukrainian',
        'by' => 'Belarusian',
        'pl' => 'Polish',
        'kz' => 'Kazakh' ,
    ],
];
